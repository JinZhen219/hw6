package tam.style;

import tam.workspace.courseDetailsTabContent;
import djf.AppTemplate;
import djf.components.AppStyleComponent;
import java.util.HashMap;
import javafx.scene.Node;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import tam.data.TeachingAssistant;
//import csg.workspace.taDataTabContent;
import tam.workspace.TAWorkspace;

/**
 * This class manages all CSS style for this application.
 * 
 * @author Richard McKenna
 * @version 1.0
 */
public class TAStyle extends AppStyleComponent {
    // FIRST WE SHOULD DECLARE ALL OF THE STYLE TYPES WE PLAN TO USE
    
    // WE'LL USE THIS FOR ORGANIZING LEFT AND RIGHT CONTROLS
    public static String CLASS_PLAIN_PANE = "plain_pane";
    
    // THESE ARE THE HEADERS FOR EACH SIDE
    public static String CLASS_HEADER_PANE = "header_pane";
    public static String CLASS_HEADER_LABEL = "header_label";

    // ON THE LEFT WE HAVE THE TA ENTRY
    public static String CLASS_TA_TABLE = "ta_table";
    public static String CLASS_TA_TABLE_COLUMN_HEADER = "ta_table_column_header";
    public static String CLASS_ADD_TA_PANE = "add_ta_pane";
    public static String CLASS_ADD_TA_TEXT_FIELD = "add_ta_text_field";
    public static String CLASS_ADD_TA_BUTTON = "add_ta_button";
    public static String CLASS_UPDATE_TA_BUTTON="update_ta_button";
    public static String CLASS_CLEAR_BUTTON="clear_button"; 
    public static String CLASS_CLEAR_BUTTON_1="clear_button_1"; 

    // ON THE RIGHT WE HAVE THE OFFICE HOURS GRID
    public static String CLASS_OFFICE_HOURS_GRID = "office_hours_grid";
    public static String CLASS_OFFICE_HOURS_GRID_TIME_COLUMN_HEADER_PANE = "office_hours_grid_time_column_header_pane";
    public static String CLASS_OFFICE_HOURS_GRID_TIME_COLUMN_HEADER_LABEL = "office_hours_grid_time_column_header_label";
    public static String CLASS_OFFICE_HOURS_GRID_DAY_COLUMN_HEADER_PANE = "office_hours_grid_day_column_header_pane";
    public static String CLASS_OFFICE_HOURS_GRID_DAY_COLUMN_HEADER_LABEL = "office_hours_grid_day_column_header_label";
    public static String CLASS_OFFICE_HOURS_GRID_TIME_CELL_PANE = "office_hours_grid_time_cell_pane";
    public static String CLASS_OFFICE_HOURS_GRID_TIME_CELL_LABEL = "office_hours_grid_time_cell_label";
    public static String CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE = "office_hours_grid_ta_cell_pane";
    public static String CLASS_OFFICE_HOURS_GRID_TA_CELL_LABEL = "office_hours_grid_ta_cell_label";
    public static String CLASS_OFFICE_HOURS_GRID_NEW_START_TIME="office_hours_grid_new_start_time"; 
    public static String CLASS_OFFICE_HOURS_GRID_NEW_END_TIME="office_hours_grid_new_end_time";   
    public static String CLASS_OFFICE_HOURS_GRID_UPDATE_TIME_BUTTON="office_hours_grid_update_time_button";  

    // FOR HIGHLIGHTING CELLS, COLUMNS, AND ROWS
    public static String CLASS_HIGHLIGHTED_GRID_CELL = "highlighted_grid_cell";
    public static String CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN = "highlighted_grid_row_or_column";
    public static String BOLD_SIZE10 = "boldSize10";
    public static String BOLD_SIZE12 = "boldSize12";
    // THIS PROVIDES ACCESS TO OTHER COMPONENTS
    private AppTemplate app;
    
    /**
     * This constructor initializes all style for the application.
     * 
     * @param initApp The application to be stylized.
     */
    public TAStyle(AppTemplate initApp) {
        // KEEP THIS FOR LATER
        app = initApp;

        // LET'S USE THE DEFAULT STYLESHEET SETUP
        super.initStylesheet(app);

        // INIT THE STYLE FOR THE FILE TOOLBAR
        app.getGUI().initFileToolbarStyle();

        // AND NOW OUR WORKSPACE STYLE
        initTAWorkspaceStyle();
        initCourseDetailsTabContentStyle();
        initRecitationPaneStyle();
        initScheduleTabContent();
        initProjectTabContent();
    }

    /**
     * This function specifies all the style classes for
     * all user interface controls in the workspace.
     */

    private void initTAWorkspaceStyle() {
        // LEFT SIDE - THE HEADER
        TAWorkspace workspaceComponent = (TAWorkspace)app.getWorkspaceComponent();
        workspaceComponent.getTAsHeaderBox().getStyleClass().add(CLASS_HEADER_PANE);
        workspaceComponent.getTAsHeaderLabel().getStyleClass().add(CLASS_HEADER_LABEL);
        workspaceComponent.getTALabel().getStyleClass().add(CLASS_HEADER_LABEL);
        // LEFT SIDE - THE TABLE
        TableView<TeachingAssistant> taTable = workspaceComponent.getTATable();
        taTable.getStyleClass().add(CLASS_TA_TABLE);
        for (TableColumn tableColumn : taTable.getColumns()) {
            tableColumn.getStyleClass().add(CLASS_TA_TABLE_COLUMN_HEADER);
        }
        

        // LEFT SIDE - THE TA DATA ENTRY
        workspaceComponent.getAddBox().getStyleClass().add(CLASS_ADD_TA_PANE);
        workspaceComponent.getNameTextField().getStyleClass().add(CLASS_ADD_TA_TEXT_FIELD);
        workspaceComponent.getEmailTextField().getStyleClass().add(CLASS_ADD_TA_TEXT_FIELD);
        workspaceComponent.getAddButton().getStyleClass().add(CLASS_ADD_TA_BUTTON);
        workspaceComponent.getUpdateTaButton().getStyleClass().add(CLASS_UPDATE_TA_BUTTON); 
        workspaceComponent.getClearButton().getStyleClass().add(CLASS_CLEAR_BUTTON); 
        workspaceComponent.getClearButton1().getStyleClass().add(CLASS_CLEAR_BUTTON_1); 
        workspaceComponent.getStartTimeLabel().getStyleClass().add(BOLD_SIZE10); 
        workspaceComponent.getEndTimeLabel().getStyleClass().add(BOLD_SIZE10);

        // RIGHT SIDE - THE HEADER
        workspaceComponent.getOfficeHoursSubheaderBox().getStyleClass().add(CLASS_HEADER_PANE);
        workspaceComponent.getOfficeHoursSubheaderLabel().getStyleClass().add(CLASS_HEADER_LABEL);
    }
    
    
    private void initCourseDetailsTabContentStyle() {
        // LEFT SIDE - THE HEADER
        TAWorkspace workspaceComponent = (TAWorkspace)app.getWorkspaceComponent();
        
        
        //TOP THIRD OF PANE
        workspaceComponent.getCourseDetailsTabContent().getCourseInfolabel().getStyleClass().add(CLASS_HEADER_LABEL);
        workspaceComponent.getCourseDetailsTabContent().getSubjectLabel().getStyleClass().add(BOLD_SIZE10);
        workspaceComponent.getCourseDetailsTabContent().getNumberLabel().getStyleClass().add(BOLD_SIZE10);
        workspaceComponent.getCourseDetailsTabContent().getYearLabel().getStyleClass().add(BOLD_SIZE10);
        workspaceComponent.getCourseDetailsTabContent().getSemesterLabel().getStyleClass().add(BOLD_SIZE10);
        workspaceComponent.getCourseDetailsTabContent().getTitleLabel().getStyleClass().add(BOLD_SIZE10);
        workspaceComponent.getCourseDetailsTabContent().getInstructNameLabel().getStyleClass().add(BOLD_SIZE10);
        workspaceComponent.getCourseDetailsTabContent().getInstructHomeLabel().getStyleClass().add(BOLD_SIZE10);
        workspaceComponent.getCourseDetailsTabContent().getexportDirLabel().getStyleClass().add(BOLD_SIZE10);
        workspaceComponent.getCourseDetailsTabContent().actualExportDirectoryLabel().getStyleClass().add(BOLD_SIZE10);
       
        //MIDDLE OF SPLITPANE
        workspaceComponent.getCourseDetailsTabContent().getSiteTemplateLabel().getStyleClass().add(CLASS_HEADER_LABEL);
        workspaceComponent.getCourseDetailsTabContent().getTemplateDirectoryLabel().getStyleClass().add(BOLD_SIZE10);
        workspaceComponent.getCourseDetailsTabContent().getSitePagesLabel().getStyleClass().add(BOLD_SIZE10);
        workspaceComponent.getCourseDetailsTabContent().getSubjectLabel().getStyleClass().add(BOLD_SIZE10);
        
        //BOTTOM OF SPLITPANE
        workspaceComponent.getCourseDetailsTabContent().getPageStyleLabel().getStyleClass().add(CLASS_HEADER_LABEL);
        workspaceComponent.getCourseDetailsTabContent().getSchoolImageLabel().getStyleClass().add(BOLD_SIZE10);
        workspaceComponent.getCourseDetailsTabContent().getLeftFooterLabel().getStyleClass().add(BOLD_SIZE10);
        workspaceComponent.getCourseDetailsTabContent().getRightFooterLabel().getStyleClass().add(BOLD_SIZE10);
        workspaceComponent.getCourseDetailsTabContent().getStylesheetLabel().getStyleClass().add(BOLD_SIZE10);

    }
    
    
    
    
    /**
     * This method initializes the style for all UI components in
     * the office hours grid. Note that this should be called every
     * time a new TA Office Hours Grid is created or loaded.
     */
    public void initOfficeHoursGridStyle() {
        // RIGHT SIDE - THE OFFICE HOURS GRID TIME HEADERS
        TAWorkspace workspaceComponent = (TAWorkspace)app.getWorkspaceComponent();
        workspaceComponent.getOfficeHoursGridPane().getStyleClass().add(CLASS_OFFICE_HOURS_GRID);
        setStyleClassOnAll(workspaceComponent.getOfficeHoursGridTimeHeaderPanes(), CLASS_OFFICE_HOURS_GRID_TIME_COLUMN_HEADER_PANE);
        setStyleClassOnAll(workspaceComponent.getOfficeHoursGridTimeHeaderLabels(), CLASS_OFFICE_HOURS_GRID_TIME_COLUMN_HEADER_LABEL);
        setStyleClassOnAll(workspaceComponent.getOfficeHoursGridDayHeaderPanes(), CLASS_OFFICE_HOURS_GRID_DAY_COLUMN_HEADER_PANE);
        setStyleClassOnAll(workspaceComponent.getOfficeHoursGridDayHeaderLabels(), CLASS_OFFICE_HOURS_GRID_DAY_COLUMN_HEADER_LABEL);
        setStyleClassOnAll(workspaceComponent.getOfficeHoursGridTimeCellPanes(), CLASS_OFFICE_HOURS_GRID_TIME_CELL_PANE);
        setStyleClassOnAll(workspaceComponent.getOfficeHoursGridTimeCellLabels(), CLASS_OFFICE_HOURS_GRID_TIME_CELL_LABEL);
        setStyleClassOnAll(workspaceComponent.getOfficeHoursGridTACellPanes(), CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE);
        setStyleClassOnAll(workspaceComponent.getOfficeHoursGridTACellLabels(), CLASS_OFFICE_HOURS_GRID_TA_CELL_LABEL);
        workspaceComponent.getNewEndBox().getStyleClass().add( CLASS_OFFICE_HOURS_GRID_NEW_END_TIME); 
        workspaceComponent.getNewStartBox().getStyleClass().add(CLASS_OFFICE_HOURS_GRID_NEW_START_TIME);
        workspaceComponent.getChangeTimeButton().getStyleClass().add( CLASS_OFFICE_HOURS_GRID_UPDATE_TIME_BUTTON); 
    }
    public void initRecitationPaneStyle(){
        TAWorkspace workspaceComponent = (TAWorkspace)app.getWorkspaceComponent();
        workspaceComponent.getRecitationTabContent().getRecitationLabel().getStyleClass().add(CLASS_HEADER_LABEL);
        workspaceComponent.getRecitationTabContent().getaddEditLabel().getStyleClass().add(BOLD_SIZE12);
        workspaceComponent.getRecitationTabContent().getInstructor().getStyleClass().add(BOLD_SIZE10);
        workspaceComponent.getRecitationTabContent().getDayTime().getStyleClass().add(BOLD_SIZE10);
        workspaceComponent.getRecitationTabContent().getLocation().getStyleClass().add(BOLD_SIZE10);
        workspaceComponent.getRecitationTabContent().getSupervisingTA1().getStyleClass().add(BOLD_SIZE10);
        workspaceComponent.getRecitationTabContent().getSupervisingTA2().getStyleClass().add(BOLD_SIZE10);
        workspaceComponent.getRecitationTabContent().getSection().getStyleClass().add(BOLD_SIZE10);
    }
    public void initProjectTabContent(){
        TAWorkspace workspaceComponent = (TAWorkspace)app.getWorkspaceComponent();
        workspaceComponent.getProjectTabContent().getProjects().getStyleClass().add(CLASS_HEADER_LABEL);
        workspaceComponent.getProjectTabContent().getAddEdit().getStyleClass().add(BOLD_SIZE12);
        workspaceComponent.getProjectTabContent().getAddEdit2().getStyleClass().add(BOLD_SIZE12);
        workspaceComponent.getProjectTabContent().getAddEditName().getStyleClass().add(BOLD_SIZE10);
        workspaceComponent.getProjectTabContent().getAddEditTeam().getStyleClass().add(BOLD_SIZE10);
        workspaceComponent.getProjectTabContent().getColor().getStyleClass().add(BOLD_SIZE10);
        workspaceComponent.getProjectTabContent().getFirstName().getStyleClass().add(BOLD_SIZE10);
        workspaceComponent.getProjectTabContent().getLastNme().getStyleClass().add(BOLD_SIZE10);
        workspaceComponent.getProjectTabContent().getLink().getStyleClass().add(BOLD_SIZE10);
        workspaceComponent.getProjectTabContent().getRole().getStyleClass().add(BOLD_SIZE10);
        workspaceComponent.getProjectTabContent().getStudents().getStyleClass().add(BOLD_SIZE12);
        workspaceComponent.getProjectTabContent().getTeam().getStyleClass().add(BOLD_SIZE12);
        workspaceComponent.getProjectTabContent().getTextColor().getStyleClass().add(BOLD_SIZE10);
        
    }
    public void initScheduleTabContent(){
        TAWorkspace workspaceComponent = (TAWorkspace)app.getWorkspaceComponent();
        workspaceComponent.getScheduleTabContent().getSchedule().getStyleClass().add(CLASS_HEADER_LABEL);
        workspaceComponent.getScheduleTabContent().getCalenderBoundaries().getStyleClass().add(BOLD_SIZE10);
        workspaceComponent.getScheduleTabContent().getStartingMonday().getStyleClass().add(BOLD_SIZE10);
        workspaceComponent.getScheduleTabContent().getEndingFriday().getStyleClass().add(BOLD_SIZE10);
        workspaceComponent.getScheduleTabContent().getScheduleItems().getStyleClass().add(BOLD_SIZE10);
        workspaceComponent.getScheduleTabContent().getAddEdit().getStyleClass().add(BOLD_SIZE12);
        workspaceComponent.getScheduleTabContent().getType().getStyleClass().add(BOLD_SIZE10);
        workspaceComponent.getScheduleTabContent().getDate().getStyleClass().add(BOLD_SIZE10);
        workspaceComponent.getScheduleTabContent().getTime().getStyleClass().add(BOLD_SIZE10);
        workspaceComponent.getScheduleTabContent().getTitle().getStyleClass().add(BOLD_SIZE10);
        workspaceComponent.getScheduleTabContent().getTopic().getStyleClass().add(BOLD_SIZE10);
        workspaceComponent.getScheduleTabContent().getLink().getStyleClass().add(BOLD_SIZE10);
        workspaceComponent.getScheduleTabContent().getCriteria().getStyleClass().add(BOLD_SIZE10);
        
        
    }
    
    
    /**
     * This helper method initializes the style of all the nodes in the nodes
     * map to a common style, styleClass.
     */
    private void setStyleClassOnAll(HashMap nodes, String styleClass) {
        for (Object nodeObject : nodes.values()) {
            Node n = (Node)nodeObject;
            n.getStyleClass().add(styleClass);
        }
    }
}