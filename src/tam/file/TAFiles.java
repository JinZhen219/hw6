package tam.file;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import djf.components.AppDataComponent;
import djf.components.AppFileComponent;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javafx.collections.ObservableList;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import tam.TAManagerApp;
import tam.data.Recitation;
import tam.data.Schedule;
import tam.data.Student;
import tam.data.TAData;
import tam.data.TeachingAssistant;
import tam.data.Team;
import tam.workspace.TAWorkspace;
import tam.workspace.courseDetailsTabContent;
import tam.workspace.scheduleDataTabContent;
import org.apache.commons.io.FileUtils;

/**
 * This class serves as the file component for the TA
 * manager app. It provides all saving and loading 
 * services for the application.
 * 
 * @author Richard McKenna
 */
public class TAFiles implements AppFileComponent {
    // THIS IS THE APP ITSELF
    TAManagerApp app;
    
    // THESE ARE USED FOR IDENTIFYING JSON TYPES
    static final String JSON_START_HOUR = "startHour";
    static final String JSON_END_HOUR = "endHour";
    static final String JSON_OFFICE_HOURS = "officeHours";
    static final String JSON_DAY = "day";
    static final String JSON_TIME = "time";
    static final String JSON_NAME = "name";
    static final String JSON_UNDERGRAD_TAS = "undergrad_tas";
    static final String JSON_EMAIL = "email";
    static final String JSON_TA_UNDERGRAD_BOOLEAN= "undergraduate";
    
    //COURSE DETIALS TAB
    static final String JSON_SUBJECT = "subject";
    static final String JSON_NUMBER = "number";
    static final String JSON_SEMESTER = "semester";
    static final String JSON_YEAR = "year";
    static final String JSON_TITLE= "title";
    static final String JSON_INSTRUCTOR_NAME= "instructorName";
    static final String JSON_INSTRUCTOR_HOME = "instructorHome";
    static final String JSON_EXPORT_DIRECTORY = "exportDir";
    static final String JSON_SITE_TEMPLATE_DIRECTORY = "siteTemplateDirectory";
    static final String JSON_COURSE_DETAILS_CHECKBOXES = "courseDetailsCheckBoxes";
    static final String JSON_HOME_CHECKBOX = "homeCheckBox";
    static final String JSON_SYLLABUS_CHECKBOX= "syllabusCheckBox";
    static final String JSON_SCHEDULE_CHECKBOX = "scheduleCheckBox";
    static final String JSON_HW_CHECKBOX = "hwCheckBox";
    static final String JSON_PROJECTS_CHECKBOX= "projectsCheckBox";
    static final String JSON_SCHOOL_IMAGE = "schoolImageDirectory";
    static final String JSON_LEFT_IMAGE = "leftImageDirectory";
    static final String JSON_RIGHT_IMAGE= "rightImageDirectory";
    static final String JSON_STYLESHEET = "stylesheet";
    static final String JSON_COURSE_INFO_TAB = "courseInfoTab";
    
    //RECITATION TAB
    static final String JSON_RECITATION = "recitations";
    static final String JSON_SECTION= "section";
    static final String JSON_INSTRUCTOR = "instructor";
    static final String JSON_DAY_TIME = "dayTime";
    static final String JSON_LOCATION= "location";
    static final String JSON_TA1 = "ta1";
    static final String JSON_TA2= "ta2";
    
    //SCHEDULE TAB
    static final String JSON_SCHEDULES = "schedules";
    static final String JSON_START_DATE= "startDate";
    static final String JSON_END_DATE = "endDate";
    static final String JSON_TYPE = "type";
    static final String JSON_DATE = "date";
    static final String JSON_SCHEDULE_TITLE = "scheduleTitle";
    static final String JSON_TOPIC = "topic";
    static final String JSON_SCHEDULE_LINK = "link";
    static final String JSON_CRITERIA = "criteria";
    
    
    //PROJECTS TAB
    static final String JSON_TEAMS = "teams";
    static final String JSON_STUDENTS = "students";
    static final String JSON_TEAM_NAME = "teamName";
    static final String JSON_COLOR = "color";
    static final String JSON_TEXT_COLOR = "textColor";
    static final String JSON_LINK = "link";
    static final String JSON_FIRST_NAME = "firstName";
    static final String JSON_LAST_NAME = "lastName";
    static final String JSON_TEAM = "team";
    static final String JSON_ROLE = "role";
    
    
    
    
    
    
    
    public TAFiles(TAManagerApp initApp) {
        app = initApp;
    }

    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
	// CLEAR THE OLD DATA OUT
	TAData dataManager = (TAData)data;
        TAWorkspace workspace = (TAWorkspace)dataManager.getWorkspace();
        
        scheduleDataTabContent scheduleTabContent = (scheduleDataTabContent)workspace.getScheduleTabContent();
        courseDetailsTabContent courseDetailsTabContent = (courseDetailsTabContent)workspace.getCourseDetailsTabContent();
        
        scheduleTabContent.resetScheduleTab();
        courseDetailsTabContent.resetCourseDetails();
        workspace.getRecitationTabContent().resetRecitationTab();
        workspace.getProjectTabContent().resetProjectTab();
        
	// LOAD THE JSON FILE WITH ALL THE DATA
	JsonObject json = loadJSONFile(filePath);
        String courseSubject = json.getString(JSON_SUBJECT);
        String courseNumber = json.getString(JSON_NUMBER);
        String courseSemester = json.getString(JSON_SEMESTER);
        String courseYear = json.getString(JSON_YEAR);
        courseDetailsTabContent.initChoiceBoxes(courseSubject, courseNumber, courseSemester, courseYear);
        
        //LOADS REST OF COURSE INFO
        String title = json.getString(JSON_TITLE);
        String instructorHome = json.getString(JSON_INSTRUCTOR_HOME);
        String instructorName = json.getString(JSON_INSTRUCTOR_NAME);
        courseDetailsTabContent.initCourseInfo(title, instructorName, instructorHome);
        
        // INIT CHECKBOXES
        JsonArray jsonCheckBoxArray = json.getJsonArray(JSON_COURSE_DETAILS_CHECKBOXES);
        JsonObject jsonHomeBoolean = jsonCheckBoxArray.getJsonObject(0);
        boolean home = jsonHomeBoolean.getBoolean(JSON_HOME_CHECKBOX);
        JsonObject jsonSyllabusBoolean = jsonCheckBoxArray.getJsonObject(1);
        boolean syllabus = jsonSyllabusBoolean.getBoolean(JSON_SYLLABUS_CHECKBOX);
        JsonObject jsonScheduleBoolean = jsonCheckBoxArray.getJsonObject(2);
        boolean schedule = jsonScheduleBoolean.getBoolean(JSON_SCHEDULE_CHECKBOX);
        JsonObject jsonHWBoolean = jsonCheckBoxArray.getJsonObject(3);
        boolean hw = jsonHWBoolean.getBoolean(JSON_HW_CHECKBOX);
        JsonObject jsonProjectsBoolean = jsonCheckBoxArray.getJsonObject(4);
        boolean projects = jsonProjectsBoolean.getBoolean(JSON_PROJECTS_CHECKBOX);
        courseDetailsTabContent.initCheckBox(home, syllabus, schedule, hw, projects);
        
        // LOADS PAGE STYLE
        String schoolPath = json.getString(JSON_SCHOOL_IMAGE);
        String leftPath = json.getString(JSON_LEFT_IMAGE);
        String rightPath = json.getString(JSON_RIGHT_IMAGE);
        String stylesheet = json.getString(JSON_STYLESHEET);
        courseDetailsTabContent.initPageStyle(schoolPath, leftPath, rightPath, stylesheet);

	// LOAD THE START AND END HOURS
	String startHour = json.getString(JSON_START_HOUR);
        String endHour = json.getString(JSON_END_HOUR);
        dataManager.initOfficeHoursUpdate(Integer.parseInt(startHour), Integer.parseInt(endHour));

        
        // NOW RELOAD THE WORKSPACE WITH THE LOADED DATA
        app.getWorkspaceComponent().reloadWorkspace(app.getDataComponent());

        // NOW LOAD ALL THE UNDERGRAD TAs
        JsonArray jsonTAArray = json.getJsonArray(JSON_UNDERGRAD_TAS);
        for (int i = 0; i < jsonTAArray.size(); i++) {
            JsonObject jsonTA = jsonTAArray.getJsonObject(i);
            String name = jsonTA.getString(JSON_NAME);
            String email = jsonTA.getString(JSON_EMAIL);
            boolean undergrad = jsonTA.getBoolean(JSON_TA_UNDERGRAD_BOOLEAN);
            dataManager.addTA(name, email, undergrad);
        }

        // AND THEN ALL THE OFFICE HOURS
        JsonArray jsonOfficeHoursArray = json.getJsonArray(JSON_OFFICE_HOURS);
        for (int i = 0; i < jsonOfficeHoursArray.size(); i++) {
            JsonObject jsonOfficeHours = jsonOfficeHoursArray.getJsonObject(i);
            String day = jsonOfficeHours.getString(JSON_DAY);
            String time = jsonOfficeHours.getString(JSON_TIME);
            String name = jsonOfficeHours.getString(JSON_NAME);
            dataManager.addOfficeHoursReservation(day, time, name);
        }
        
        // LOAD RECITATIONS
        JsonArray jsonRecitationArray = json.getJsonArray(JSON_RECITATION);
        for(int i = 0; i< jsonRecitationArray.size(); i++){
            JsonObject jsonRecitation = jsonRecitationArray.getJsonObject(i);
            String section = jsonRecitation.getString(JSON_SECTION);
            String instructor = jsonRecitation.getString(JSON_INSTRUCTOR);
            String dayTime = jsonRecitation.getString(JSON_DAY_TIME);
            String location = jsonRecitation.getString(JSON_LOCATION);
            String ta1 = jsonRecitation.getString(JSON_TA1);
            String ta2 = jsonRecitation.getString(JSON_TA2);
            dataManager.addRecitation(section, instructor, dayTime, location, ta1, ta2);
        }
        
        // LAOD SCHEDULE ITEMS
        JsonArray jsonScheduleArray = json.getJsonArray(JSON_SCHEDULES);
        for(int i = 0; i < jsonScheduleArray.size(); i++){
            JsonObject jsonSchedule = jsonScheduleArray.getJsonObject(i);
            String type = jsonSchedule.getString(JSON_TYPE);
            String date = jsonSchedule.getString(JSON_DATE);
            String time = jsonSchedule.getString(JSON_TIME);
            String scheduleTitle = jsonSchedule.getString(JSON_SCHEDULE_TITLE);
            String topic = jsonSchedule.getString(JSON_TOPIC);
            String scheduleLink = jsonSchedule.getString(JSON_SCHEDULE_LINK);
            String criteria = jsonSchedule.getString(JSON_CRITERIA);
            dataManager.addSchedule(type, date, time, scheduleTitle, topic, scheduleLink, criteria);
        }
       
        // LOADS STARTING MONDAY AND ENDING FRIDAY
        String startDate = json.getString(JSON_START_DATE);
        String endDate = json.getString(JSON_END_DATE);
        scheduleTabContent.setStartDate(startDate);
        scheduleTabContent.setEndDate(endDate);
        
        // LOAD TEAMS
        JsonArray jsonTeamArray = json.getJsonArray(JSON_TEAMS);
        for(int i = 0; i < jsonTeamArray.size(); i++){
            JsonObject jsonTeam = jsonTeamArray.getJsonObject(i);
            String name = jsonTeam.getString(JSON_TEAM_NAME);
            String color = jsonTeam.getString(JSON_COLOR);
            String textColor = jsonTeam.getString(JSON_TEXT_COLOR);
            String link = jsonTeam.getString(JSON_LINK);
            dataManager.addTeam(name, color, textColor, link);
        }
        
        // LOADS STUDENTS ON PROJECTS TAB
        JsonArray jsonStudentArray = json.getJsonArray(JSON_STUDENTS);
        for(int i = 0; i < jsonStudentArray.size(); i++){
            JsonObject jsonStudent = jsonStudentArray.getJsonObject(i);
            String firstName = jsonStudent.getString(JSON_FIRST_NAME);
            String lastName = jsonStudent.getString(JSON_LAST_NAME);
            String team = jsonStudent.getString(JSON_TEAM);
            String role = jsonStudent.getString(JSON_ROLE);
            dataManager.addStudent(firstName, lastName, team, role);
        }
        
    }
      
    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    public JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }

    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
	// GET THE DATA
	TAData dataManager = (TAData)data;
//        TAWorkspace workspace = (TAWorkspace)dataManager.getWorkspace();
//        
//        scheduleDataTabContent scheduleTabContent = (scheduleDataTabContent)workspace.getScheduleTabContent();
//        courseDetailsTabContent courseDetailsTabContent = (courseDetailsTabContent)workspace.getCourseDetailsTabContent();
	// NOW BUILD THE TA JSON OBJCTS TO SAVE
	JsonArrayBuilder taArrayBuilder = Json.createArrayBuilder();
	ObservableList<TeachingAssistant> tas = dataManager.getTeachingAssistants();
	for (TeachingAssistant ta : tas) {	    
	    JsonObject taJson = Json.createObjectBuilder()
		    .add(JSON_NAME, ta.getName())
		    .add(JSON_EMAIL, ta.getEmail())
                    .add(JSON_TA_UNDERGRAD_BOOLEAN, ta.getUndergraduate()).build();
            
	    taArrayBuilder.add(taJson);
	}       
	JsonArray undergradTAsArray = taArrayBuilder.build();
        
        //SAVING COURSE DETAILS TAB CONTENTS
        JsonArrayBuilder courseTabCheckBoxBuilder = Json.createArrayBuilder();
        JsonObject homeCheckBox = Json.createObjectBuilder().add(JSON_HOME_CHECKBOX, dataManager.getHome()).build();
        JsonObject syllabusCheckBox = Json.createObjectBuilder().add(JSON_SYLLABUS_CHECKBOX, dataManager.getSyllabus()).build();
        JsonObject scheduleCheckBox = Json.createObjectBuilder().add(JSON_SCHEDULE_CHECKBOX, dataManager.getSchedule()).build();
        JsonObject hwCheckBox = Json.createObjectBuilder().add(JSON_HW_CHECKBOX, dataManager.getHW()).build();
        JsonObject projectCheckBox = Json.createObjectBuilder().add(JSON_PROJECTS_CHECKBOX, dataManager.getProject()).build();
        courseTabCheckBoxBuilder
                .add(homeCheckBox)
                .add(syllabusCheckBox)
                .add(scheduleCheckBox)
                .add(hwCheckBox)
                .add(projectCheckBox);
        JsonArray courseTabCheckBoxArray = courseTabCheckBoxBuilder.build();
        
        //SAVING RECITATION TAB CONTENT
        JsonArrayBuilder recitationBuilder = Json.createArrayBuilder();
        ObservableList<Recitation> recitations = dataManager.getRecitations();
        for(Recitation rec : recitations){
            JsonObject recitationJson = Json.createObjectBuilder()
                    .add(JSON_SECTION, rec.getSection())
                    .add(JSON_INSTRUCTOR, rec.getInstructor())
                    .add(JSON_DAY_TIME, rec.getDayTime())
                    .add(JSON_LOCATION, rec.getLocation())
                    .add(JSON_TA1, rec.getTa1())
                    .add(JSON_TA2, rec.getTa2()).build();  
            recitationBuilder.add(recitationJson);
        }
        JsonArray recitationTabArray = recitationBuilder.build();
        
        // SAVING SCHEDULE TAB CONTENT
        JsonArrayBuilder scheduleBuilder = Json.createArrayBuilder();
        ObservableList<Schedule> schedules = dataManager.getSchedules();
        for(Schedule sche : schedules){
            JsonObject scheduleJson = Json.createObjectBuilder()
                    .add(JSON_TYPE, sche.getType())
                    .add(JSON_DATE, sche.getDate())
                    .add(JSON_TIME, sche.getTime())
                    .add(JSON_SCHEDULE_TITLE, sche.getTitle())
                    .add(JSON_TOPIC, sche.getTopic())
                    .add(JSON_SCHEDULE_LINK, sche.getLink())
                    .add(JSON_CRITERIA, sche.getCriteria()).build();
            scheduleBuilder.add(scheduleJson);
        }
        JsonArray scheduleTabArray = scheduleBuilder.build();  
        
        //SAVING PROJECTS TAB CONTENT
        // TEAM FIRST
        JsonArrayBuilder teamBuilder = Json.createArrayBuilder();
        ObservableList<Team> teams = dataManager.getTeams();
        for(Team team : teams){
            JsonObject teamJson = Json.createObjectBuilder()
                    .add(JSON_TEAM_NAME, team.getName())
                    .add(JSON_COLOR, team.getColor())
                    .add(JSON_TEXT_COLOR, team.getTextColor())
                    .add(JSON_LINK, team.getLink()).build();
            teamBuilder.add(teamJson);
        }
        JsonArray teamArray = teamBuilder.build();
        
        // NOW STUDENTS
        JsonArrayBuilder studentBuilder = Json.createArrayBuilder();
        ObservableList<Student> students = dataManager.getStudents();
        for(Student student : students){
            JsonObject studentJson = Json.createObjectBuilder()
                    .add(JSON_FIRST_NAME, student.getFirstName())
                    .add(JSON_LAST_NAME, student.getLastName())
                    .add(JSON_TEAM, student.getTeam())
                    .add(JSON_ROLE, student.getRole()).build();
            studentBuilder.add(studentJson);
                    
        }
        JsonArray studentArray = studentBuilder.build();
        
	// NOW BUILD THE TIME SLOT JSON OBJCTS TO SAVE
	JsonArrayBuilder timeSlotArrayBuilder = Json.createArrayBuilder();
	ArrayList<TimeSlot> officeHours = TimeSlot.buildOfficeHoursList(dataManager);
	for (TimeSlot ts : officeHours) {	    
	    JsonObject tsJson = Json.createObjectBuilder()
		    .add(JSON_DAY, ts.getDay())
		    .add(JSON_TIME, ts.getTime())
		    .add(JSON_NAME, ts.getName()).build();
	    timeSlotArrayBuilder.add(tsJson);
	}
	JsonArray timeSlotsArray = timeSlotArrayBuilder.build();
        
	// THEN PUT IT ALL TOGETHER IN A JsonObject
	JsonObject dataManagerJSO = Json.createObjectBuilder()
		.add(JSON_START_HOUR, "" + dataManager.getStartHour())
		.add(JSON_END_HOUR, "" + dataManager.getEndHour())
                .add(JSON_UNDERGRAD_TAS, undergradTAsArray)
                .add(JSON_OFFICE_HOURS, timeSlotsArray)
                
                //ADDING IN COURSE TAB INFO
                .add(JSON_SUBJECT, "" +dataManager.getSubject())
                .add(JSON_NUMBER,"" + dataManager.getNumber())
                .add(JSON_SEMESTER,"" +dataManager.getSemester())
                .add(JSON_YEAR,"" +dataManager.getYear())
                .add(JSON_TITLE, "" +dataManager.getTitle())
                .add(JSON_INSTRUCTOR_NAME,"" + dataManager.getInstructorName())
                .add(JSON_INSTRUCTOR_HOME, "" +dataManager.getInstructorHome())

                .add(JSON_SCHOOL_IMAGE, "" +dataManager.getSchoolDirectory())
                .add(JSON_RIGHT_IMAGE,"" +dataManager.getRightDirectory() )
                .add(JSON_LEFT_IMAGE, "" +dataManager.getLeftDirectory())
                .add(JSON_STYLESHEET, "" +dataManager.getStylesheet())    
                .add(JSON_COURSE_DETAILS_CHECKBOXES, courseTabCheckBoxArray)
                
                //ADDING IN RECITATIONS
                .add(JSON_RECITATION, recitationTabArray)
                
                //ADDING IN SCHEDULES   (CURRENTLY ADDING IN STARTING MONDAY STRING AND ENDING FRIDAY STRING
                .add(JSON_START_DATE,"" + dataManager.getStartDate())
                .add(JSON_END_DATE, "" +dataManager.getEndDate())
                .add(JSON_SCHEDULES, scheduleTabArray)

                //ADDING IN PROJECTS TAB
                .add(JSON_TEAMS, teamArray)
                .add(JSON_STUDENTS, studentArray)
		.build();
	
	// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(dataManagerJSO);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(filePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(dataManagerJSO);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath);
	pw.write(prettyPrinted);
	pw.close();
    }
    
    // IMPORTING/EXPORTING DATA IS USED WHEN WE READ/WRITE DATA IN AN
    // ADDITIONAL FORMAT USEFUL FOR ANOTHER PURPOSE, LIKE ANOTHER APPLICATION

    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {
        // GET THE DATA
        File source = app.getGUI().getFileController().getSelectedDirectory();
        File dest = app.getGUI().getFileController().getNewPath();
        System.out.println(dest);
            //This moves all files 
        FileUtils.copyDirectory(source, dest);
            //Need to save current file to directory of dest /js and save as OfficeHoursGridData.json
       
        File oldJson = new File(dest + "\\js\\OfficeHoursGridData.json");
        oldJson.delete();
        filePath = dest + "\\js\\";
	TAData dataManager = (TAData)data;
        exportCoursePage( data, filePath);
        exportRecitationPage( data,  filePath);
        exportSchedulePage( data,  filePath);
        exportProjectPage( data,  filePath);
        filePath = filePath + "OfficeHoursGridData.json";
	// NOW BUILD THE TA JSON OBJCTS TO SAVE
	JsonArrayBuilder taArrayBuilder = Json.createArrayBuilder();
	ObservableList<TeachingAssistant> tas = dataManager.getTeachingAssistants();
	for (TeachingAssistant ta : tas) {	    
	    JsonObject taJson = Json.createObjectBuilder()
		    .add(JSON_NAME, ta.getName())
		    .add(JSON_EMAIL, ta.getEmail())
                    .add(JSON_TA_UNDERGRAD_BOOLEAN, ta.getUndergraduate()).build();
	    taArrayBuilder.add(taJson);
	}
	JsonArray undergradTAsArray = taArrayBuilder.build();

	// NOW BUILD THE TIME SLOT JSON OBJCTS TO SAVE
	JsonArrayBuilder timeSlotArrayBuilder = Json.createArrayBuilder();
	ArrayList<TimeSlot> officeHours = TimeSlot.buildOfficeHoursList(dataManager);
	for (TimeSlot ts : officeHours) {	    
	    JsonObject tsJson = Json.createObjectBuilder()
		    .add(JSON_DAY, ts.getDay())
		    .add(JSON_TIME, ts.getTime())
		    .add(JSON_NAME, ts.getName()).build();
	    timeSlotArrayBuilder.add(tsJson);
	}
	JsonArray timeSlotsArray = timeSlotArrayBuilder.build();
        
	// THEN PUT IT ALL TOGETHER IN A JsonObject
	JsonObject dataManagerJSO = Json.createObjectBuilder()
		.add(JSON_START_HOUR, "" + dataManager.getStartHour())
		.add(JSON_END_HOUR, "" + dataManager.getEndHour())
                .add(JSON_UNDERGRAD_TAS, undergradTAsArray)
                .add(JSON_OFFICE_HOURS, timeSlotsArray)
		.build();
	
	// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(dataManagerJSO);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(filePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(dataManagerJSO);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath);
	pw.write(prettyPrinted);
	pw.close();
    
    }
    public void exportCoursePage(AppDataComponent data, String filePath) throws IOException{
        TAData dataManager = (TAData)data;
        courseDetailsTabContent courseTab = dataManager.getWorkspace().getCourseDetailsTabContent();
        filePath = filePath + "CourseDetailsData.json";
        JsonArrayBuilder courseTabCheckBoxBuilder = Json.createArrayBuilder();
        JsonObject homeCheckBox = Json.createObjectBuilder().add(JSON_HOME_CHECKBOX, courseTab.getHomeCheckBoolean()).build();
        JsonObject syllabusCheckBox = Json.createObjectBuilder().add(JSON_SYLLABUS_CHECKBOX, courseTab.getSyllabusCheckBoolean()).build();
        JsonObject scheduleCheckBox = Json.createObjectBuilder().add(JSON_SCHEDULE_CHECKBOX, courseTab.getScheduleCheckBoolean()).build();
        JsonObject hwCheckBox = Json.createObjectBuilder().add(JSON_HW_CHECKBOX, courseTab.getHWCheckBoolean()).build();
        JsonObject projectCheckBox = Json.createObjectBuilder().add(JSON_PROJECTS_CHECKBOX, courseTab.getProjectsCheckBoolean()).build();
        courseTabCheckBoxBuilder
                .add(homeCheckBox)
                .add(syllabusCheckBox)
                .add(scheduleCheckBox)
                .add(hwCheckBox)
                .add(projectCheckBox);
        JsonArray courseTabCheckBoxArray = courseTabCheckBoxBuilder.build();
        JsonObject dataManagerJSO = Json.createObjectBuilder()
		.add(JSON_SUBJECT, "" +courseTab.getSubjectChoiceBox())
                .add(JSON_NUMBER,"" + courseTab.getNumberChoiceBox())
                .add(JSON_SEMESTER,"" +courseTab.getSemesterChoiceBox())
                .add(JSON_YEAR,"" +courseTab.getYearChoiceBox())
                .add(JSON_TITLE, "" +courseTab.getCourseTitle())
                .add(JSON_INSTRUCTOR_NAME,"" + courseTab.getInstructorName())
                .add(JSON_INSTRUCTOR_HOME, "" +courseTab.getInstructorHome())

                .add(JSON_SCHOOL_IMAGE, "" +courseTab.getSchoolDirectory())
                .add(JSON_RIGHT_IMAGE,"" +courseTab.getRightDirectory() )
                .add(JSON_LEFT_IMAGE, "" +courseTab.getLeftDirectory())
                .add(JSON_STYLESHEET, "" +courseTab.getStylesheet())    
                .add(JSON_COURSE_DETAILS_CHECKBOXES, courseTabCheckBoxArray)
		.build();
	
	// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(dataManagerJSO);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(filePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(dataManagerJSO);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath);
	pw.write(prettyPrinted);
	pw.close();
    }
    
    
    
    public void exportRecitationPage(AppDataComponent data, String filePath) throws IOException{
        TAData dataManager = (TAData)data;
        filePath = filePath + "RecitationsData.json";
        JsonArrayBuilder recitationBuilder = Json.createArrayBuilder();
        ObservableList<Recitation> recitations = dataManager.getRecitations();
        for(Recitation rec : recitations){
            JsonObject recitationJson = Json.createObjectBuilder()
                    .add(JSON_SECTION, rec.getSection())
                    .add(JSON_INSTRUCTOR, rec.getInstructor())
                    .add(JSON_DAY_TIME, rec.getDayTime())
                    .add(JSON_LOCATION, rec.getLocation())
                    .add(JSON_TA1, rec.getTa1())
                    .add(JSON_TA2, rec.getTa2()).build();  
            recitationBuilder.add(recitationJson);
        }
        JsonArray recitationTabArray = recitationBuilder.build();
        
        JsonObject dataManagerJSO = Json.createObjectBuilder()
		.add(JSON_RECITATION, recitationTabArray)
		.build();
	
	// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(dataManagerJSO);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(filePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(dataManagerJSO);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath);
	pw.write(prettyPrinted);
	pw.close();
    }
    public void exportSchedulePage(AppDataComponent data, String filePath) throws IOException{
        TAData dataManager = (TAData)data;
        filePath = filePath+"ScheduleData.json";
        scheduleDataTabContent scheTab = dataManager.getWorkspace().getScheduleTabContent();
        
        JsonArrayBuilder scheduleBuilder = Json.createArrayBuilder();
        ObservableList<Schedule> schedules = dataManager.getSchedules();
        for(Schedule sche : schedules){
            JsonObject scheduleJson = Json.createObjectBuilder()
                    .add(JSON_TYPE, sche.getType())
                    .add(JSON_DATE, sche.getDate())
                    .add(JSON_TIME, sche.getTime())
                    .add(JSON_SCHEDULE_TITLE, sche.getTitle())
                    .add(JSON_TOPIC, sche.getTopic())
                    .add(JSON_SCHEDULE_LINK, sche.getLink())
                    .add(JSON_CRITERIA, sche.getCriteria()).build();
            scheduleBuilder.add(scheduleJson);
        }
        JsonArray scheduleTabArray = scheduleBuilder.build();  
        JsonObject dataManagerJSO = Json.createObjectBuilder()
		.add(JSON_START_DATE,"" + scheTab.getStartDate())
                .add(JSON_END_DATE, "" +scheTab.getEndDate())
                .add(JSON_SCHEDULES, scheduleTabArray)
		.build();
	
	// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(dataManagerJSO);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(filePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(dataManagerJSO);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath);
	pw.write(prettyPrinted);
	pw.close();
    }
    public void exportProjectPage(AppDataComponent data, String filePath) throws IOException{
        TAData dataManager = (TAData)data;
        filePath = filePath + "TeamsAndStudents.json";
        JsonArrayBuilder teamBuilder = Json.createArrayBuilder();
        ObservableList<Team> teams = dataManager.getTeams();
        for(Team team : teams){
            JsonObject teamJson = Json.createObjectBuilder()
                    .add(JSON_TEAM_NAME, team.getName())
                    .add(JSON_COLOR, team.getColor())
                    .add(JSON_TEXT_COLOR, team.getTextColor())
                    .add(JSON_LINK, team.getLink()).build();
            teamBuilder.add(teamJson);
        }
        JsonArray teamArray = teamBuilder.build();
        
        // NOW STUDENTS
        JsonArrayBuilder studentBuilder = Json.createArrayBuilder();
        ObservableList<Student> students = dataManager.getStudents();
        for(Student student : students){
            JsonObject studentJson = Json.createObjectBuilder()
                    .add(JSON_FIRST_NAME, student.getFirstName())
                    .add(JSON_LAST_NAME, student.getLastName())
                    .add(JSON_TEAM, student.getTeam())
                    .add(JSON_ROLE, student.getRole()).build();
            studentBuilder.add(studentJson);
                    
        }
        JsonArray studentArray = studentBuilder.build();
        JsonObject dataManagerJSO = Json.createObjectBuilder()
		.add(JSON_TEAMS, teamArray)
                .add(JSON_STUDENTS, studentArray)
		.build();
	
	// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(dataManagerJSO);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(filePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(dataManagerJSO);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath);
	pw.write(prettyPrinted);
	pw.close();
    }
    public void testLoadData(AppDataComponent data, String filePath) throws IOException{
        TAData dataManager = (TAData)data;

   
        
	// LOAD THE JSON FILE WITH ALL THE DATA
	JsonObject json = loadJSONFile(filePath);
        String courseSubject = json.getString(JSON_SUBJECT);
        String courseNumber = json.getString(JSON_NUMBER);
        String courseSemester = json.getString(JSON_SEMESTER);
        String courseYear = json.getString(JSON_YEAR);
        dataManager.setSemester(courseSemester);
        dataManager.setSubject(courseSubject);
        dataManager.setNumber(courseNumber);
        dataManager.setYear(courseYear);
        
        
        //LOADS REST OF COURSE INFO
        String title = json.getString(JSON_TITLE);
        String instructorHome = json.getString(JSON_INSTRUCTOR_HOME);
        String instructorName = json.getString(JSON_INSTRUCTOR_NAME);
        dataManager.setTitle(title);
        dataManager.setInstructorHome(instructorHome);
        dataManager.setInstructorName(instructorName);
        // INIT CHECKBOXES
        JsonArray jsonCheckBoxArray = json.getJsonArray(JSON_COURSE_DETAILS_CHECKBOXES);
        JsonObject jsonHomeBoolean = jsonCheckBoxArray.getJsonObject(0);
        boolean home = jsonHomeBoolean.getBoolean(JSON_HOME_CHECKBOX);
        JsonObject jsonSyllabusBoolean = jsonCheckBoxArray.getJsonObject(1);
        boolean syllabus = jsonSyllabusBoolean.getBoolean(JSON_SYLLABUS_CHECKBOX);
        JsonObject jsonScheduleBoolean = jsonCheckBoxArray.getJsonObject(2);
        boolean schedule = jsonScheduleBoolean.getBoolean(JSON_SCHEDULE_CHECKBOX);
        JsonObject jsonHWBoolean = jsonCheckBoxArray.getJsonObject(3);
        boolean hw = jsonHWBoolean.getBoolean(JSON_HW_CHECKBOX);
        JsonObject jsonProjectsBoolean = jsonCheckBoxArray.getJsonObject(4);
        boolean projects = jsonProjectsBoolean.getBoolean(JSON_PROJECTS_CHECKBOX);
        dataManager.setHWBoolean(hw);
        dataManager.setHomeBoolean(home);
        dataManager.setSyllabusBoolean(syllabus);
        dataManager.setProjectBoolean(projects);
        dataManager.setScheduleBoolean(schedule);
        
        // LOADS PAGE STYLE
        String schoolPath = json.getString(JSON_SCHOOL_IMAGE);
        String leftPath = json.getString(JSON_LEFT_IMAGE);
        String rightPath = json.getString(JSON_RIGHT_IMAGE);
        String stylesheet = json.getString(JSON_STYLESHEET);
        dataManager.setSchoolDirectory(schoolPath);
        dataManager.setLeftDirectory(leftPath);
        dataManager.setRightDirectory(rightPath);
        dataManager.setStyleshet(stylesheet);
        
	// LOAD THE START AND END HOURS
	String startHour = json.getString(JSON_START_HOUR);
        String endHour = json.getString(JSON_END_HOUR);
        dataManager.setStartHour(Integer.parseInt(startHour));
        dataManager.setEndHour(Integer.parseInt(endHour));
        
        // NOW RELOAD THE WORKSPACE WITH THE LOADED DATA

        // NOW LOAD ALL THE UNDERGRAD TAs
        JsonArray jsonTAArray = json.getJsonArray(JSON_UNDERGRAD_TAS);
        for (int i = 0; i < jsonTAArray.size(); i++) {
            JsonObject jsonTA = jsonTAArray.getJsonObject(i);
            String name = jsonTA.getString(JSON_NAME);
            String email = jsonTA.getString(JSON_EMAIL);
            boolean undergrad = jsonTA.getBoolean(JSON_TA_UNDERGRAD_BOOLEAN);
            dataManager.addTA(name, email, undergrad);
        }

        // AND THEN ALL THE OFFICE HOURS
        JsonArray jsonOfficeHoursArray = json.getJsonArray(JSON_OFFICE_HOURS);
        for (int i = 0; i < jsonOfficeHoursArray.size(); i++) {
            JsonObject jsonOfficeHours = jsonOfficeHoursArray.getJsonObject(i);
            String day = jsonOfficeHours.getString(JSON_DAY);
            String time = jsonOfficeHours.getString(JSON_TIME);
            String name = jsonOfficeHours.getString(JSON_NAME);
            dataManager.addOfficeHoursReservation(day, time, name);
        }
        
        // LOAD RECITATIONS
        JsonArray jsonRecitationArray = json.getJsonArray(JSON_RECITATION);
        for(int i = 0; i< jsonRecitationArray.size(); i++){
            JsonObject jsonRecitation = jsonRecitationArray.getJsonObject(i);
            String section = jsonRecitation.getString(JSON_SECTION);
            String instructor = jsonRecitation.getString(JSON_INSTRUCTOR);
            String dayTime = jsonRecitation.getString(JSON_DAY_TIME);
            String location = jsonRecitation.getString(JSON_LOCATION);
            String ta1 = jsonRecitation.getString(JSON_TA1);
            String ta2 = jsonRecitation.getString(JSON_TA2);
            dataManager.addRecitation(section, instructor, dayTime, location, ta1, ta2);
        }
        
        // LAOD SCHEDULE ITEMS
        JsonArray jsonScheduleArray = json.getJsonArray(JSON_SCHEDULES);
        for(int i = 0; i < jsonScheduleArray.size(); i++){
            JsonObject jsonSchedule = jsonScheduleArray.getJsonObject(i);
            String type = jsonSchedule.getString(JSON_TYPE);
            String date = jsonSchedule.getString(JSON_DATE);
            String time = jsonSchedule.getString(JSON_TIME);
            String scheduleTitle = jsonSchedule.getString(JSON_SCHEDULE_TITLE);
            String topic = jsonSchedule.getString(JSON_TOPIC);
            String scheduleLink = jsonSchedule.getString(JSON_SCHEDULE_LINK);
            String criteria = jsonSchedule.getString(JSON_CRITERIA);
            dataManager.addSchedule(type, date, time, scheduleTitle, topic, scheduleLink, criteria);
        }
       
        // LOADS STARTING MONDAY AND ENDING FRIDAY
        String startDate = json.getString(JSON_START_DATE);
        String endDate = json.getString(JSON_END_DATE);
        dataManager.setStartDate(startDate);
        dataManager.setEndDate(endDate);
        // LOAD TEAMS
        JsonArray jsonTeamArray = json.getJsonArray(JSON_TEAMS);
        for(int i = 0; i < jsonTeamArray.size(); i++){
            JsonObject jsonTeam = jsonTeamArray.getJsonObject(i);
            String name = jsonTeam.getString(JSON_TEAM_NAME);
            String color = jsonTeam.getString(JSON_COLOR);
            String textColor = jsonTeam.getString(JSON_TEXT_COLOR);
            String link = jsonTeam.getString(JSON_LINK);
            dataManager.addTeam(name, color, textColor, link);
        }
        
        // LOADS STUDENTS ON PROJECTS TAB
        JsonArray jsonStudentArray = json.getJsonArray(JSON_STUDENTS);
        for(int i = 0; i < jsonStudentArray.size(); i++){
            JsonObject jsonStudent = jsonStudentArray.getJsonObject(i);
            String firstName = jsonStudent.getString(JSON_FIRST_NAME);
            String lastName = jsonStudent.getString(JSON_LAST_NAME);
            String team = jsonStudent.getString(JSON_TEAM);
            String role = jsonStudent.getString(JSON_ROLE);
            dataManager.addStudent(firstName, lastName, team, role);
        }
    }
}