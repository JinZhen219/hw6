/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.data;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.CheckBoxTableCell;
/**
 *
 * @author Jin
 */
public class SitePageTemplate <E extends Comparable<E>> implements Comparable<E>
{
    private final StringProperty navbarTitle;
    private final StringProperty fileName;
    private final StringProperty script;
    private boolean use;
    public SitePageTemplate(boolean initUse, String initNavbarTitle, String initFileName, String initScript)
    {
        navbarTitle  = new SimpleStringProperty(initNavbarTitle);
        fileName  = new SimpleStringProperty(initFileName);
        script  = new SimpleStringProperty(initScript);
        use = initUse;
    }
    public boolean getUse()
    {
        return use;
    }
    public String getNavbarTitle()
    {
        return navbarTitle.get();
    }
    public String getFileName()
    {
        return fileName.get();
    }
    public String getScript()
    {
        return script.get();
    }
    public void setUse(boolean use)
    {
        this.use = use;
    }
    @Override 
    public int compareTo(E otherStudent)
    {
        return -1;
    }
    
}
