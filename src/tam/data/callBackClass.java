package tam.data;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.util.Callback;
import tam.data.SitePageTemplate;

public class callBackClass implements Callback<TableColumn.CellDataFeatures<SitePageTemplate, CheckBox>, ObservableValue<CheckBox>> {
    @Override
    public ObservableValue call(TableColumn.CellDataFeatures<SitePageTemplate, CheckBox> param) {
        SitePageTemplate template = (SitePageTemplate)param.getValue();
        CheckBox checkBox = new CheckBox();
        checkBox.selectedProperty().setValue(template.getUse());
        checkBox.selectedProperty().addListener((ov, oldValue, newValue) -> {
            template.setUse(newValue);
        });
        return new SimpleObjectProperty<>(checkBox);
    }
}