package tam.data;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.util.Callback;
import tam.data.TeachingAssistant;

public class CallBackClassTA implements Callback<TableColumn.CellDataFeatures<TeachingAssistant, CheckBox>, ObservableValue<CheckBox>> {
    @Override
    public ObservableValue call(TableColumn.CellDataFeatures<TeachingAssistant, CheckBox> param) {
        TeachingAssistant ta = (TeachingAssistant)param.getValue();
        CheckBox checkBox = new CheckBox();
        checkBox.selectedProperty().setValue(ta.getUndergraduate());
        checkBox.selectedProperty().addListener((ov, oldValue, newValue) -> {
            ta.setUndergraduate(newValue);
        });
        return new SimpleObjectProperty<>(checkBox);
    }
}