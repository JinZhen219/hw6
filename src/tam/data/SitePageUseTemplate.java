/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.data;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Jin
 */
public class SitePageUseTemplate 
{
    private StringProperty header;
    private BooleanProperty checkBox;
    public SitePageUseTemplate(String name, boolean checked)
    {
        header = new SimpleStringProperty(name);
        checkBox = new SimpleBooleanProperty(checked);
        
    }
    public StringProperty headerProperty(){
        return header;
    }
    public BooleanProperty checkProperty(){
        return checkBox;
    }
}
