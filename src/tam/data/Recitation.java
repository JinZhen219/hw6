/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Jin
 */
public class Recitation<E extends Comparable<E>> implements Comparable<E> {
    private final StringProperty section;
    private final StringProperty instructor;
    private final StringProperty dayTime;
    private final StringProperty location; 
    private final StringProperty ta1;
    private final StringProperty ta2;
    StringProperty failure;
    public Recitation(String initSection, String initInstructor, String initDayTime, String initLocation, String initTA1, String initTA2)
    {
        ta1 = new SimpleStringProperty(initTA1);
        ta2 = new SimpleStringProperty(initTA2);
        section = new SimpleStringProperty(initSection);
        instructor = new SimpleStringProperty(initInstructor);
        dayTime = new SimpleStringProperty(initDayTime);
        location = new SimpleStringProperty(initLocation);
        failure = new SimpleStringProperty("fdsfds");
    }
    public String getTa1(){
        return ta1.get();
    }
    public String getTa2(){
        return ta2.get();
    }
    public String getSection()
    {
        return section.get();
    }
    public String getInstructor()
    {
        return instructor.get();
    }
    public String getDayTime()
    {
        return dayTime.get();
    }
    public String getLocation()
    {
        return location.get();
    }
    public void setSection(String initSection)
    {
        section.set(initSection);
    }
    public void setInstructor(String initInstructor)
    {
        instructor.set(initInstructor);
    }
    public void setDayTime(String initDayTime)
    {
        dayTime.set(initDayTime);
    }
    public void setLocation(String initLocation)
    {
        location.set(initLocation);
    }
    public void setTA1(String initTA1)
    {
        ta1.set(initTA1);
    }
    public void setTA2(String initTA2)
    {
        ta2.set(initTA2);
    }
    @Override
    public int compareTo(E otherRecitation)
    {
        return getSection().compareTo(((Recitation)otherRecitation).getSection());
    }
    @Override
    public String toString()
    {
        return section.getValue();
    }
}