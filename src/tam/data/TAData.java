package tam.data;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import djf.components.AppDataComponent;
import djf.ui.AppMessageDialogSingleton;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import properties_manager.PropertiesManager;
import tam.CSGManagerProp;
import tam.TAManagerApp;
import tam.TAManagerProp;
import static tam.TAManagerProp.TA_NAME_AND_EMAIL_NOT_UNIQUE_MESSAGE;
import static tam.TAManagerProp.TA_NAME_AND_EMAIL_NOT_UNIQUE_TITLE;
import tam.workspace.TAWorkspace;
import tam.workspace.TAWorkspace;
import tam.workspace.projectDataTabContent;
import tam.workspace.recitationDataTabContent;
import tam.workspace.scheduleDataTabContent;
//import csg.workspace.taDataTabContent;

/**
 * This is the data component for TAManagerApp. It has all the data needed to be
 * set by the user via the User Interface and file I/O can set and get all the
 * data from this object
 *
 * @author Richard McKenna
 */
public class TAData implements AppDataComponent {

    // WE'LL NEED ACCESS TO THE APP TO NOTIFY THE GUI WHEN DATA CHANGES
    TAManagerApp app;

    // NOTE THAT THIS DATA STRUCTURE WILL DIRECTLY STORE THE
    // DATA IN THE ROWS OF THE TABLE VIEW
    ObservableList<TeachingAssistant> teachingAssistants;
    ObservableList<Recitation> recitationsList;
    ObservableList<Schedule> schedule;
    ObservableList<Team> team;
    ObservableList<Student> student;
    ObservableList<SitePageTemplate> template;
    ObservableList<Student> modifiedStudentList;
    // THIS WILL STORE ALL THE OFFICE HOURS GRID DATA, WHICH YOU
    // SHOULD NOTE ARE StringProperty OBJECTS THAT ARE CONNECTED
    // TO UI LABELS, WHICH MEANS IF WE CHANGE VALUES IN THESE
    // PROPERTIES IT CHANGES WHAT APPEARS IN THOSE LABELS
    HashMap<String, StringProperty> officeHours;

    // THESE ARE THE LANGUAGE-DEPENDENT VALUES FOR
    // THE OFFICE HOURS GRID HEADERS. NOTE THAT WE
    // LOAD THESE ONCE AND THEN HANG ON TO THEM TO
    // INITIALIZE OUR OFFICE HOURS GRID
    ArrayList<String> gridHeaders;

    // THESE ARE THE TIME BOUNDS FOR THE OFFICE HOURS GRID. NOTE
    // THAT THESE VALUES CAN BE DIFFERENT FOR DIFFERENT FILES, BUT
    // THAT OUR APPLICATION USES THE DEFAULT TIME VALUES AND PROVIDES
    // NO MEANS FOR CHANGING THESE VALUES
    int startHour;
    int endHour;
    String subject;
    String semester;
    String number;
    String year;
    String title;
    String instructorHome;
    String instructorName;
    boolean hwUse;
    boolean syllabusUse;
    boolean projectUse;
    boolean scheduleUse;
    boolean homeUse;
    String stylesheet;
    String startDate;
    String endDate;
    String leftImageDirectory = "";
    String rightImageDirectory = "";
    String schoolImageDirectory = "";
    String oldStartDate = "";
    String oldEndDate = "";
    String nextStartDate = "";
    String nextEndDate = "";
    // DEFAULT VALUES FOR START AND END HOURS IN MILITARY HOURS
    
    public static final int MIN_START_HOUR = 9;
    public static final int MAX_END_HOUR = 20;

    /**
     * This constructor will setup the required data structures for use, but
     * will have to wait on the office hours grid, since it receives the
     * StringProperty objects from the Workspace.
     *
     * @param initApp The application this data manager belongs to.
     */
    public TAData(TAManagerApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;
        

        // CONSTRUCT THE LIST OF TAs FOR THE TABLE
        teachingAssistants = FXCollections.observableArrayList();
        recitationsList = FXCollections.observableArrayList();
        schedule = FXCollections.observableArrayList();
        team = FXCollections.observableArrayList();
        student = FXCollections.observableArrayList();
        template = FXCollections.observableArrayList();
        modifiedStudentList = FXCollections.observableArrayList();
        // THESE ARE THE DEFAULT OFFICE HOURS
        startHour = MIN_START_HOUR;
        endHour = MAX_END_HOUR;

        //THIS WILL STORE OUR OFFICE HOURS
        officeHours = new HashMap();

        // THESE ARE THE LANGUAGE-DEPENDENT OFFICE HOURS GRID HEADERS
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        ArrayList<String> timeHeaders = props.getPropertyOptionsList(TAManagerProp.OFFICE_HOURS_TABLE_HEADERS);
        ArrayList<String> dowHeaders = props.getPropertyOptionsList(TAManagerProp.DAYS_OF_WEEK);
        gridHeaders = new ArrayList();
        gridHeaders.addAll(timeHeaders);
        gridHeaders.addAll(dowHeaders);
    }

    /**
     * Called each time new work is created or loaded, it resets all data and
     * data structures such that they can be used for new values.
     */
    @Override
    public void resetData() {
        startHour = MIN_START_HOUR;
        endHour = MAX_END_HOUR;
        teachingAssistants.clear();
        officeHours.clear();
    }

    // ACCESSOR METHODS
    public int getStartHour() {
        return startHour;
    }
    public void setNextStartDate(String date){
        nextStartDate = date;
    }
    public String getOldStartDate(){
        return oldStartDate;  
    }
    public void setNextEndDate(String date){
        nextEndDate = date;
    }
    public String getNextStartDate(){
        return nextStartDate;
    }
    public String getOldEndDate(){
        return oldEndDate;
    }
    public void setOldStartDate(String date){
        oldStartDate = date;
    }
    public void setOldEndDate(String date){
        oldEndDate = date;
    }
    public int getEndHour() {
        return endHour;
    }

    public void setStartHour(int startTime) {
        startHour = startTime;
    }

    public void setEndHour(int endTime) {
        endHour = endTime;
    }

    public ArrayList<String> getGridHeaders() {
        return gridHeaders;
    }

    public ObservableList getTeachingAssistants() {
        return teachingAssistants;
    }
    public ObservableList getRecitations(){
        return recitationsList;
    }
    public ObservableList getStudents()
    {
        return student;
    }
    public ObservableList getTemplate()
    {
        return template;
    }
    public ObservableList getTeams()
    {
        return team;
    }
    public ObservableList getSchedules()
    {
        return schedule;
    }
    public String getCellKey(int col, int row) {
        return col + "_" + row;
    }

    public StringProperty getCellTextProperty(int col, int row) {
        String cellKey = getCellKey(col, row);
        return officeHours.get(cellKey);
    }

    public HashMap<String, StringProperty> getOfficeHours() {
        return officeHours;
    }

    public int getNumRows() {
        return ((endHour - startHour) * 2) + 1;
    }

    public String getTimeString(int militaryHour, boolean onHour) {
        String minutesText = "00";
        if (!onHour) {
            minutesText = "30";
        }

        // FIRST THE START AND END CELLS
        int hour = militaryHour;
        if (hour > 12) {
            hour -= 12;
        }
        String cellText = "" + hour + ":" + minutesText;
        if (militaryHour < 12) {
            cellText += "am";
        } else {
            cellText += "pm";
        }
        return cellText;
    }

    public String getCellKey(String day, String time) {
        int col = gridHeaders.indexOf(day);
        int row = 1;
        int hour = Integer.parseInt(time.substring(0, time.indexOf("_")));
        int milHour = hour;
        if (hour < startHour) {
            milHour += 12;
        }
        row += (milHour - startHour) * 2;
        if (time.contains("_30")) {
            row += 1;
        }
        return getCellKey(col, row);
    }

    public TeachingAssistant getTA(String testName) {
        for (TeachingAssistant ta : teachingAssistants) {
            if (ta.getName().equals(testName)) {
                return ta;
            }
        }
        return null;
    }

    /**
     * This method is for giving this data manager the string property for a
     * given cell.
     */
    public void setCellProperty(int col, int row, StringProperty prop) {
        String cellKey = getCellKey(col, row);
        officeHours.put(cellKey, prop);
    }

    /**
     * This method is for setting the string property for a given cell.
     */
    public void setGridProperty(ArrayList<ArrayList<StringProperty>> grid,
            int column, int row, StringProperty prop) {
        grid.get(row).set(column, prop);
    }

    /*
    * THis method updates the time for office hours 
     */
    public void updateTime(int newStartHour, int newEndHour) {
        HashMap<String, StringProperty> newOfficeHours;
        newOfficeHours = new HashMap();
        TAWorkspace workspaceComponent = (TAWorkspace) app.getWorkspaceComponent();
        int shiftIndex = (newEndHour - newStartHour) * 2 + 1;
        int oldStartRow = (newStartHour - startHour) * 2 + 1;
        int newStartRow = 1;
        if (newStartHour >= startHour && newEndHour <= endHour) // IF New Time is 10 and old time is 8.  New End is 16 , old end is 20         && newEndHour<=endHour
        {

            for (int col = 2; col < 7; col++) {
                for (int i = 0; i < ((newEndHour - newStartHour) * 2); i++) {

                    int row = oldStartRow;
                    String cellKey = col + "_" + row;
                    StringProperty prop = officeHours.get(cellKey);
                    String newCellKey = col + "_" + newStartRow;
                    newOfficeHours.put(newCellKey, prop);
                    oldStartRow++;
                    newStartRow++;

                }
                oldStartRow = (newStartHour - startHour) * 2 + 1;
                newStartRow = 1;
            }
        } else if (newStartHour >= startHour && newEndHour > endHour) {

            for (int col = 2; col < 7; col++) {
                for (int i = 0; i < ((endHour - newStartHour) * 2); i++) {

                    int row = oldStartRow;
                    String cellKey = col + "_" + row;
                    StringProperty prop = officeHours.get(cellKey);
                    String newCellKey = col + "_" + newStartRow;
                    newOfficeHours.put(newCellKey, prop);
                    oldStartRow++;
                    newStartRow++;

                }
                oldStartRow = (newStartHour - startHour) * 2 + 1;
                newStartRow = 1;
            }
        } else if (startHour > newStartHour && endHour >= newEndHour) {
            oldStartRow = 1;
            newStartRow = (startHour - newStartHour) * 2 + 1;
            for (int col = 2; col < 7; col++) {
                for (int i = 0; i < ((newEndHour - startHour) * 2); i++) {

                    int row = oldStartRow;
                    String cellKey = col + "_" + row;
                    StringProperty prop = officeHours.get(cellKey);
                    String newCellKey = col + "_" + newStartRow;
                    newOfficeHours.put(newCellKey, prop);
                    oldStartRow++;
                    newStartRow++;

                }
                oldStartRow = 1;
                newStartRow = (startHour - newStartHour) * 2 + 1;
            }
        } else if (startHour > newStartHour && newEndHour > endHour) {
            oldStartRow = 1;
            newStartRow = (startHour - newStartHour) * 2 + 1;
            for (int col = 2; col < 7; col++) {
                for (int i = 0; i < ((endHour - startHour) * 2); i++) {

                    int row = oldStartRow;
                    String cellKey = col + "_" + row;
                    StringProperty prop = officeHours.get(cellKey);
                    String newCellKey = col + "_" + newStartRow;
                    newOfficeHours.put(newCellKey, prop);
                    oldStartRow++;
                    newStartRow++;

                }
                oldStartRow = 1;
                newStartRow = (startHour - newStartHour) * 2 + 1;
            }
        }

        workspaceComponent.resetWorkspace();
        initOfficeHoursUpdate(newStartHour, newEndHour);

        for (HashMap.Entry<String, StringProperty> entry : newOfficeHours.entrySet()) {
            String key = entry.getKey();
            StringProperty prop = newOfficeHours.get(key);
            toggleTAOfficeHoursUpdate(key, prop);
        }
    }

    private void initOfficeHours(int initStartHour, int initEndHour) {
        // NOTE THAT THESE VALUES MUST BE PRE-VERIFIED
        startHour = initStartHour;
        endHour = initEndHour;

        // EMPTY THE CURRENT OFFICE HOURS VALUES
        officeHours.clear();
        // WE'LL BUILD THE USER INTERFACE COMPONENT FOR THE
        // OFFICE HOURS GRID AND FEED THEM TO OUR DATA
        // STRUCTURE AS WE GO
        TAWorkspace workspaceComponent = (TAWorkspace) app.getWorkspaceComponent();
        workspaceComponent.reloadOfficeHoursGrid(this);
    }

    public void initOfficeHoursUpdate(int initStartHour, int initEndHour) {
        // NOTE THAT THESE VALUES MUST BE PRE-VERIFIED
        startHour = initStartHour;
        endHour = initEndHour;

        // EMPTY THE CURRENT OFFICE HOURS VALUES
        officeHours.clear();

        // WE'LL BUILD THE USER INTERFACE COMPONENT FOR THE
        // OFFICE HOURS GRID AND FEED THEM TO OUR DATA
        // STRUCTURE AS WE GO
        TAWorkspace workspaceComponent = (TAWorkspace) app.getWorkspaceComponent();
        workspaceComponent.reloadOfficeHoursGrid(this);
    }

    public void initHours(String startHourText, String endHourText) {
        int initStartHour = Integer.parseInt(startHourText);
        int initEndHour = Integer.parseInt(endHourText);
        if ((initStartHour >= MIN_START_HOUR)
                && (initEndHour <= MAX_END_HOUR)
                && (initStartHour <= initEndHour)) {
            // THESE ARE VALID HOURS SO KEEP THEM
            initOfficeHours(initStartHour, initEndHour);
        }
    }

    public boolean containsTA(String testName, String testEmail) {
        for (TeachingAssistant ta : teachingAssistants) {
            if (ta.getName().equals(testName)) {
                return true;
            }
            if (ta.getEmail().equals(testEmail)) {
                return true;
            }
        }
        return false;
    }
    public boolean containSchedule(String initDate, String initTitle){
        for (Schedule sche : schedule) {
            if (sche.getDate().equals(initDate) && sche.getTitle().equals(initTitle)) {
                return true;
            }           
        }
        return false;
    }
    public void addTA(String initName, String initEmail, boolean undergrad) {
        // MAKE THE TA
        TeachingAssistant ta = new TeachingAssistant(initName, initEmail, undergrad);

        // ADD THE TA
        if (!containsTA(initName, initEmail)) {
            teachingAssistants.add(ta);
        }

        // SORT THE TAS
        Collections.sort(teachingAssistants);
    }
    
    public boolean editTA(String name, String email)
    {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
//
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        boolean undergrad;
        try{
            undergrad = getTA(workspace.getOldName()).getUndergraduate();
        }
        catch(NullPointerException e){
            undergrad = false;
        }
               //RETURNS WHETHER THE TA HAS BEEN CHANGED OR NOT
        
            removeTA(workspace.getOldName());

            if(containsTA(name,email) == true && workspace.getUpdate() == true)
            {
                //        PropertiesManager props = PropertiesManager.getPropertiesManager();
                System.out.println("failed");
                
                addTA(workspace.getOldName(),workspace.getOldEmail(),undergrad );
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_TITLE), props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_MESSAGE));
//                workspace.getNameTextField().requestFocus();
//                workspace.getNameTextField().clear();
//                workspace.getEmailTextField().clear();
                workspace.getTATable().refresh();    
                return false;
            }
            else
            {
                addTA(name,email, undergrad);
//                workspace.setOldEmail(email);
//                workspace.setOldName(name);
//                workspace.getTATable().refresh();    
                return true;
            }
        
//        catch(Exception e)
//        {
//            addTA(workspace.getOldName(), workspace.getOldEmail());
//        }
        
        
    }
    public void addSitePageTemplate(boolean initUse, String initNavbarTitle, String initFileName, String initScript) {
        // MAKE THE TA
        SitePageTemplate sitePage = new SitePageTemplate(initUse, initNavbarTitle, initFileName, initScript);

        // ADD THE TA
        template.add(sitePage);

        // SORT THE TAS
        Collections.sort(template);
    }
    public void addRecitation(String initSection,String initInstructor,String initDayTime,String initLocation,String initTA1,String initTA2)
    {
        Recitation newRecitation = new Recitation(initSection, initInstructor, initDayTime, initLocation, initTA1, initTA2);
        
        recitationsList.add(newRecitation);
        
        Collections.sort(recitationsList);
    }
    public void removeRecitation(String initSection){
        for (Recitation rec : recitationsList) {
            if (initSection.equals(rec.getSection())) {
                recitationsList.remove(rec);
                return;
            }
        }
    }
    public boolean containRecitation(String initSection){
        for (Recitation rec : recitationsList) {
            if (initSection.equals(rec.getSection())) {
                return true;
            }
        }
        return false;
    }
    public boolean containTeam(String initName){
        for (Team testTeam : team) {
            if (initName.equals(testTeam.getName())) {
                return true;
            }
        }
        return false;
    }
    public void editStudent(Student initStudent, TAWorkspace workspace)
    {
        
        projectDataTabContent projectTab = workspace.getProjectTabContent();
        Student oldStudent = initStudent;
        removeStudent(initStudent.getFirstName(), initStudent.getLastName(), initStudent.getTeam(), initStudent.getRole());
        if(containStudent(projectTab.getFirstNameTextField().getText(), projectTab.getLastNameTextField().getText(), projectTab.getTeamChoiceBox().getValue().toString(), projectTab.getRoleTextField().getText()))
        {
            student.add(oldStudent);
            Collections.sort(student);
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(CSGManagerProp.STUDENT_EXIST_ERROR), props.getProperty(CSGManagerProp.STUDENT_EXIST_ERROR));
        }
        else{
            addStudent(projectTab.getFirstNameTextField().getText(), projectTab.getLastNameTextField().getText(), projectTab.getTeamChoiceBox().getValue().toString(), projectTab.getRoleTextField().getText());
        }
        
        
        
    }
    public void removeStudent(String testFirstName, String testLastName, String testTeam, String testRole){
        for (Student stu : student) {
            if (testFirstName.equals(stu.getFirstName()) && testLastName.equals(stu.getLastName()) && testTeam.equals(stu.getTeam()) && testRole.equals(stu.getRole())) {
                student.remove(stu);
                return;
            }
        }
    }
    public boolean containStudent(String testFirstName, String testLastName, String testTeam, String testRole){
        for (Student testStudent : student) {
            if (testFirstName.equals(testStudent.getFirstName()) && testLastName.equals(testStudent.getLastName()) && testTeam.equals(testStudent.getTeam()) && testRole.equals(testStudent.getRole())) {
                return true;
            }
        }
        return false;
    }
    public void removeTeam(String initName){
        for (Team te : team) {
            if (initName.equals(te.getName())) {
                team.remove(te);
                return;
            }
        }
    }
    public void editTeam(Team testTeam, TAWorkspace workspace){
        projectDataTabContent projectTab = workspace.getProjectTabContent();
        Team oldTeam = testTeam;
        removeTeam(testTeam.getName());
        if(containTeam(projectTab.getTeamNameTextField().getText()))
        {
            team.add(oldTeam);
            Collections.sort(team);
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(CSGManagerProp.TEAM_EXIST_ERROR), props.getProperty(CSGManagerProp.TEAM_EXIST_ERROR));
        }
        else{
            projectTab.getTeamChoiceBox().getItems().remove(oldTeam.getName());
            addTeam(projectTab.getTeamNameTextField().getText(), projectTab.getColorPicker().getValue().toString().substring(2,8), projectTab.getTextColorPicker().getValue().toString().substring(2,8), projectTab.getLinkTextField().getText());
            projectTab.getTeamChoiceBox().getItems().add(projectTab.getTeamNameTextField().getText());
        }
    }
    public void editRecitation(Recitation rec, TAWorkspace workspace){
        recitationDataTabContent recTab = workspace.getRecitationTabContent();
        Recitation oldRecitation = getRecitation(rec.getSection());
        removeRecitation(rec.getSection());
        if(containRecitation(recTab.getSectionTextField().getText()))
        {
            recitationsList.add(oldRecitation);
            Collections.sort(recitationsList);
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(CSGManagerProp.RECITATION_EXIST_ERROR), props.getProperty(CSGManagerProp.RECITATION_EXIST_ERROR));
        }
        else{
        addRecitation(recTab.getSectionTextField().getText(), recTab.getInstructorTextField().getText(), recTab.getDayTimeTextField().getText(),
                        recTab.getLocationTextField().getText(), (String)recTab.getTA1ChoiceBox().getValue(), (String)recTab.getTA2ChoiceBox().getValue());
        }
    }
    public Recitation getRecitation(String initSection){
        for (Recitation rec : recitationsList) {
            if (initSection.equals(rec.getSection())){
                return rec;
            }
        }
        return null;
    }
    public void addSchedule(String type, String date, String time, String title, String topic, String link, String criteria)
    {
        Schedule newSchedule = new Schedule(type,date,time, title,topic, link, criteria);
        schedule.add(newSchedule);
        Collections.sort(schedule);
    }
    public void editSchedule(Schedule sche, TAWorkspace workspace){
        scheduleDataTabContent scheTab = workspace.getScheduleTabContent();
        removeSchedule(sche.getType(), sche.getDate(), sche.getTime(), sche.getTitle(), sche.getTopic(), sche.getLink(), sche.getCriteria());

            addSchedule(scheTab.getTypeChoiceBox().getValue().toString(), scheTab.getScheduleDate(), scheTab.getTimeTextField().getText(), scheTab.getTitleTextField().getText(), 
                                        scheTab.getTopicTextField().getText(), scheTab.getLinkTextField().getText(), scheTab.getCriteriaTextField().getText());
    
//        }
    }
    public void removeSchedule(String type,String date,String time, String title, String topic, String link, String criteria){
        for (Schedule sche : schedule) {
            if (date.equals(sche.getDate()) && title.equals(sche.getTitle()) && type.equals(sche.getType()) && time.equals(sche.getTime()) && topic.equals(sche.getTopic()) && link.equals(sche.getLink()) && criteria.equals((sche.getCriteria()))) {
                schedule.remove(sche);
                return;
            }
        }
    }
    public Schedule getSchedules(String date, String title){
        for (Schedule sche : schedule) {
            if (date.equals(sche.getDate()) && title.equals(sche.getTitle())) {
                return sche;
            }
        }
        return null;
    }
    public void addTeam(String name, String color, String textColor, String link)
    {
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        Team newTeam = new Team(name, color, textColor, link);
        team.add(newTeam);       
        Collections.sort(team);
    }
    public void addStudent(String firstName, String lastName, String team, String role){
        Student newStudent = new Student(firstName, lastName, team, role);
        student.add(newStudent);
        Collections.sort(student);
    }
    public void setSubject(String subject){
        this.subject = subject;
    }
    public void setNumber(String number){
        this.number = number;
    }
    public void setYear(String year){
        this.year = year;
    }
    public void setSemester(String semester){
        this.semester = semester;
    }
    public void setTitle(String title){
        this.title = title;
    }
    public void setInstructorHome(String instructorHome){
        this.instructorHome = instructorHome;
    }
    public void setInstructorName(String instructorName){
        this.instructorName = instructorName;
    }
    public void setHomeBoolean(boolean home){
        homeUse = home;
    }
    public void setSyllabusBoolean(boolean syllabusUse){
        this.syllabusUse = syllabusUse;
    }
    public void setScheduleBoolean(boolean scheduleUse){
        this.scheduleUse = scheduleUse;
    }
    public void setHWBoolean(boolean hwUse){
        this.hwUse = hwUse;
    }
    public void setProjectBoolean(boolean projectUse){
        this.projectUse = projectUse;
    }
    public void setStyleshet(String stylesheet){
        this.stylesheet = stylesheet;
    }
    public boolean getHome(){
        return homeUse;
    }
    public boolean getSyllabus(){
        return syllabusUse;
    }
    public boolean getSchedule(){
        return scheduleUse;
    }
    public boolean getHW(){
        return hwUse;
    }
    public boolean getProject(){
        return projectUse;
    }
    public String getSubject(){
        return subject;
    }
    public String getNumber(){
        return number;
    }
    public String getSemester(){
        return semester;
    }
    public String getYear(){
        return year;
    }
    public String getTitle(){
        return title;
    }
    public String getInstructorName(){
        return instructorName;
    }
    public String getInstructorHome(){
        return instructorHome;
    }
    public String getStylesheet(){
        return stylesheet;
    }
    public String getStartDate(){
        return startDate;
    }
    public void setStartDate(String startDate){
        this.startDate = startDate;
    }
    public void setEndDate(String endDate){
        this.endDate = endDate;
    }
    public String getEndDate(){
        return endDate;
    }
    public String getSchoolDirectory(){
        return schoolImageDirectory;
    }
    public String getLeftDirectory(){
        return leftImageDirectory;
    }
    public String getRightDirectory(){
        return rightImageDirectory;
    }
    public void setSchoolDirectory(String schoolDir){
        schoolImageDirectory = schoolDir;
    }
    public void setLeftDirectory(String leftDir){
        leftImageDirectory = leftDir;
    }
    public void setRightDirectory(String rightDir){
        rightImageDirectory = rightDir;
    }
    
    public ObservableList getModifiedStudentList(){
        return modifiedStudentList;
    }
    
    
    
    

    public void removeTA(String name) {
        for (TeachingAssistant ta : teachingAssistants) {
            if (name.equals(ta.getName())) {
                teachingAssistants.remove(ta);
                return;
            }
        }
    }
    
    public void editTACell(StringProperty cellProp, String taName)
    {
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();

        String cellText = cellProp.getValue();
        // IS IT THE ONLY TA IN THE CELL?
        if (cellText.equals(workspace.getOldName())) {
            cellProp.setValue(taName);
        }
        // IS IT THE FIRST TA IN A CELL WITH MULTIPLE TA'S?
        else if (cellText.indexOf(workspace.getOldName()) == 0) {
            int startIndex = cellText.indexOf("\n") ;
            cellText = taName + cellText.substring(startIndex);
            cellProp.setValue(cellText);
        }
        // IS IT IN THE MIDDLE OF A LIST OF TAs
        else if (cellText.indexOf(workspace.getOldName()) < cellText.indexOf("\n", cellText.indexOf(workspace.getOldName()))) {
            int startIndex = cellText.indexOf("\n" + workspace.getOldName());
            int endIndex = startIndex + workspace.getOldName().length() +1;
            cellText = cellText.substring(0, startIndex) + "\n" + taName  + cellText.substring(endIndex);
            cellProp.setValue(cellText);
        }
        // IT MUST BE THE LAST TA
        else {
            int startIndex = cellText.indexOf( workspace.getOldName());
            cellText = cellText.substring(0, startIndex) + taName;
            cellProp.setValue(cellText);
        }
    }
    
    public TAWorkspace getWorkspace(){
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        return workspace;
    }
    

    public void addOfficeHoursReservation(String day, String time, String taName) {
        String cellKey = getCellKey(day, time);
        toggleTAOfficeHours(cellKey, taName);
    }

    /**
     * This function toggles the taName in the cell represented by cellKey.
     * Toggle means if it's there it removes it, if it's not there it adds it.
     */
    public void toggleTAOfficeHours(String cellKey, String taName) {
        StringProperty cellProp = officeHours.get(cellKey);
        String cellText = cellProp.getValue();
        String[]  split =  cellText.split("\\n");
        boolean has = false;
        for(String x: split)
        {
            if(taName.equals(x))
                has = true;
        }
        // IF IT ALREADY HAS THE TA, REMOVE IT
        if (has == true) {
            removeTAFromCell(cellProp, taName);
        } // OTHERWISE ADD IT
        else if (cellText.length() == 0) {
            cellProp.setValue(taName);
        } else {
            cellProp.setValue(cellText + "\n" + taName);
        }
    }

    public void toggleTAOfficeHoursUpdate(String cellKey, StringProperty prop) {
        String cellText2 = prop.getValue();
        StringProperty cellProp = officeHours.get(cellKey);
        //String cellText = cellProp.getValue();
        cellProp.set(cellText2);

    }

    /**
     * This method removes taName from the office grid cell represented by
     * cellProp.
     */
    public void removeTAFromCell(StringProperty cellProp, String taName) {
        // GET THE CELL TEXT
        int startIndex = 0;
        String cellText = cellProp.getValue();
        String[]  split =  cellText.split("\\n");
        boolean has = false;
        int y = 0;
        for(String x: split)
        {
            y =  y + x.length();
            if(taName.equals(x))
            {
                has = true;
                y = y- x.length();
                break;
            }
            y+= 2;
            
        }
        startIndex = y;
        boolean goOn = true;
        // IS IT THE ONLY TA IN THE CELL?
        if (cellText.equals(taName)) {
            cellProp.setValue("");
            goOn = false;
        }
        
        // IS IT THE FIRST TA IN A CELL WITH MULTIPLE TA'S?
        else if (startIndex == 0) {
            
            cellText = cellText.substring(startIndex+ taName.length() + 1);
            cellProp.setValue(cellText);
            goOn = false;
        }
        if(goOn == true)
        {
            startIndex -=2;
            // IS IT IN THE MIDDLE OF A LIST OF TAs
            if (startIndex < cellText.indexOf("\n", cellText.indexOf(taName))) {

                int endIndex = startIndex + taName.length() + 1;
                cellText = cellText.substring(0, startIndex) + cellText.substring(endIndex);
                cellProp.setValue(cellText);
            }
            // IT MUST BE THE LAST TA
            else {

                cellText = cellText.substring(0, startIndex);
                cellProp.setValue(cellText);
            }
        }
    }

    public void renameTaCell(StringProperty cellProp, String taName, String newName) {
        // GET THE CELL TEXT
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();

        String cellText = cellProp.getValue();
        // IS IT THE ONLY TA IN THE CELL?
        if (cellText.equals(workspace.getOldName())) {
            cellProp.setValue(taName);
        }
        // IS IT THE FIRST TA IN A CELL WITH MULTIPLE TA'S?
        else if (cellText.indexOf(workspace.getOldName()) == 0) {
            int startIndex = cellText.indexOf("\n") ;
            cellText = taName + cellText.substring(startIndex);
            cellProp.setValue(cellText);
        }
        // IS IT IN THE MIDDLE OF A LIST OF TAs
        else if (cellText.indexOf(workspace.getOldName()) < cellText.indexOf("\n", cellText.indexOf(workspace.getOldName()))) {
            int startIndex = cellText.indexOf("\n" + workspace.getOldName());
            int endIndex = startIndex + workspace.getOldName().length() +1;
            cellText = cellText.substring(0, startIndex) + "\n" + taName  + cellText.substring(endIndex);
            cellProp.setValue(cellText);
        }
        // IT MUST BE THE LAST TA
        else {
            int startIndex = cellText.indexOf( workspace.getOldName());
            cellText = cellText.substring(0, startIndex) + taName;
            cellProp.setValue(cellText);
        }
    }
    public void initOfficeHours(){
        officeHours = new HashMap();
        officeHours.put("0_1", new SimpleStringProperty("8:00am"));
        officeHours.put("0_2", new SimpleStringProperty("8:30am"));
        officeHours.put("3_2", new SimpleStringProperty("Calvin Cheng"));
        officeHours.put("4_2", new SimpleStringProperty("Calvin Li"));
        setOfficeHoursGrid(officeHours);
    }
    public void setOfficeHoursGrid(HashMap <String, StringProperty> officeHours){
        this.officeHours = officeHours;
    }
    public void updateTaCell(StringProperty cellProp, String taName, String newName) {
        // GET THE CELL TEXT
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();

        String cellText = cellProp.getValue();
        // IS IT THE ONLY TA IN THE CELL?
        if (cellText.equals(workspace.getOldName())) {
            cellProp.setValue(taName);
        }
        // IS IT THE FIRST TA IN A CELL WITH MULTIPLE TA'S?
        else if (cellText.indexOf(workspace.getOldName()) == 0) {
            int startIndex = cellText.indexOf("\n") ;
            cellText = taName + cellText.substring(startIndex);
            cellProp.setValue(cellText);
        }
        // IS IT IN THE MIDDLE OF A LIST OF TAs
        else if (cellText.indexOf(workspace.getOldName()) < cellText.indexOf("\n", cellText.indexOf(workspace.getOldName()))) {
            int startIndex = cellText.indexOf("\n" + workspace.getOldName());
            int endIndex = startIndex + workspace.getOldName().length() +1;
            cellText = cellText.substring(0, startIndex) + "\n" + taName  + cellText.substring(endIndex);
            cellProp.setValue(cellText);
        }
        // IT MUST BE THE LAST TA
        else {
            int startIndex = cellText.indexOf( workspace.getOldName());
            cellText = cellText.substring(0, startIndex) + taName;
            cellProp.setValue(cellText);
        }
    }
}
