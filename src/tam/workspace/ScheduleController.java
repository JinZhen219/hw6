/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.workspace;

import djf.ui.AppGUI;
import djf.ui.AppMessageDialogSingleton;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableView;
import javafx.scene.input.KeyCode;
import jtps.jTPS;
import jtps.jTPS_Transaction;
import properties_manager.PropertiesManager;
import tam.CSGManagerProp;
import tam.TAManagerApp;
import tam.data.Recitation;
import tam.data.Schedule;
import tam.data.Student;
import tam.data.TAData;
import static tam.workspace.TAController.jTPS;

/**
 *
 * @author Jin
 */
public class ScheduleController {
    TAManagerApp app;
    TAData data;
    
     /*
     * 
     * Constructor, note that the app must already be constructed.
     */
    public ScheduleController(TAManagerApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;
        data = (TAData)app.getDataComponent();
    }
    public void handleAddUpdate(){
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        scheduleDataTabContent scheduleTab = workspace.getScheduleTabContent();
        try{
            if(scheduleTab.update == true)
            {
                Object selectedItem = scheduleTab.scheduleTable.getSelectionModel().getSelectedItem();
                Schedule sche = (Schedule) selectedItem;

                // IF IT REACHES BELOW THEN IT IS UPDATING
                
//                data.editSchedule(sche, workspace);
                jTPS_Transaction trans = new ScheduleEditTransaction(sche, data);
                jTPS.addTransaction(trans);
                markWorkAsEdited();
                handleClear();
            }
            else throw new Exception();
            
        }
        catch(Exception e)
        {        
                    try{
//                        data.addSchedule(scheduleTab.typeChoiceBox.getValue().toString(), scheduleTab.getScheduleDate(), scheduleTab.timeTextField.getText(), scheduleTab.titleTextField.getText(), 
//                                            scheduleTab.topicTextField.getText(), scheduleTab.linkTextField.getText(), scheduleTab.criteriaTextField.getText());
                        jTPS_Transaction trans = new AddScheduleTransaction(data, scheduleTab);
                        jTPS.addTransaction(trans);
                        markWorkAsEdited();
                        handleClear();
                    }
                    catch(NullPointerException ex){
                        PropertiesManager props = PropertiesManager.getPropertiesManager();
                        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                        dialog.show(props.getProperty(CSGManagerProp.MISSING_INFO_ERROR), props.getProperty(CSGManagerProp.MISSING_INFO_ERROR));
                    }
                }
        
    
    }
    public void handleDelete(KeyCode code){
        if(code == KeyCode.DELETE){
            TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
            scheduleDataTabContent scheduleTab = workspace.getScheduleTabContent();
            
            TableView scheduleTable = scheduleTab.scheduleTable;
            Object selectedItem = scheduleTable.getSelectionModel().getSelectedItem();
            if (selectedItem != null) {
                Schedule sche = (Schedule) selectedItem;
                TAData data = (TAData) app.getDataComponent();
                
                jTPS_Transaction transaction1 = new DeleteScheduleTransaction(sche, data);
                jTPS.addTransaction(transaction1);
                handleClear();
                markWorkAsEdited();
            }
        }
    }
    public void handleStartDateChange(){
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        scheduleDataTabContent scheduleTab = workspace.getScheduleTabContent();
        DatePicker startDatePicker = scheduleTab.startDatePicker;
        jTPS_Transaction startDateTrans = new DateChangeTransaction(startDatePicker, data);
        jTPS.addTransaction(startDateTrans);
        markWorkAsEdited();
    }
    public void handleEndDateChange(){
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        scheduleDataTabContent scheduleTab = workspace.getScheduleTabContent();
        DatePicker endDatePicker = scheduleTab.endDatePicker;
        jTPS_Transaction endDateTrans = new EndDateTransaction(endDatePicker, data);
        jTPS.addTransaction(endDateTrans);
        markWorkAsEdited();
    }
    public void handleClear(){
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        scheduleDataTabContent scheduleTab = workspace.getScheduleTabContent();
        scheduleTab.typeChoiceBox.setValue(null);
        scheduleTab.timeTextField.clear();
        scheduleTab.titleTextField.clear();
        scheduleTab.topicTextField.clear();
        scheduleTab.linkTextField.clear();
        scheduleTab.criteriaTextField.clear();
        scheduleTab.dateDatePicker.setValue(null);
        scheduleTab.timeTextField.requestFocus();
        scheduleTab.update = false;
    }
    public void markWorkAsEdited() {
        // MARK WORK AS EDITED
        AppGUI gui = app.getGUI();
        gui.getFileController().markAsEdited(gui);
    }    
    public jTPS getJTPS(){
        return jTPS;
    }
        
}
