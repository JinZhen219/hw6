/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.workspace;

import jtps.jTPS_Transaction;
import tam.data.Schedule;
import tam.data.TAData;

/**
 *
 * @author Jin
 */
public class AddScheduleTransaction implements jTPS_Transaction{
    private TAData data;
    private scheduleDataTabContent scheduleTab;
    private String type;
    private String date;
    private String time;
    private String title;
    private String topic;
    private String link;
    private String criteria;
    
    public AddScheduleTransaction(TAData data, scheduleDataTabContent scheduleTab){
        this.data = data;
        this.scheduleTab = scheduleTab;
        type = scheduleTab.getTypeChoiceBox().getValue().toString();
        date = scheduleTab.getScheduleDate();
        time = scheduleTab.getTimeTextField().getText();
        title = scheduleTab.getTitleTextField().getText();
        topic = scheduleTab.getTopicTextField().getText();
        link =scheduleTab.getLinkTextField().getText();
        criteria = scheduleTab.getCriteriaTextField().getText();
    }
    @Override 
    public void doTransaction(){
        data.addSchedule(type, date, time, title, topic, link, criteria);
    }
    @Override
    public void undoTransaction(){
        data.removeSchedule(type, date, time, title, topic, link, criteria);
    }
    public void handleClear(){
        TAWorkspace workspace = data.getWorkspace();
        scheduleDataTabContent scheduleTab = workspace.getScheduleTabContent();
        scheduleTab.typeChoiceBox.setValue(null);
        scheduleTab.timeTextField.clear();
        scheduleTab.titleTextField.clear();
        scheduleTab.topicTextField.clear();
        scheduleTab.linkTextField.clear();
        scheduleTab.criteriaTextField.clear();
        scheduleTab.dateDatePicker.setValue(null);
        scheduleTab.timeTextField.requestFocus();
        scheduleTab.update = false;
    }
}
