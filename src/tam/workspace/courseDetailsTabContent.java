/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.workspace;

import tam.CSGManagerProp;
import java.util.ArrayList;
import java.util.HashMap;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SplitPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import properties_manager.PropertiesManager;
import tam.data.SitePageTemplate;
import tam.TAManagerApp;
import tam.data.TAData;
import tam.data.SitePageUseTemplate;
import tam.data.callBackClass;


/**
 *
 * @author Jin
 */
public class courseDetailsTabContent
{
    VBox courseInfoBox = new VBox();
    
    TextField titleTextField = new TextField();
    TextField instructorNameTextField = new TextField();
    TextField instructorHomeTextField = new TextField();
    SplitPane courseDetailsSplitPane = new SplitPane();
    ChoiceBox subjectChoiceBox = new ChoiceBox();
    ChoiceBox semesterChoiceBox = new ChoiceBox();
    ChoiceBox numberChoiceBox = new ChoiceBox();
    ChoiceBox yearChoiceBox = new ChoiceBox();
   
    Label courseInfoLabel = new Label();
    Label subjectLabel = new Label();
    Label semesterLabel = new Label();
    Label numberLabel = new Label();
    Label yearLabel = new Label();
    Label titleLabel = new Label();
    Label instructNameLabel = new Label();
    Label instructHomeLabel = new Label();
    Label exportDirLabel = new Label();
    Label actualExportDirectoryLabel = new Label();
    Button exportChangeButton = new Button();
    Label siteTemplateLabel = new Label();
    Label siteTemplateTextLabel = new Label();
    Label templateDirectoryLabel = new Label();
    Button templateDirectoryButton = new Button();
    Label sitePagesLabel = new Label();
    GridPane sitePagesGridPane = new GridPane();
    Label pageStyleLabel = new Label();
    Label schoolImageLabel = new Label();
    Label leftFooterLabel = new Label();
    Label rightFooterLabel = new Label();
    Button schoolChangeButton = new Button();
    Button leftChangeButton = new Button();
    Button rightChangeButton = new Button();
    Label stylesheetLabel = new Label();
    ChoiceBox stylesheetChoiceBox = new ChoiceBox();
    Label noteLabel = new Label();
    HBox subjectLineBox = new HBox();
    HBox subjectLineBox2 = new HBox();
    HBox subjectLineBox3 = new HBox();
    CheckBox homeCheckBox = new CheckBox();
    CheckBox syllabusCheckBox = new CheckBox();
    CheckBox scheduleCheckBox = new CheckBox();
    CheckBox hwCheckBox = new CheckBox();
    CheckBox projectCheckBox = new CheckBox();
    boolean homeCheckBoolean = false;
    boolean syllabusCheckBoolean = false;
    boolean scheduleCheckBoolean = false;
    boolean hwCheckBoolean = false;
    boolean projectCheckBoolean = false;
    Image schoolImage;
    Image leftImage;
    Image rightImage;
    String schoolImageDirectory = "";
    String leftImageDirectory = "";
    String rightImageDirectory = "";
    TableView <SitePageTemplate> siteTemplateTable;
    TableColumn navbarColumn;
    TableColumn fileNameColumn;
    TableColumn scriptColumn;
    ScrollPane finalPane;
    TAManagerApp app;
    SitePageTemplate homeTemplate;
    SitePageTemplate syllabusTemplate;
    SitePageTemplate scheduleTemplate;
    SitePageTemplate hwTemplate;
    SitePageTemplate projectsTemplate;
    CourseController courseController;
    ImageView schoolImageView;
    ImageView leftImageView ;
    ImageView rightImageView ;
    Pane schoolImagePane = new Pane();
    Pane leftImagePane = new Pane();
    Pane rightImagePane = new Pane();
    
    
    
    public courseDetailsTabContent(TAManagerApp initApp)
    {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        // INITIATING THE TEXT OF EACH LABEL
        app = initApp;
        courseInfoLabel.setText(props.getProperty(CSGManagerProp.COURSE_INFO.toString()));
        subjectLabel.setText(props.getProperty(CSGManagerProp.SUBJECT));
        numberLabel.setText(props.getProperty(CSGManagerProp.NUMBER));
        semesterLabel.setText(props.getProperty(CSGManagerProp.SEMESTER));
        yearLabel.setText(props.getProperty(CSGManagerProp.YEAR));
        titleLabel.setText(props.getProperty(CSGManagerProp.TITLE));
        instructNameLabel.setText(props.getProperty(CSGManagerProp.INSTRUCTOR_NAME));
        instructHomeLabel.setText(props.getProperty(CSGManagerProp.INSTRUCTOR_HOME));
        exportDirLabel.setText(props.getProperty(CSGManagerProp.EXPORT_DIR));
        schoolChangeButton.setText(props.getProperty(CSGManagerProp.CHANGE_BUTTON));
        leftChangeButton.setText(props.getProperty(CSGManagerProp.CHANGE_BUTTON));
        rightChangeButton.setText(props.getProperty(CSGManagerProp.CHANGE_BUTTON));
        exportChangeButton.setText(props.getProperty(CSGManagerProp.CHANGE_BUTTON));
        siteTemplateLabel.setText(props.getProperty(CSGManagerProp.SITE_TEMPLATE));
        siteTemplateTextLabel.setText(props.getProperty(CSGManagerProp.SITE_TEMPLATE_TEXT));
        templateDirectoryButton.setText(props.getProperty(CSGManagerProp.TEMPLATE_DIRECTORY));
        sitePagesLabel.setText(props.getProperty(CSGManagerProp.SITE_PAGES));
        pageStyleLabel.setText(props.getProperty(CSGManagerProp.PAGE_STYLE));
        schoolImageLabel.setText(props.getProperty(CSGManagerProp.SCHOOL_IMAGE));
        leftFooterLabel.setText(props.getProperty(CSGManagerProp.LEFT_IMAGE));
        rightFooterLabel.setText(props.getProperty(CSGManagerProp.RIGHT_IMAGE));
        stylesheetLabel.setText(props.getProperty(CSGManagerProp.STYLESHEET));
        noteLabel.setText(props.getProperty(CSGManagerProp.STYLESHEET_NOTE));
        
  
        // ORGANIZING COURSE INFO
        courseInfoBox = new VBox(15);

        subjectLineBox3.getChildren().addAll(subjectLabel, subjectChoiceBox);
        subjectLineBox2.getChildren().addAll(numberLabel, numberChoiceBox);
        subjectLineBox.getChildren().addAll(subjectLineBox3, subjectLineBox2);
        subjectLabel.setPrefWidth(130);
        subjectChoiceBox.setPrefWidth(70);
        numberLabel.setPrefWidth(130);
        numberChoiceBox.setPrefWidth(70);
        subjectLineBox3.setPrefWidth(230);
        HBox semesterLineBox = new HBox();
        HBox semesterLineBox2 = new HBox();
        HBox semesterLineBox3 = new HBox();
        
        
        
        subjectChoiceBox.getItems().addAll("CSE", "ISE");
        numberChoiceBox.getItems().addAll("219", "380");
        semesterChoiceBox.getItems().addAll("Spring" , "Fall");
        yearChoiceBox.getItems().addAll("2017", "2018");
        
        
        stylesheetChoiceBox.getItems().add("sea_wolf.css");
        
        
        
        semesterLineBox2.getChildren().addAll(semesterLabel, semesterChoiceBox);
        semesterLineBox3.getChildren().addAll(yearLabel, yearChoiceBox);
        semesterLineBox.getChildren().addAll(semesterLineBox2, semesterLineBox3);
        semesterLabel.setPrefWidth(130);
        semesterChoiceBox.setPrefWidth(70);
        yearLabel.setPrefWidth(130);
        yearChoiceBox.setPrefWidth(70);
        semesterLineBox2.setPrefWidth(230);
        
        HBox titleLine = new HBox(50);
        titleTextField.setPromptText(props.getProperty(CSGManagerProp.TITLE_PROMPT_TEXT));
        instructorNameTextField.setPromptText(props.getProperty(CSGManagerProp.INSTRUCTOR_NAME_PROMPT_TEXT));
        instructorHomeTextField.setPromptText(props.getProperty(CSGManagerProp.INSTRUCTOR_HOME_PROMPT_TEXT));
        titleLine.getChildren().addAll(titleLabel, titleTextField);
        titleLabel.setPrefWidth(150);
        
        HBox instructorNameLine = new HBox(50);
        templateDirectoryLabel.setText(".\\templates\\CSE219");
        actualExportDirectoryLabel.setText("..\\..\\..\\Courses\\CSE219\\Summer2017\\public");
        instructorNameTextField.setPrefWidth(400);
        instructorHomeTextField.setPrefWidth(400);
        titleTextField.setPrefWidth(400);
        instructorNameLine.getChildren().addAll(instructNameLabel, instructorNameTextField);
        HBox instructorHomeLine = new HBox(50);
        instructNameLabel.setPrefWidth(150);
        instructHomeLabel.setPrefWidth(150);
        instructorHomeLine.getChildren().addAll(instructHomeLabel, instructorHomeTextField);
        HBox exportLine = new HBox(50);
        exportLine.getChildren().addAll(exportDirLabel, actualExportDirectoryLabel, exportChangeButton);
        exportDirLabel.setPrefWidth(150);
        actualExportDirectoryLabel.setPrefWidth(400);
        exportChangeButton.setPrefWidth(75);
        courseInfoBox.getChildren().addAll(courseInfoLabel, subjectLineBox, semesterLineBox, titleLine, instructorNameLine, instructorHomeLine, exportLine);
        
        // ORGANIZING SITE TEMPLATE PORTION OF THE COURSE DETAILS TAB
        VBox siteTemplateBox = new VBox(15);
        TAData data = new TAData(app);
        
        
        
        boolean notChecked = false;
        ArrayList<String> gridHeaders = props.getPropertyOptionsList(CSGManagerProp.SITE_PAGE_GRID_HEADERS);
        ArrayList<String> navbarLabels = props.getPropertyOptionsList(CSGManagerProp.NAVBAR_LABELS);
        ArrayList<String> fileNameLabels = props.getPropertyOptionsList(CSGManagerProp.FILE_NAME_LABELS);
        ArrayList<String> scriptsLabels = props.getPropertyOptionsList(CSGManagerProp.SCRIPTS_LABELS);
        
        ObservableList<SitePageTemplate> templateData = data.getTemplate();
        homeTemplate = new SitePageTemplate(notChecked, navbarLabels.get(0), fileNameLabels.get(0), scriptsLabels.get(0));
        syllabusTemplate = new SitePageTemplate(notChecked, navbarLabels.get(1), fileNameLabels.get(1), scriptsLabels.get(1));
        scheduleTemplate = new SitePageTemplate(true, navbarLabels.get(2), fileNameLabels.get(2), scriptsLabels.get(2));
        hwTemplate = new SitePageTemplate(notChecked, navbarLabels.get(3), fileNameLabels.get(3), scriptsLabels.get(3));
        projectsTemplate = new SitePageTemplate(notChecked, navbarLabels.get(4), fileNameLabels.get(4), scriptsLabels.get(4));
        
        
        templateData.add(homeTemplate);
        templateData.add(syllabusTemplate);
        templateData.add(scheduleTemplate);
        templateData.add(hwTemplate);
        templateData.add(projectsTemplate);
        
        
        ArrayList<String> sitePageGridHeaders = props.getPropertyOptionsList(CSGManagerProp.SITE_PAGE_GRID_HEADERS);
        navbarColumn = new TableColumn(sitePageGridHeaders.get(1));
        fileNameColumn = new TableColumn(sitePageGridHeaders.get(2));
        scriptColumn = new TableColumn(sitePageGridHeaders.get(3));


        siteTemplateTable = new TableView<SitePageTemplate>();
        siteTemplateTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        
        
        siteTemplateTable.setItems(templateData);

        TableColumn<SitePageTemplate, CheckBox> c2 = new TableColumn<SitePageTemplate, CheckBox>(sitePageGridHeaders.get(0));
        c2.setCellValueFactory(new callBackClass());
        
        
        

//        useColumn.setCellValueFactory(new PropertyValueFactory<SitePageTemplate, Boolean>("use"));
//            final Callback<TableColumn<SitePageTemplate, Boolean>, TableCell<SitePageTemplate, Boolean>> cellFactory = CheckBoxTableCell.forTableColumn(useColumn);
//            useColumn.setCellFactory(new Callback<TableColumn<SitePageTemplate, Boolean>, TableCell<SitePageTemplate, Boolean>>() {
//                @Override
//                public TableCell<SitePageTemplate, Boolean> call(TableColumn<SitePageTemplate, Boolean> column) {
//                    TableCell<SitePageTemplate, Boolean> cell = cellFactory.call(column);
//                    cell.setAlignment(Pos.CENTER);
//                    return cell ;
//                }
//            });
//        useColumn.setCellFactory(cellFactory);
//        useColumn.setEditable(true);



        navbarColumn.setCellValueFactory(new PropertyValueFactory<SitePageTemplate, String>("navbarTitle"));
        fileNameColumn.setCellValueFactory(new PropertyValueFactory<SitePageTemplate, String>("fileName"));
        scriptColumn.setCellValueFactory(new PropertyValueFactory<SitePageTemplate, String>("script"));
        
        navbarColumn.setStyle( "-fx-alignment: CENTER;");
        fileNameColumn.setStyle( "-fx-alignment: CENTER;");
        scriptColumn.setStyle( "-fx-alignment: CENTER;");
        
        
        
        navbarColumn.setMinWidth(200);
        fileNameColumn.setMinWidth(200);
        scriptColumn.setMinWidth(200);
        siteTemplateTable.getColumns().addAll(c2, navbarColumn, fileNameColumn, scriptColumn);
        siteTemplateTable.setFocusTraversable(true);
        siteTemplateTable.setEditable(true);
        siteTemplateTable.setMinHeight(160);
        siteTemplateTable.setMaxHeight(160);
        siteTemplateTable.setMaxWidth(820);
        
        
        for (int i = 0; i < 5; i++) {
            if(i < 4)
                addCellToGrid( gridHeaders, i, 0);

                addLabelsToGrid(navbarLabels, 1, i+1);
                addLabelsToGrid(fileNameLabels, 2, i+1);
                addLabelsToGrid(scriptsLabels, 3, i+1);    
        }
        Label emptyLabel = new Label("  ");
        Label emptyLabel2 = new Label("  ");
        Label emptyLabel3 = new Label("  ");
        Label emptyLabel4 = new Label("  ");
        Label emptyLabel5 = new Label("  ");
        
        HBox homeCheck = new HBox();
        HBox syllabusCheck = new HBox();
        syllabusCheck.getChildren().addAll(emptyLabel2, syllabusCheckBox);
        HBox scheduleCheck = new HBox(emptyLabel3, scheduleCheckBox);
        HBox hwCheck = new HBox(emptyLabel4, hwCheckBox);
        HBox projectCheck = new HBox(emptyLabel5, projectCheckBox);
        homeCheck.getChildren().addAll(emptyLabel, homeCheckBox);
        
//        syllabusCheck.setOnMouseClicked(e -> {
//            syllabusCheckBoolean = !syllabusCheckBoolean;
//            System.out.println(syllabusCheckBoolean);
//                });
//        scheduleCheckBox.setOnMouseClicked(e->{
//            scheduleCheckBoolean = !scheduleCheckBoolean;
//            System.out.println(scheduleCheckBoolean);
//        });
//        hwCheckBox.setOnMouseClicked(e->{
//            hwCheckBoolean = !hwCheckBoolean;
//            System.out.println(hwCheckBoolean);
//        });
//        projectCheckBox.setOnMouseClicked(e ->{
//            projectCheckBoolean = !projectCheckBoolean;
//        });
//        homeCheckBox.setOnMouseClicked(e->
//        {
//            homeCheckBoolean = !homeCheckBoolean;
//        });
        
        sitePagesGridPane.add(homeCheck, 0, 1);
        sitePagesGridPane.add(syllabusCheck, 0, 2);
        sitePagesGridPane.add(scheduleCheck, 0, 3);
        sitePagesGridPane.add(hwCheck, 0, 4);
        sitePagesGridPane.add(projectCheck, 0, 5);
        sitePagesGridPane.setGridLinesVisible(true);
        sitePagesGridPane.setPrefHeight(40);     
        sitePagesGridPane.setPadding(new Insets(10, 10, 10, 30));
        siteTemplateBox.getChildren().addAll(siteTemplateLabel, siteTemplateTextLabel, templateDirectoryLabel, templateDirectoryButton, sitePagesLabel, siteTemplateTable);
        
        // ORGANIZES PAGE STYLE SECTION OF COURSE DETAILS PAGE

        HBox schoolBox = new HBox(schoolImageLabel, schoolImagePane);
        HBox schoolBox2 = new HBox(schoolBox, schoolChangeButton);
        HBox leftFooterBox = new HBox(leftFooterLabel, leftImagePane);
        
        schoolImageLabel.setPrefWidth(200);
        schoolBox.setPrefWidth(400);
        leftFooterBox.setPrefWidth(400);
        HBox leftFooterBox2 = new HBox(leftFooterBox, leftChangeButton);
        leftFooterLabel.setPrefWidth(200);
        rightFooterLabel.setPrefWidth(200);
        
        
        
        HBox rightFooterBox = new HBox(rightFooterLabel,rightImagePane);
        HBox rightFooterBox2 = new HBox(rightFooterBox, rightChangeButton);
        rightFooterBox.setPrefWidth(400);
        leftFooterBox2.setSpacing(50);
        rightFooterBox2.setSpacing(50);
        schoolBox2.setSpacing(50);
        stylesheetChoiceBox.setPrefWidth(150);
        stylesheetLabel.setPrefWidth(120);
        HBox stylesheetBox = new HBox(stylesheetLabel, stylesheetChoiceBox);
        stylesheetBox.setSpacing(40);
        VBox pageStyleBox = new VBox(pageStyleLabel, schoolBox2, leftFooterBox2, rightFooterBox2, stylesheetBox, noteLabel);
        pageStyleBox.setSpacing(10);
        
        
        

        siteTemplateBox.setPadding(new Insets(5, 0, 0, 0));
        pageStyleBox.setPadding(new Insets(5,0,0,0));
        courseDetailsSplitPane = new SplitPane(courseInfoBox, siteTemplateBox, pageStyleBox);

        
        courseDetailsSplitPane.setOrientation(Orientation.VERTICAL);
        courseDetailsSplitPane.setDividerPositions(0.37f, 0.37f, 0.3f);
        courseDetailsSplitPane.setPadding(new Insets(5,5,5,10));
        VBox courseBox = new VBox(courseInfoBox, siteTemplateBox, pageStyleBox);
        courseBox.setStyle("-fx-background-color : bisque");
        courseBox.setSpacing(10);
        courseInfoBox.setStyle("-fx-background-color:#f4f4f4");
        siteTemplateBox.setStyle("-fx-background-color:#f4f4f4");
        pageStyleBox.setStyle("-fx-background-color:#f4f4f4");
        courseInfoBox.setPadding(new Insets(5,5,5,5));
        siteTemplateBox.setPadding(new Insets(5,5,5,5));
        pageStyleBox.setPadding(new Insets(5,5,5,5));
        finalPane = new ScrollPane(courseBox);
        finalPane.setFitToWidth(true);
        
        courseController = new CourseController(app);
        
        
        // SETTING USAGE FOR BUTTONS AND STUFF
        exportChangeButton.setOnMouseClicked(e ->{
            courseController.handleChangeExportDirectory();
        });
        templateDirectoryButton.setOnMouseClicked(e->{
            courseController.handleChangeTemplateDirectory();
        });
        schoolChangeButton.setOnMouseClicked(e->{
            courseController.handleChangeSchoolImage();
        });
        leftChangeButton.setOnMouseClicked(e->{
            courseController.handleChangeLeftImage();
        });
        rightChangeButton.setOnMouseClicked(e->{
            courseController.handleChangeRightImage();
        });
        
    }
    public void addCellToGrid(ArrayList<String> headers, int col, int row) {
        Label label = new Label("  " + headers.get(col)+ "     ");
        sitePagesGridPane.add(label, col, row);
        // AND ALSO KEEP IN IN CASE WE NEED TO STYLIZE IT
    }
    public void addLabelsToGrid(ArrayList<String> headers, int col, int row) {
        Label label = new Label("  " + headers.get(row - 1)+ "     ");
        sitePagesGridPane.add(label, col, row);
        // AND ALSO KEEP IN IN CASE WE NEED TO STYLIZE IT
    }
    public void initChoiceBoxes(String subject, String number, String semester, String year){
        subjectChoiceBox.setValue(subject);
        numberChoiceBox.setValue(number);
        semesterChoiceBox.setValue(semester);
        yearChoiceBox.setValue(year);
    }
    public void initCourseInfo(String title, String instructorName, String instructorHome){
        titleTextField.setText(title);
        instructorNameTextField.setText(instructorName);
        instructorHomeTextField.setText(instructorHome);
    }
    public void initCheckBox(boolean home, boolean syllabus, boolean schedule, boolean hw, boolean project){
        homeTemplate.setUse(home);
        syllabusTemplate.setUse(syllabus);
        scheduleTemplate.setUse(schedule);
        hwTemplate.setUse(hw);
        projectsTemplate.setUse(project);
    }
    public void initPageStyle(String schoolPath, String leftPath, String rightPath, String stylesheet){
        stylesheetChoiceBox.setValue(stylesheet);
        schoolImageDirectory = schoolPath;
        leftImageDirectory = leftPath;
        rightImageDirectory = rightPath;
    }
    public ScrollPane getCourseDetailsSplitPane()
    {
        return finalPane;
    }
    public void setExportDirectory(String path)
    {
        actualExportDirectoryLabel.setText(path);
    }
    public Label getCourseInfolabel()
    {
        return courseInfoLabel;
    }
    public Label getSubjectLabel()
    {
        return subjectLabel;
    }
    public Label getSemesterLabel(){
        return semesterLabel;
    }
    public Label getNumberLabel(){
        return numberLabel;
    }
    public Label getYearLabel(){
        return yearLabel;
    }
    public Label getTitleLabel(){
        return titleLabel;
    }
    public Label getInstructNameLabel()
    {
        return instructNameLabel;
    }
    public Label getInstructHomeLabel()
    {
        return instructHomeLabel;
    }
    public Label getexportDirLabel(){
        return exportDirLabel;
    }
    public Label actualExportDirectoryLabel(){
        return actualExportDirectoryLabel;
    }
    public Label getSiteTemplateLabel(){
        return siteTemplateLabel;
    }
    public Label getSiteTemplateTextLabel(){
        return siteTemplateTextLabel;
    }
    public Label getTemplateDirectoryLabel()
    {
        return templateDirectoryLabel;
    }
    public Label getSitePagesLabel(){
        return sitePagesLabel;
    }
    public Label getPageStyleLabel(){
        return pageStyleLabel;
    }
    public Label getSchoolImageLabel(){
        return schoolImageLabel;
    }
    public Label getLeftFooterLabel(){
        return leftFooterLabel;
    }
    public Label getRightFooterLabel(){
        return rightFooterLabel;
    }
    public Label getStylesheetLabel(){
        return stylesheetLabel;
    }
    public String getSubjectChoiceBox(){
        return (String)subjectChoiceBox.getValue();         
    }
    public String getNumberChoiceBox(){
        return (String)numberChoiceBox.getValue();         
    }
    public String getSemesterChoiceBox(){
        return (String)semesterChoiceBox.getValue();         
    }
    public String getYearChoiceBox(){
        return (String)yearChoiceBox.getValue();         
    }
    public String getStylesheet(){
        return (String)stylesheetChoiceBox.getValue();         
    }
    public String getCourseTitle(){
        return (String)titleTextField.getText();
    }
    public String getInstructorName(){
        return (String)instructorNameTextField.getText();
    }
    public String getInstructorHome(){
        return (String)instructorHomeTextField.getText();
    }
    public String getTitle(){
        return (String)titleTextField.getText();
    }
    public void setTitle(String title){
        titleTextField.setText(title);
    }
    public TextField getTitleTextField(){
        return titleTextField;
    }
    public TextField getInstructorHomeTextField(){
        return instructorHomeTextField;
    }
    public TextField getInstructorNameTextField(){
        return instructorNameTextField;
    }
    
    public String getActualDirectory(){
        return (String)actualExportDirectoryLabel.getText();
    }
    public String getSiteTemplateDirectory(){
        return (String)templateDirectoryLabel.getText();
    }
    public String getSchoolDirectory(){
        return schoolImageDirectory;
    }
    public void setSchoolDirectory(String directory){
        schoolImageDirectory = "file:" + directory;
        schoolImage = new Image(schoolImageDirectory, 250, 100, false, false);
        schoolImagePane.getChildren().clear();
        schoolImageView = new ImageView(schoolImage);
        schoolImagePane.getChildren().add(schoolImageView);
        
        
    }
    public void initStylesheetChoiceBox(){
        
    }
    public String getLeftDirectory(){
        return leftImageDirectory;
    }
    public void setLeftDirectory(String directory)
    {
        leftImageDirectory = "file:" + directory;
        leftImage = new Image(leftImageDirectory, 250, 100, false, false);
        leftImagePane.getChildren().clear();
        leftImageView = new ImageView(leftImage);
        leftImagePane.getChildren().add(leftImageView);
    }
    public String getRightDirectory(){
        return rightImageDirectory;
    }
    public void setRightDirectory(String directory){
        rightImageDirectory = "file:" + directory;
        rightImage = new Image(rightImageDirectory, 250, 100, false, false);
        rightImagePane.getChildren().clear();
        rightImageView = new ImageView(rightImage);
        rightImagePane.getChildren().add(rightImageView);
    }
    public boolean getHomeCheckBoolean(){
        return homeTemplate.getUse();
    }
    public boolean getSyllabusCheckBoolean(){
        return syllabusTemplate.getUse();
    }
    public boolean getScheduleCheckBoolean(){
        return scheduleTemplate.getUse();
    }
    public boolean getHWCheckBoolean(){
        return hwTemplate.getUse();
    }
    public boolean getProjectsCheckBoolean(){
        return projectsTemplate.getUse();
    }
    public void resetCourseDetails(){
        titleTextField.clear();
        instructorNameTextField.clear();
        instructorHomeTextField.clear();
        subjectChoiceBox.getSelectionModel().selectFirst();
        numberChoiceBox.getSelectionModel().selectFirst();
        semesterChoiceBox.getSelectionModel().selectFirst();
        yearChoiceBox.getSelectionModel().selectFirst();
        stylesheetChoiceBox.getSelectionModel().selectFirst();
        homeTemplate.setUse(false);
        syllabusTemplate.setUse(false);
        scheduleTemplate.setUse(false);
        hwTemplate.setUse(false);
        projectsTemplate.setUse(false);
        schoolImagePane.getChildren().clear();
        rightImagePane.getChildren().clear();
        leftImagePane.getChildren().clear();
        homeTemplate.setUse(false);
        syllabusTemplate.setUse(false);
        scheduleTemplate.setUse(false);
        hwTemplate.setUse(false);
        projectsTemplate.setUse(false);
        
    }
   
    
}
