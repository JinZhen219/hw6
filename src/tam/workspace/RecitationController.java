/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.workspace;

import djf.ui.AppGUI;
import djf.ui.AppMessageDialogSingleton;
import javafx.scene.control.TableView;
import javafx.scene.input.KeyCode;
import jtps.jTPS;
import jtps.jTPS_Transaction;
import properties_manager.PropertiesManager;
import tam.CSGManagerProp;
import static tam.CSGManagerProp.RECITATION_EXIST_ERROR;
import tam.TAManagerApp;
import static tam.TAManagerProp.TA_NAME_AND_EMAIL_NOT_UNIQUE_MESSAGE;
import static tam.TAManagerProp.TA_NAME_AND_EMAIL_NOT_UNIQUE_TITLE;
import tam.data.Recitation;
import tam.data.Schedule;
import tam.data.TAData;
import tam.data.TeachingAssistant;
import static tam.workspace.TAController.jTPS;
/**
 *
 * @author Jin
 */
public class RecitationController {
    TAManagerApp app;
    TAData data;
    
     /*
     * 
     * Constructor, note that the app must already be constructed.
     */
    public RecitationController(TAManagerApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;
        data = (TAData)app.getDataComponent();
    }
    public void handleAddUpdate(){
        
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        recitationDataTabContent recitationTab = workspace.getRecitationTabContent();
        
        try{
            if(recitationTab.update == true){
                Object selectedItem = recitationTab.recitationTable.getSelectionModel().getSelectedItem();
                Recitation rec = (Recitation) selectedItem;
                System.out.println("Updating recitation " + rec.getSection());

                // IF IT REACHES BELOW THEN IT IS UPDATING
//                data.editRecitation(rec, workspace);
                jTPS_Transaction trans = new EditRecitationTransaction(rec, data);
                jTPS.addTransaction(trans);
                markWorkAsEdited();
                handleClear();
            }
            else throw new Exception();
        }
        catch(Exception e)
        {
            System.out.println("adding recitation");
            if(data.containRecitation(recitationTab.sectionTextField.getText()) == true)
            {
                PropertiesManager props = PropertiesManager.getPropertiesManager();
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(CSGManagerProp.RECITATION_EXIST_ERROR), props.getProperty(CSGManagerProp.RECITATION_EXIST_ERROR));
            
            }
            else{
                try{
//                    data.addRecitation(recitationTab.sectionTextField.getText(), recitationTab.instructorTextField.getText(), recitationTab.dayTimeTextField.getText(),
//                                                        recitationTab.locationTextField.getText(), recitationTab.ta1ChoiceBox.getValue().toString(), recitationTab.ta2ChoiceBox.getValue().toString());
                    jTPS_Transaction trans = new AddRecitationTransaction(recitationTab, data);
                    jTPS.addTransaction(trans);
//                    handleClear();
                    markWorkAsEdited();
                }
                catch(NullPointerException ex){
                    PropertiesManager props = PropertiesManager.getPropertiesManager();
                    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                    dialog.show(props.getProperty(CSGManagerProp.MISSING_INFO_ERROR), props.getProperty(CSGManagerProp.MISSING_INFO_ERROR));
                }
            }
        }
        
    }
    public void handleClear(){
            TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
            recitationDataTabContent recitationTab = workspace.getRecitationTabContent();
            recitationTab.sectionTextField.clear();
            recitationTab.instructorTextField.clear();
            recitationTab.dayTimeTextField.clear();
            recitationTab.locationTextField.clear();
            recitationTab.ta1ChoiceBox.setValue(null);
            recitationTab.ta2ChoiceBox.setValue(null);
            recitationTab.sectionTextField.requestFocus();
            recitationTab.update = false;
    }
    public void markWorkAsEdited() {
        // MARK WORK AS EDITED
        AppGUI gui = app.getGUI();
        gui.getFileController().markAsEdited(gui);
    }    
    public void handleDelete(KeyCode code){
        if(code == KeyCode.DELETE){
            TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
            recitationDataTabContent recitationTab = workspace.getRecitationTabContent();
            
            TableView recitationTable = recitationTab.recitationTable;
            Object selectedItem = recitationTable.getSelectionModel().getSelectedItem();
            if (selectedItem != null) {
                Recitation rec = (Recitation) selectedItem;
                TAData data = (TAData) app.getDataComponent();
                
                jTPS_Transaction transaction1 = new DeleteRecitationTransaction(rec, data);
                jTPS.addTransaction(transaction1);
//                handleClear();
                markWorkAsEdited();
            }
        }
    }
    
}
