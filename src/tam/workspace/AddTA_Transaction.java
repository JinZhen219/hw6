
package tam.workspace;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableView;
import jtps.jTPS_Transaction;
import tam.data.TAData;
import tam.TAManagerApp;
import tam.data.Recitation;
import tam.workspace.TAWorkspace;

/**
 *
 * @author Jin
 */
public class AddTA_Transaction implements jTPS_Transaction
{
    
    
    private String name;
    private String email;
    private TAData data;
    private TAWorkspace workspace;
    private recitationDataTabContent recitationTab;

    public AddTA_Transaction(String initName, String initEmail, TAData initData)
    {
        name = initName;
        email = initEmail;
        data = initData;
        workspace = data.getWorkspace();
        recitationTab = workspace.getRecitationTabContent();
//        recitationList = data.getRecitations();
//        modifiedRecitationList = FXCollections.observableArrayList();
    }
    @Override 
    public void doTransaction()
    { 
        data.addTA(name,email, false);  
        recitationTab.ta1ChoiceBox.getItems().add(name);
        recitationTab.ta2ChoiceBox.getItems().add(name);
        workspace.nameTextField.setText("");
        workspace.emailTextField.setText("");

    }
    @Override 
    public void undoTransaction()
    {
        data.removeTA(name);
        recitationTab.ta1ChoiceBox.getItems().remove(name);
        recitationTab.ta2ChoiceBox.getItems().remove(name);
        workspace.nameTextField.setText("");
        workspace.emailTextField.setText("");

    }
}
