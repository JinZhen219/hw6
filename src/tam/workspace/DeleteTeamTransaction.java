/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.workspace;

import java.util.ArrayList;
import java.util.HashMap;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import jtps.jTPS_Transaction;
import tam.data.Student;
import tam.data.TAData;
import tam.data.Team;

/**
 *
 * @author khurr
 */
public class DeleteTeamTransaction implements jTPS_Transaction {

    private Team team;
    private TAData data;
    private ObservableList<Student> removedStudentList = FXCollections.observableArrayList();
    public DeleteTeamTransaction(Team team, TAData data) {
        
        this.data = data;
        this.team = team;
    }

    @Override
    public void doTransaction() {  //control Y 
        
        String teamName = team.getName();
        data.removeTeam(teamName);
        ObservableList<Student> studentsList = data.getStudents();
        ObservableList<Student> studentsList2 = FXCollections.observableArrayList(studentsList);
        for (Student stu : studentsList2) {           
            if (teamName.equals(stu.getTeam())) {
                studentsList.remove(stu);
                removedStudentList.add(stu);
            }
        }
        data.getWorkspace().projectTabContent.teamChoiceBox.getItems().remove(teamName);
    }

    @Override
    public void undoTransaction() {        
        data.addTeam(team.getName(), team.getColor(), team.getTextColor(), team.getLink());
        for(Student student : removedStudentList){
            data.addStudent(student.getFirstName(), student.getLastName(), student.getTeam(), student.getRole());
        }
        removedStudentList.clear();
        data.getWorkspace().projectTabContent.teamChoiceBox.getItems().remove(team.getName());
    }
}
