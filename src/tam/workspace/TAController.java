package tam.workspace;

import djf.ui.AppGUI;
import djf.ui.AppMessageDialogSingleton;
import djf.ui.AppYesNoCancelDialogSingleton;
import static tam.TAManagerProp.*;
import djf.ui.AppYesNoDialogSingleton;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import jtps.jTPS;
import jtps.jTPS_Transaction;
import properties_manager.PropertiesManager;
import tam.TAManagerApp;
import tam.data.Recitation;
import tam.data.TAData;
import tam.data.TeachingAssistant;
import tam.file.TimeSlot;
import tam.style.TAStyle;
import static tam.style.TAStyle.CLASS_HIGHLIGHTED_GRID_CELL;
import static tam.style.TAStyle.CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN;
import static tam.style.TAStyle.CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE;
import tam.workspace.TAWorkspace;

/**
 * This class provides responses to all workspace interactions, meaning
 * interactions with the application controls not including the file toolbar.
 *
 * @author Richard McKenna
 * @version 1.0
 */
public class TAController {

    // THE APP PROVIDES ACCESS TO OTHER COMPONENTS AS NEEDED
    TAManagerApp app;
    static jTPS jTPS = new jTPS();
    boolean conflictTimes;
    ObservableList<Recitation> modifiedRecitationList = FXCollections.observableArrayList();
    /**
     * Constructor, note that the app must already be constructed.
     */
    public TAController(TAManagerApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;
    }

    /**
     * This helper method should be called every time an edit happens.
     */
    public void markWorkAsEdited() {
        // MARK WORK AS EDITED
        AppGUI gui = app.getGUI();
        gui.getFileController().markAsEdited(gui);
    }
    public jTPS getJTPS(){
        return jTPS;
    }
    /**
     * This method responds to when the user requests to add a new TA via the
     * UI. Note that it must first do some validation to make sure a unique name
     * and email address has been provided.
     */
    public void handleAddTA(KeyCode code) {
        // WE'LL NEED THE WORKSPACE TO RETRIEVE THE USER INPUT VALUES
        if(code == KeyCode.ENTER){
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        System.out.println("Update : " + workspace.getUpdate());
        
        if(workspace.getUpdate() == true)
        {
            TableView taTable = workspace.getTATable();
            Object selectedItem = taTable.getSelectionModel().getSelectedItem();
            TeachingAssistant ta = (TeachingAssistant) selectedItem;
            System.out.println(ta.getName());
            workspace.setOldName(ta.getName());
            workspace.setOldEmail(ta.getEmail());
            handleUpdateTA();
        }
        else{
            TextField nameTextField = workspace.getNameTextField();
            TextField emailTextField = workspace.getEmailTextField();
            String name = nameTextField.getText();
            String email = emailTextField.getText();
            EmailValidator checkEmail = new EmailValidator();

            // WE'LL NEED TO ASK THE DATA SOME QUESTIONS TOO
            TAData data = (TAData) app.getDataComponent();

            // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
            PropertiesManager props = PropertiesManager.getPropertiesManager();

            // DID THE USER NEGLECT TO PROVIDE A TA NAME?
            if (name.isEmpty()) {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(MISSING_TA_NAME_TITLE), props.getProperty(MISSING_TA_NAME_MESSAGE));
            } // DID THE USER NEGLECT TO PROVIDE A TA EMAIL?
            else if (email.isEmpty()) {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(MISSING_TA_EMAIL_TITLE), props.getProperty(MISSING_TA_EMAIL_MESSAGE));
            } // DOES A TA ALREADY HAVE THE SAME NAME OR EMAIL?
            else if (data.containsTA(name, email)) {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_TITLE), props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_MESSAGE));
            } // **********Check the TA Email Address for correct format 
            else if (!checkEmail.validate(email)) {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(INVALID_TA_EMAIL_TITLE), props.getProperty(INVALID_TA_EMAIL_MESSAGE));

            } // EVERYTHING IS FINE, ADD A NEW TA
            else {
                // ADD THE NEW TA TO THE DATA
                //data.addTA(name, email);
                jTPS_Transaction transaction1 = new AddTA_Transaction(name, email, data);
                boolean undergrad;
                try{
                    undergrad = data.getTA(name).getUndergraduate();
                }
                catch(Exception e){
                    undergrad = false;
                }
                
                jTPS.addTransaction(transaction1);
                //MIGHT POSE A PROBLEM LATER
//                data.addTA(name, email, undergrad);
                // CLEAR THE TEXT FIELDS               
//                recitationDataTabContent recitationTab = workspace.getRecitationTabContent();
//                recitationTab.ta1ChoiceBox.getItems().add(name);
//                recitationTab.ta2ChoiceBox.getItems().add(name);
                // AND SEND THE CARET BACK TO THE NAME TEXT FIELD FOR EASY DATA ENTRY
                nameTextField.requestFocus();
                // WE'VE CHANGED STUFF
                markWorkAsEdited();
            }
        }
        }
    }

    /**
     * This function provides a response for when the user presses a keyboard
     * key. Note that we're only responding to Delete, to remove a TA.
     *
     * @param code The keyboard code pressed.
     */
    public void handleKeyPress(KeyCode code) {
        // DID THE USER PRESS THE DELETE KEY?
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        if (code == KeyCode.DELETE) {
            // GET THE TABLE
            TableView taTable = workspace.getTATable();

            // IS A TA SELECTED IN THE TABLE?
            Object selectedItem = taTable.getSelectionModel().getSelectedItem();
            if (selectedItem != null) {
                TeachingAssistant ta = (TeachingAssistant) selectedItem;
                String taName = ta.getName();
                TAData data = (TAData) app.getDataComponent();
                ArrayList<TimeSlot> officeHours = TimeSlot.buildOfficeHoursList(data);
                
                // AND BE SURE TO REMOVE ALL THE TA'S OFFICE HOURS
                HashMap<String, Label> labels = workspace.getOfficeHoursGridTACellLabels();

                
                jTPS_Transaction transaction1 = new deleteTA(ta, officeHours, data, workspace, labels);
                jTPS.addTransaction(transaction1);


            }
        }
        
        if(code == KeyCode.ENTER && workspace.getUpdate() == true)
            ;
    }

    public void handleUndoTransaction() {
        jTPS.undoTransaction();
        markWorkAsEdited();
    }

    public void handleReDoTransaction() {
        jTPS.doTransaction();
        markWorkAsEdited();
    }

    /**
     * This function provides a response for when the user clicks on the office
     * hours grid to add or remove a TA to a time slot.
     *
     * @param pane The pane that was toggled.
     */
    public void handleCellToggle(Pane pane) {
  
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TableView taTable = workspace.getTATable();
        
        // IS A TA SELECTED IN THE TABLE?
        Object selectedItem = taTable.getSelectionModel().getSelectedItem();
        if (selectedItem != null) {
            // GET THE TA
            TeachingAssistant ta = (TeachingAssistant)selectedItem;
            String taName = ta.getName();
            TAData data = (TAData)app.getDataComponent();
            String cellKey = pane.getId();
            
            // AND TOGGLE THE OFFICE HOURS IN THE CLICKED CELL
//            data.toggleTAOfficeHours(cellKey, taName);
            jTPS_Transaction cellTransaction = new ToggleTa_Transaction(cellKey, taName,data);
            jTPS.addTransaction(cellTransaction);
            taTable.refresh();
            // WE'VE CHANGED STUFF
            markWorkAsEdited();
        }
    }

    public void handleTaClicked(Pane pane, Pane addBox) {
        // GET THE TABLE
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        TableView taTable = workspace.getTATable();

        // IS A TA SELECTED IN THE TABLE?
        Object selectedItem = taTable.getSelectionModel().getSelectedItem();

        if (selectedItem != null) {
            //selectedItem = taTable.getSelectionModel().getSelectedItem();
            workspace.getClearButton1().setDisable(false);
            
            workspace.getClearButton().setDisable(false);
            TeachingAssistant ta = (TeachingAssistant) selectedItem;
            // workspace.nameTextField.clear();
            //workspace.emailTextField.clear(); 
            addBox.getChildren().clear();
            // addBox.getChildren().remove(workspace.addButton); 
            // addBox.getChildren().remove(workspace.clearButton1); 
            addBox.getChildren().add(workspace.nameTextField);
            addBox.getChildren().add(workspace.emailTextField);
            addBox.getChildren().add(workspace.updateTaButton);
            addBox.getChildren().add(workspace.clearButton);
            workspace.setUpdate(true);
            // GET THE TA
            String taName = ta.getName();
            String taEmail = ta.getEmail();
            TAData data = (TAData) app.getDataComponent();

            // SET TextField To TA NAME 
            workspace.nameTextField.setText(taName);
            workspace.emailTextField.setText(taEmail);
            // workspace.updateTaButton.setOnAction(e -> {
            //handleUpdateTA(taName,taEmail,ta);

            // });
            //markWorkAsEdited();   
        }
    }

    public void handleUpdateTA() {
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TextField nameTextField = workspace.getNameTextField();
        TextField emailTextField = workspace.getEmailTextField();
        String name = nameTextField.getText();
        String email = emailTextField.getText();
        EmailValidator emailValidator = new EmailValidator();
        // WE'LL NEED TO ASK THE DATA SOME QUESTIONS TOO
        TAData data = (TAData)app.getDataComponent();
        
        // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        System.out.println(workspace.getUpdate());

        // DID THE USER NEGLECT TO PROVIDE A TA NAME?
        if (name.isEmpty()) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_TA_NAME_TITLE), props.getProperty(MISSING_TA_NAME_MESSAGE));            
        }
        // DID THE USER NEGLECT TO PROVIDE A TA EMAIL?
        else if (email.isEmpty()) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_TA_EMAIL_TITLE), props.getProperty(MISSING_TA_EMAIL_MESSAGE));                        
        }
        // DOES A TA ALREADY HAVE THE SAME NAME OR EMAIL?
        else if (data.containsTA(name, email) && workspace.getUpdate() == false) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_TITLE), props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_MESSAGE));                                    
        }
        else if (emailValidator.validate(email) == false)
        {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(INVALID_TA_EMAIL_MESSAGE), props.getProperty(INVALID_TA_EMAIL_MESSAGE));                                 
        }
        else{

            boolean undergrad;
            try{
                undergrad = data.getTA(workspace.getOldName()).getUndergraduate();
            }
            catch(NullPointerException e){
                undergrad = false;
            }
            data.removeTA(workspace.getOldName());

            
            if(data.containsTA(name,email) == true)
            {
                //        PropertiesManager props = PropertiesManager.getPropertiesManager();
                System.out.println("failed");
                System.out.println(workspace.getOldName());
                System.out.println(name);
                data.addTA(workspace.getOldName(),workspace.getOldEmail(),undergrad );
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_TITLE), props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_MESSAGE));
                workspace.getNameTextField().requestFocus();
                workspace.getNameTextField().clear();
                workspace.getEmailTextField().clear();
                workspace.getTATable().refresh();    
            }
            else{
                data.addTA(workspace.getOldName(), workspace.getOldEmail(), undergrad);
                jTPS_Transaction editTransaction = new editTATransaction(name, email, data, workspace);
                jTPS.addTransaction(editTransaction);

                markWorkAsEdited();
                        
                
            }
    
}
            

        }

    public void handleTaTableRefresh() {
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        TableView taTable = workspace.getTATable();
        taTable.refresh();
    }

    public void handleUpdateTaGrid(String taName, String newName) {

        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        TAData data = (TAData) app.getDataComponent();
        //data.removeTA(taName);

        // AND BE SURE TO REMOVE ALL THE TA'S OFFICE HOURS
        HashMap<String, Label> labels = workspace.getOfficeHoursGridTACellLabels();

        for (Label label : labels.values()) {   //iterates thourhg the hashmap to find all occurences of orgTA in the office hour grid
            if (label.getText().equals(taName)
                    || (label.getText().contains(taName + "\n"))
                    || (label.getText().contains("\n" + taName))) {
                data.renameTaCell(label.textProperty(), taName, newName);
            }
        }
        TableView taTable = workspace.getTATable();
        Collections.sort(data.getTeachingAssistants());  //sorts the teachingAssistants List 
        taTable.refresh();

        markWorkAsEdited();

    }

    void handleGridCellMouseExited(Pane pane) {
        String cellKey = pane.getId();
        TAData data = (TAData) app.getDataComponent();
        int column = Integer.parseInt(cellKey.substring(0, cellKey.indexOf("_")));
        int row = Integer.parseInt(cellKey.substring(cellKey.indexOf("_") + 1));
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();

        Pane mousedOverPane = workspace.getTACellPane(data.getCellKey(column, row));
        mousedOverPane.getStyleClass().clear();
        mousedOverPane.getStyleClass().add(CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE);

        // THE MOUSED OVER COLUMN HEADER
        Pane headerPane = workspace.getOfficeHoursGridDayHeaderPanes().get(data.getCellKey(column, 0));
        headerPane.getStyleClass().remove(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);

        // THE MOUSED OVER ROW HEADERS
        headerPane = workspace.getOfficeHoursGridTimeCellPanes().get(data.getCellKey(0, row));
        headerPane.getStyleClass().remove(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        headerPane = workspace.getOfficeHoursGridTimeCellPanes().get(data.getCellKey(1, row));
        headerPane.getStyleClass().remove(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);

        // AND NOW UPDATE ALL THE CELLS IN THE SAME ROW TO THE LEFT
        for (int i = 2; i < column; i++) {
            cellKey = data.getCellKey(i, row);
            Pane cell = workspace.getTACellPane(cellKey);
            cell.getStyleClass().remove(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
            cell.getStyleClass().add(CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE);
        }

        // AND THE CELLS IN THE SAME COLUMN ABOVE
        for (int i = 1; i < row; i++) {
            cellKey = data.getCellKey(column, i);
            Pane cell = workspace.getTACellPane(cellKey);
            cell.getStyleClass().remove(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
            cell.getStyleClass().add(CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE);
        }
    }

    void handleGridCellMouseEntered(Pane pane) {
        String cellKey = pane.getId();
        TAData data = (TAData) app.getDataComponent();
        int column = Integer.parseInt(cellKey.substring(0, cellKey.indexOf("_")));
        int row = Integer.parseInt(cellKey.substring(cellKey.indexOf("_") + 1));
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();

        // THE MOUSED OVER PANE
        Pane mousedOverPane = workspace.getTACellPane(data.getCellKey(column, row));
        mousedOverPane.getStyleClass().clear();
        mousedOverPane.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_CELL);

        // THE MOUSED OVER COLUMN HEADER
        Pane headerPane = workspace.getOfficeHoursGridDayHeaderPanes().get(data.getCellKey(column, 0));
        headerPane.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);

        // THE MOUSED OVER ROW HEADERS
        headerPane = workspace.getOfficeHoursGridTimeCellPanes().get(data.getCellKey(0, row));
        headerPane.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        headerPane = workspace.getOfficeHoursGridTimeCellPanes().get(data.getCellKey(1, row));
        headerPane.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);

        // AND NOW UPDATE ALL THE CELLS IN THE SAME ROW TO THE LEFT
        for (int i = 2; i < column; i++) {
            cellKey = data.getCellKey(i, row);
            Pane cell = workspace.getTACellPane(cellKey);
            cell.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        }

        // AND THE CELLS IN THE SAME COLUMN ABOVE
        for (int i = 1; i < row; i++) {
            cellKey = data.getCellKey(column, i);
            Pane cell = workspace.getTACellPane(cellKey);
            cell.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        }
    }

    void handleChangeTime(String startTime, String endTime) {
        //TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        AppYesNoDialogSingleton yesNoDialog = AppYesNoDialogSingleton.getSingleton();
        int start = convertToMilitaryTime(startTime);
            int end = convertToMilitaryTime(endTime);
            System.out.println(start);

            //TAWorkspace workspace = (TAWorkspace)app.getDataComponent();
            if (start == end || start == -1 || end == -1) {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(INVALID_TIME_INPUT_TITLE), props.getProperty(INVALID_TIME_INPUT_MESSAGE));       //REMEMBER TO CHANGE TO PROPER ERROR MESSAGE                              

            } else if (start > end) {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(INVALID_TIME_INPUT_TITLE), props.getProperty(INVALID_TIME_INPUT_MESSAGE));       //REMEMBER TO CHANGE TO PROPER ERROR MESSAGE                              

            } else if (end < start) {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(INVALID_TIME_INPUT_TITLE), props.getProperty(INVALID_TIME_INPUT_MESSAGE));       //REMEMBER TO CHANGE TO PROPER ERROR MESSAGE                              

            } else {
        conflictTimes = handleConflictTimes();
        String selection;
        if(conflictTimes == true)
        {
            yesNoDialog.show(props.getProperty(UPDATE_TIME_TITLE), props.getProperty(UPDATE_TIME_MESSAGE));
            selection = yesNoDialog.getSelection();
        }
        else{
            selection = AppYesNoDialogSingleton.YES;
        }
        // AND NOW GET THE USER'S SELECTION
        
        if (selection.equals(AppYesNoDialogSingleton.YES)) {

                //At this point the time varialbes are good to go. 
                TAData data = (TAData) app.getDataComponent();

                jTPS_Transaction transaction = new updateTime_Transaction(start, end, data);
                jTPS.addTransaction(transaction);

                //workspace.resetWorkspace(); 
                //workspace.reloadWorkspace(oldData);
                markWorkAsEdited();
                //workspace.reloadOfficeHoursGrid(data);
        }
        }

    }

    public int convertToMilitaryTime(String time) {
        int milTime = 0;
        if (time == null) {
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(INVALID_TA_EMAIL_TITLE), props.getProperty(INVALID_TA_EMAIL_MESSAGE));       //REMEMBER TO CHANGE TO PROPER ERROR MESSAGE                              
        } else if (time.equalsIgnoreCase("12:00pm")) {
            milTime = 12;
        } 
        else if(time.equalsIgnoreCase("12:00am"))
        {
            milTime = 0;
        }
        else {
            int index = time.indexOf(":");
            String subStringTime = time.substring(0, index);
            milTime = Integer.parseInt(subStringTime);
            if (time.contains("p")) {
                milTime += 12;
            }
        }
        return milTime;
    }
    public boolean handleConflictTimes() 
    {
            TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();  
            AppYesNoCancelDialogSingleton yesNoDialog = AppYesNoCancelDialogSingleton.getSingleton();


            TAData data = (TAData)app.getDataComponent();
            ArrayList<TimeSlot> officeHours = TimeSlot.buildOfficeHoursList(data);

            ChoiceBox box = workspace.getNewStartBox();
            String start = String.valueOf(box.getValue());
            String end = String.valueOf(workspace.getNewEndBox().getValue());
            
            //12:00 or 12:30
            int hour = 0;
            int minutes = 0;
            int endHour = 0;
            int endMinutes = 0;
            boolean invalid = false;
            String timeOfDayStart = "";
            String timeOfDayEnd = "";
            int gridStart = data.getStartHour();
            int gridEnd = data.getEndHour();
            
            
            if(start.length() == 7)
            {
                hour = Integer.parseInt(start.substring(0,2)) * 100;
                minutes = Integer.parseInt(start.substring(3,5));
                timeOfDayStart = start.substring(5,7);
                
                
            }
            else if (start.length() == 6)
            {
                hour = Integer.parseInt(start.substring(0,1)) * 100;
                minutes = Integer.parseInt(start.substring(2,4));
                timeOfDayStart = start.substring(4,6);
            }
 
            if(end.length() == 7)
            {
                endHour = Integer.parseInt(end.substring(0,2)) * 100;
                endMinutes = Integer.parseInt(end.substring(3,5));
                timeOfDayEnd = end.substring(5,7);
            }
            else if(end.length() == 6)
            {
                endHour = Integer.parseInt(end.substring(0,1)) * 100;
                endMinutes = Integer.parseInt(end.substring(2,4));
                timeOfDayEnd = end.substring(4,6);
            }
            int startTime = hour / 100;
            int endTime = endHour/100;
            int maxTime = 0;
            int minTime = 2400;
            //below checks if any hours are going to be removed
            for(TimeSlot ts : officeHours)
                {
                    String x = ts.getTime();
                    x = x.replace("_", "");
                    int y = Integer.parseInt(x.substring(0,x.length() - 2)) ;
                    if((x.substring(x.length()- 2, x.length()).equals("pm") && y != 1200))
                    {
                        if( (x.substring(x.length()- 2, x.length()).equals("pm") && y != 1230))
                            y +=1200;
                    }
                    if (y < minTime)
                        minTime = y;
                    if(y > maxTime)
                        maxTime = y;
                    
                }
            int j = hour + minutes;
            if(start.equals("12:00am"))
            {
                startTime = 0;
                j = 0;
            }
            if(end.equals("12:00am"))
            {
                endTime = 0;
            }
            
            if(start.contains("pm"))
                j += 1200;

            int l = endHour + endMinutes;
            if(end.contains("pm") && !end.contains("12"))
                l += 1200;
            if(l < maxTime || j > minTime)
            {
                
                conflictTimes = true;
            }
            return conflictTimes;
            
    }
}
