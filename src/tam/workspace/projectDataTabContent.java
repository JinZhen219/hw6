/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.workspace;

import djf.settings.AppPropertyType;
import static djf.settings.AppStartupConstants.FILE_PROTOCOL;
import static djf.settings.AppStartupConstants.PATH_IMAGES;
import tam.CSGManagerProp;
import java.util.ArrayList;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import properties_manager.PropertiesManager;
import tam.TAManagerApp;
import tam.data.Student;
import tam.data.TAData;
import tam.data.Team;
/**
 *
 * @author Jin
 */
public class projectDataTabContent 
{
    Label projects = new Label();
    ObservableList<String> teamGridHeaders;
    Label team = new Label();
    Button teamDeleteButton = new Button();
    Button studentDeleteButton = new Button();
    Label addEdit = new Label();
    Label addEditName = new Label();
    Label color = new Label();
    Label textColor = new Label();
    Label link = new Label();
    Label students = new Label();
    Label firstName = new Label();
    Label lastName = new Label();
    Label addEditTeam = new Label();
    Label role = new Label();
    Button addUpdateButton = new Button();
    Button clearButton = new Button();
    ColorPicker colorPicker = new ColorPicker();
    ColorPicker textColorPicker = new ColorPicker();
    TextField nameTextField = new TextField();
    TextField linkTextField = new TextField();
    TextField firstNameTextField = new TextField();
    TextField lastNameTextField = new TextField();
    ChoiceBox teamChoiceBox = new ChoiceBox();
    TextField roleTextField = new TextField();
    Button addUpdateButton2 = new Button();
    Button clearButton2 = new Button();

    TableView teamTable ;
    TableView studentTable;
    TableColumn teamNameColumn;
    TableColumn colorColumn;
    TableColumn  textColorColumn;
    TableColumn linkColumn;
    TableColumn firstNameColumn;
    TableColumn lastNameColumn;
    TableColumn teamColumn;
    TableColumn roleColumn;
    Label addEdit2 = new Label();
    TAManagerApp app;
    boolean teamUpdate = false;
    boolean studentUpdate = false;
    
    
    VBox projectTabContent;
    
    public projectDataTabContent(TAManagerApp initApp)
    {
        app = initApp;
        
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        ArrayList<String> studentGridHeaders = props.getPropertyOptionsList(CSGManagerProp.STUDENT_GRID_HEADERS);
        projects.setText(props.getProperty(CSGManagerProp.PROJECTS));
        team.setText(props.getProperty(CSGManagerProp.TEAM));

        String imagePath = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(AppPropertyType.DELETE_ICON);
        Image buttonImage = new Image(imagePath);
	
	// NOW MAKE THE BUTTON
        teamDeleteButton.setDisable(false);
        teamDeleteButton.setGraphic(new ImageView(buttonImage));
        
        String imagePath2 = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(AppPropertyType.DELETE_ICON);
        Image buttonImage2 = new Image(imagePath2);
	
	// NOW MAKE THE BUTTON
        studentDeleteButton.setDisable(false);
        studentDeleteButton.setGraphic(new ImageView(buttonImage2));
        addEdit.setText(props.getProperty(CSGManagerProp.ADD_EDIT));
        addEdit2.setText(props.getProperty(CSGManagerProp.ADD_EDIT));
        addEditName.setText(props.getProperty(CSGManagerProp.NAME_PROMPT_TEXT2) + ":");
        color.setText(props.getProperty(CSGManagerProp.COLOR) + ":");
        textColor.setText(props.getProperty(CSGManagerProp.TEXT_COLOR) + ":");
        link.setText(props.getProperty(CSGManagerProp.LINK));
        students.setText(props.getProperty(CSGManagerProp.STUDENTS));
        firstName.setText(props.getProperty(CSGManagerProp.FIRST_NAME));
        lastName.setText(props.getProperty(CSGManagerProp.LAST_NAME));
        
        addEditTeam.setText(studentGridHeaders.get(2) + ":");
        role.setText(props.getProperty(CSGManagerProp.ROLE));
        addUpdateButton.setText(props.getProperty(CSGManagerProp.ADD_UPDATE_BUTTON_TEXT));
        clearButton.setText(props.getProperty(CSGManagerProp.CLEAR_BUTTON_TEXT));
        nameTextField.setPromptText(props.getProperty(CSGManagerProp.NAME_PROMPT_TEXT2));
        linkTextField.setPromptText(props.getProperty(CSGManagerProp.LINK_PROMPT_TEXT));
        firstNameTextField.setPromptText(props.getProperty(CSGManagerProp.FIRST_NAME_PROMPT_TEXT));
        lastNameTextField.setPromptText(props.getProperty(CSGManagerProp.LAST_NAME_PROMPT_TEXT));
        roleTextField.setPromptText(props.getProperty(CSGManagerProp.ROLE_TEXT_FIELD));
        addUpdateButton2.setText(props.getProperty(CSGManagerProp.ADD_UPDATE_BUTTON_TEXT));
        clearButton2.setText(props.getProperty(CSGManagerProp.CLEAR_BUTTON_TEXT));

        
        
        nameTextField.setMinWidth(200);
        linkTextField.setMinWidth(400);
        firstNameTextField.setMinWidth(200);
        lastNameTextField.setMinWidth(200);
        roleTextField.setMinWidth(200);
        teamChoiceBox.setMinWidth(200);
        TAData data = (TAData) app.getDataComponent();
        
        data.addTeam("Atomic Comics", "552211", "fff fff", "http://atomicomin.com");
        data.addTeam("C4 Comics", "235399", "fff fff", "https://c4-comics-appspot.com");
        data.addStudent("Beau", "Brummel", "Atomic Comics", "Lead Designer");
        data.addStudent("Jane", "Doe", "C4 Comics", "Lead Programmer");
        data.addStudent("Moonian", "Soong", "Atomic Comics", "Data Designer");
        
        
        
        
        
        studentTable = new TableView();
        studentTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        
        ObservableList<Student> studentData = data.getStudents();
        studentTable.setItems(studentData);
        studentTable.setMaxWidth(850);
        firstNameColumn = new TableColumn(studentGridHeaders.get(0));
        lastNameColumn = new TableColumn(studentGridHeaders.get(1));
        teamColumn = new TableColumn(studentGridHeaders.get(2));
        roleColumn = new TableColumn(studentGridHeaders.get(3));

        firstNameColumn.setCellValueFactory(new PropertyValueFactory<Student, String>("firstName"));
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<Student, String>("lastName"));
        teamColumn.setCellValueFactory(new PropertyValueFactory<Student, String>("team"));
        roleColumn.setCellValueFactory(new PropertyValueFactory<Student, String>("role"));
        
        firstNameColumn.setMinWidth(200);
        lastNameColumn.setMinWidth(200);
        teamColumn.setMinWidth(200);
        roleColumn.setMinWidth(200);
        studentTable.getColumns().addAll(firstNameColumn, lastNameColumn, teamColumn, roleColumn);
        studentTable.setFocusTraversable(true);

        //NEED TO ADD TEAMTABLE AND FIX TADATA AND ADD IN GRADUATE CHECKBOX FOR TADATA
        
        teamTable = new TableView();
        teamTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        ObservableList<Team> teamData = data.getTeams();
        teamTable.setItems(teamData);
        ArrayList<String> teamGridHeaders = props.getPropertyOptionsList(CSGManagerProp.TEAM_GRID_HEADERS);
        teamNameColumn = new TableColumn(teamGridHeaders.get(0));
        colorColumn = new TableColumn(teamGridHeaders.get(1));
        textColorColumn = new TableColumn(teamGridHeaders.get(2));
        linkColumn = new TableColumn(teamGridHeaders.get(3));
        teamTable.setMaxWidth(900);
        teamNameColumn.setCellValueFactory(new PropertyValueFactory<Team, String>("name"));
        colorColumn.setCellValueFactory(new PropertyValueFactory<Team, String>("color"));
        textColorColumn.setCellValueFactory(new PropertyValueFactory<Team, String>("textColor"));
        linkColumn.setCellValueFactory(new PropertyValueFactory<Team, String>("link"));
        
        teamNameColumn.setMinWidth(200);
        colorColumn.setMinWidth(150);
        textColorColumn.setMinWidth(200);
        linkColumn.setMinWidth(300);
        teamTable.getColumns().addAll(teamNameColumn, colorColumn, textColorColumn, linkColumn);
        teamTable.setFocusTraversable(true);
        studentTable.setPrefHeight(200);
        teamTable.setPrefHeight(200);
        // ORGANIZING ALL INFORMATION INTO A GIANT PANE
        HBox teamLine = new HBox(team, teamDeleteButton );
        teamLine.setSpacing(15);
        HBox nameLine = new HBox(addEditName, nameTextField);
        addEditName.setPrefWidth(100);
        HBox colorLine = new HBox(color, colorPicker);
        color.setPrefWidth(100);
        textColorPicker.setMinHeight(30);
        colorPicker.setMinHeight(30);
        HBox textColorLine = new HBox(textColor, textColorPicker);
        textColor.setPrefWidth(100);
        HBox bothColorPickers = new HBox(colorLine, textColorLine);
        bothColorPickers.setSpacing(70);
        HBox linkLine = new HBox(link, linkTextField);
        link.setPrefWidth(100);
        HBox buttonLine1 = new HBox( addUpdateButton, clearButton);
        buttonLine1.setSpacing(20);
        HBox studentLine = new HBox(students, studentDeleteButton);
        studentLine.setSpacing(15);
        VBox studentTableBox = new VBox(studentLine , studentTable);
        studentTableBox.setSpacing(13);
        
        HBox firstNameLine = new HBox(firstName, firstNameTextField);
        firstName.setPrefWidth(100);
        HBox lastNameLine = new HBox(lastName, lastNameTextField);
        lastName.setPrefWidth(100);
        HBox teamLine2 = new HBox(addEditTeam, teamChoiceBox);
        addEditTeam.setPrefWidth(100);
        HBox roleLine = new HBox(role, roleTextField);
        role.setPrefWidth(100);
        HBox buttonLine2 = new HBox(addUpdateButton2, clearButton2);
        buttonLine2.setSpacing(20);
        
        VBox addEditBottomVBox = new VBox(addEdit, firstNameLine, lastNameLine, teamLine2, roleLine, buttonLine2);
        addEditBottomVBox.setSpacing(15);
        
        VBox bottomHalf = new VBox(studentTableBox, addEditBottomVBox);
        bottomHalf.setSpacing(8);
        VBox teamTableBox = new VBox(teamLine, teamTable );
        teamTableBox.setSpacing(5);
        VBox addEditUpperHalfVBox = new VBox(addEdit2, nameLine, bothColorPickers,linkLine, buttonLine1 );
        addEditUpperHalfVBox.setSpacing(15);
        VBox upperHalf = new VBox(teamTableBox, addEditUpperHalfVBox);
        upperHalf.setSpacing(20);
        VBox panee = new VBox(upperHalf, bottomHalf);
        panee.setSpacing(9);
        panee.setStyle("-fx-background-color : bisque");
        upperHalf.setStyle("-fx-background-color : #f4f4f4");
        bottomHalf.setStyle("-fx-background-color: #f4f4f4");
        SplitPane grayPane = new SplitPane(panee);
        grayPane.setPadding(new Insets(10, 10, 10, 10));
        grayPane.setOrientation(Orientation.VERTICAL);
        grayPane.setStyle("-fx-background-color: bisque");
        
        bottomHalf.setPadding(new Insets(5,0,0,0));
        ScrollPane finalPane = new ScrollPane(grayPane);
        finalPane.setFitToWidth(true);
        finalPane.setStyle("-fx-background-color : bisque");
        upperHalf.setPadding(new Insets(5,5,5,5));
        bottomHalf.setPadding(new Insets(5,5,5,5));
        projectTabContent = new VBox(projects, finalPane);
        projectTabContent.setPadding(new Insets(10,10,10,10));

        projectTabContent.setStyle("-fx-background-color: bisque");
        ProjectController controller = new ProjectController(app);
        
        
        teamTable.setOnMouseClicked(e->{
            try{
                Object selectedItem = teamTable.getSelectionModel().getSelectedItem();
                Team team = (Team)selectedItem;
                nameTextField.setText(team.getName());
                linkTextField.setText(team.getLink());

                colorPicker.setValue(Color.web(team.getColor().replaceAll("\\s+", "")));
                textColorPicker.setValue(Color.web(team.getTextColor().replaceAll("\\s+", "")));
                teamUpdate = true;
            }
            catch(NullPointerException ee){}
        });
        
        studentTable.setOnMouseClicked(e->{
            try{
                Object selectedItem = studentTable.getSelectionModel().getSelectedItem();
                Student student = (Student)selectedItem;
                firstNameTextField.setText(student.getFirstName());
                lastNameTextField.setText(student.getLastName());
                roleTextField.setText(student.getRole());
                teamChoiceBox.setValue(student.getTeam());
                studentUpdate = true;
            }
            catch(NullPointerException ee){}
            
        });
        clearButton.setOnMouseClicked(e->{
            controller.handleTeamClear();
        });
        clearButton2.setOnMouseClicked(e->{
            controller.handleStudentClear();
        });
        addUpdateButton.setOnMouseClicked(e->{
            controller.handleTeamAddUpdate();
        });
        addUpdateButton2.setOnMouseClicked(e->{
            controller.handleStudentAddUpdate();
        });
        teamDeleteButton.setOnMouseClicked(e->{
            controller.handleTeamDelete(KeyCode.DELETE);
        });
        teamTable.setOnKeyPressed(e->{
            controller.handleTeamDelete(e.getCode());
        });
        studentTable.setOnKeyPressed(e->{
            controller.handleStudentDelete(e.getCode());
        });
        studentDeleteButton.setOnMouseClicked(e->{
            controller.handleStudentDelete(KeyCode.DELETE);
        });
        
        
    }
    public TextField getFirstNameTextField(){
        return firstNameTextField;
    }
    public TextField getLastNameTextField(){
        return lastNameTextField;
    }
    public TextField getTeamNameTextField(){
        return nameTextField;
    }
    public TextField getRoleTextField(){
        return roleTextField;
    }
    public TextField getLinkTextField(){
        return linkTextField;
    }
    public ColorPicker getColorPicker(){
        return colorPicker;
    }
    public ColorPicker getTextColorPicker(){
        return textColorPicker;
    }
    public ChoiceBox getTeamChoiceBox(){
        return teamChoiceBox;
    }
    public VBox getProjectTabContent()
    {
        return projectTabContent;
    }
    public Label getProjects(){
        return projects;
    }
    public Label getTeam(){
        return team;
    }
    public Label getAddEdit(){
        return addEdit;
    }
    public Label getAddEditName(){
        return addEditName;
    }
    public Label getColor(){
        return color;
    }
    public Label getTextColor(){
        return textColor;
    }
    public Label getLink(){
        return link;
    }
    public Label getStudents(){
        return students;
    }
    public Label getFirstName(){
        return firstName;
    }
    public Label getLastNme(){
        return lastName;
    }
    public Label getAddEditTeam(){
        return addEditTeam;
    }
    public Label getRole(){
        return role;
    }
    public Label getAddEdit2(){
        return addEdit2;
    }
    public void resetProjectTab(){
        teamTable.getItems().clear();
        studentTable.getItems().clear();
        nameTextField.clear();
        colorPicker.setValue(Color.WHITE);
        textColorPicker.setValue(Color.WHITE);
        linkTextField.clear();
        firstNameTextField.clear();
        lastNameTextField.clear();
        teamChoiceBox.getItems().clear();
        roleTextField.clear();
    }
    
}
