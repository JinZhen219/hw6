package tam.workspace;
import java.util.ArrayList;
import java.util.HashMap;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import jtps.jTPS_Transaction;
import tam.data.TAData;
import tam.TAManagerApp;
import tam.data.Recitation;
import tam.data.TeachingAssistant;
import tam.file.TimeSlot;
import tam.workspace.TAWorkspace;
/**
 *
 * @author Jin
 */
public class deleteTA implements jTPS_Transaction
{
    private TeachingAssistant ta;
    HashMap<String, Label> label;
    TAData data;
    ArrayList<TimeSlot> officeHours;
    HashMap<String, Label> oldLabel;
    private ObservableList<Recitation> recitationList;
    private ObservableList<Recitation> modifiedRecitationList;
    TAWorkspace workspace;
    public deleteTA(TeachingAssistant initTA, ArrayList<TimeSlot> initOfficeHours, TAData initData, TAWorkspace initWorkspace, HashMap<String, Label> initLabel)
    {
       ta = initTA;
       officeHours = initOfficeHours;
       data = initData;
       label = initLabel;
       workspace = initWorkspace;    
       recitationList = data.getRecitations();
       modifiedRecitationList = workspace.getController().modifiedRecitationList;
    }
    @Override 
    public void doTransaction()
    { 
        data.removeTA(ta.getName());
        for (Label label2 : label.values()) {
                    if (label2.getText().equals(ta.getName())
                    || (label2.getText().contains(ta.getName() + "\n"))
                    || (label2.getText().contains("\n" + ta.getName()))) {
                        data.removeTAFromCell(label2.textProperty(), ta.getName());
                    }
                }
        for(Recitation rec : recitationList)
        {
            if(rec.getTa1().equals(ta.getName()) && rec.getTa2().equals(ta.getName())){
                Recitation newRec = new Recitation(rec.getSection(), rec.getInstructor(), rec.getDayTime(), rec.getLocation(), rec.getTa1(), rec.getTa2());
                modifiedRecitationList.add(newRec);
                rec.setTA1("");
                rec.setTA2("");
                
            }
            else if(rec.getTa1().equals(ta.getName())){
                Recitation newRec = new Recitation(rec.getSection(), rec.getInstructor(), rec.getDayTime(), rec.getLocation(), rec.getTa1(), rec.getTa2());
                modifiedRecitationList.add(newRec);
                rec.setTA1("");
            }
            else if(rec.getTa2().equals(ta.getName())){
                Recitation newRec = new Recitation(rec.getSection(), rec.getInstructor(), rec.getDayTime(), rec.getLocation(), rec.getTa1(), rec.getTa2());
                modifiedRecitationList.add(newRec);
                rec.setTA2("");
            }            
        }
        try{
            workspace.getRecitationTabContent().ta1ChoiceBox.getItems().remove(ta.getName());
            workspace.getRecitationTabContent().ta2ChoiceBox.getItems().remove(ta.getName());
        }
        catch(Exception e){}
        workspace.getRecitationTabContent().recitationTable.refresh();
        clear();
        
    }
    @Override 
    public void undoTransaction()
    {
        data.addTA(ta.getName(),ta.getEmail(), ta.getUndergraduate() );
        workspace.getOfficeHoursGridPane().getChildren().clear();
        workspace.reloadWorkspace(data);
        for(TimeSlot ts : officeHours)
                {
                    ArrayList<String> gridHeaders = data.getGridHeaders();
                    for (int row = 1; row < data.getNumRows(); row++) {
                        for (int col = 2; col < 7; col++) 
                        {
                            String day = gridHeaders.get(col);
                            String timeCellKey = data.getCellKey(col, row);
                            StringProperty x = data.getCellTextProperty(0, row);
                            String cellTime = x.getValue().replace(":", "_");
                            String taNameee = ts.getName();
                            if(ts.getTime().equals(cellTime) && ts.getDay().equals(day) )
                            {
                                Pane p = workspace.getTACellPane(timeCellKey);
                                String cellKey = p.getId();
                                data.toggleTAOfficeHours(cellKey, taNameee);
                                break;
                            }
                        }


                    }

                }
        for(Recitation rec : recitationList){
            for(Recitation rec2 : modifiedRecitationList){
                String y = rec2.getTa1();
                String x = rec.getTa1();

                if(rec2.getSection().equals(rec.getSection())){
                    rec.setTA1(rec2.getTa1());
                    rec.setTA2(rec2.getTa2());
                }
            }
        }
        modifiedRecitationList.clear();
        workspace.getRecitationTabContent().ta1ChoiceBox.getItems().add(ta.getName());
        workspace.getRecitationTabContent().ta2ChoiceBox.getItems().add(ta.getName());
        workspace.getRecitationTabContent().recitationTable.refresh();
        clear();
    }
    public void clear(){
        workspace.getClearButton1().setDisable(true);
                workspace.getClearButton().setDisable(true);
                workspace.getNameTextField().setPromptText("Name");
                workspace.getEmailTextField().setPromptText("Email");
                workspace.getEmailTextField().setText("");
                workspace.getNameTextField().setText("");
                HBox addBox = workspace.getAddBox();
                addBox.getChildren().clear();
                 
                addBox.getChildren().add(workspace.nameTextField);
                addBox.getChildren().add(workspace.emailTextField);
                addBox.getChildren().add(workspace.getAddButton());
                addBox.getChildren().add(workspace.clearButton);    
                workspace.setUpdate(false);
    }
}
