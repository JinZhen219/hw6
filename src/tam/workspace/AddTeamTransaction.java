/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.workspace;

import javafx.scene.paint.Color;
import jtps.jTPS_Transaction;
import tam.data.Schedule;
import tam.data.TAData;

/**
 *
 * @author Jin
 */
public class AddTeamTransaction implements jTPS_Transaction{
    private TAData data;
    private projectDataTabContent projectTab;
    private String teamName;
    private String color;
    private String textColor;
    private String link;
    
    public AddTeamTransaction(TAData data, projectDataTabContent projectTab){
        this.data = data;
        this.projectTab = projectTab;
        teamName = projectTab.nameTextField.getText();
        color = projectTab.colorPicker.getValue().toString().substring(2,8);
        textColor = projectTab.textColorPicker.getValue().toString().substring(2,8);
        link = projectTab.linkTextField.getText();
    }
    @Override 
    public void doTransaction(){
        data.addTeam(teamName, color, textColor, link);
        projectTab.getTeamChoiceBox().getItems().add(teamName);
        handleClear();
    }
    @Override
    public void undoTransaction(){
        data.removeTeam(teamName);
        projectTab.getTeamChoiceBox().getItems().remove(teamName);
        handleClear();
    }
    public void handleClear(){
        
        projectTab.nameTextField.clear();
        projectTab.colorPicker.setValue(Color.WHITE);
        projectTab.textColorPicker.setValue(Color.WHITE);
        projectTab.linkTextField.clear();
        projectTab.teamUpdate = false;
    }
}
