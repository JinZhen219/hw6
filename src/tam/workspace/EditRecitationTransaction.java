/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.workspace;

import jtps.jTPS_Transaction;
import tam.data.Recitation;
import tam.data.TAData;

/**
 *
 * @author Jin
 */
public class EditRecitationTransaction implements jTPS_Transaction{
    private Recitation rec;
    private TAWorkspace workspace;
    private Recitation newRecitation;
    private TAData data;
    
    public EditRecitationTransaction(Recitation rec, TAData data){
        this.rec = rec;
        this.data = data;
        workspace = data.getWorkspace();
        recitationDataTabContent recTab = workspace.getRecitationTabContent();
        newRecitation = new Recitation(recTab.getSectionTextField().getText(), recTab.getInstructorTextField().getText(), recTab.getDayTimeTextField().getText(),
                        recTab.getLocationTextField().getText(), (String)recTab.getTA1ChoiceBox().getValue(), (String)recTab.getTA2ChoiceBox().getValue());
    }
    @Override 
    public void doTransaction(){
        data.removeRecitation(rec.getSection());
        data.addRecitation(newRecitation.getSection(), newRecitation.getInstructor(), newRecitation.getDayTime(), newRecitation.getLocation(), newRecitation.getTa1(), newRecitation.getTa2());
    }
    @Override 
    public void undoTransaction(){
        data.removeRecitation(newRecitation.getSection());
        data.addRecitation(rec.getSection(), rec.getInstructor(), rec.getDayTime(), rec.getLocation(), rec.getTa1(), rec.getTa2());
    }
}
