/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.workspace;

import java.util.ArrayList;
import java.util.HashMap;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import jtps.jTPS_Transaction;
import tam.data.Schedule;
import tam.data.Student;
import tam.data.TAData;
import tam.data.Team;

/**
 *
 * @author khurr
 */
public class DeleteScheduleTransaction implements jTPS_Transaction {

    private Schedule sche;
    private TAData data;
    public DeleteScheduleTransaction(Schedule sche, TAData data) {
        
        this.data = data;
        this.sche = sche;
    }

    @Override
    public void doTransaction() {  //control Y 
        data.removeSchedule(sche.getType(), sche.getDate(), sche.getTime(), sche.getTitle(), sche.getTopic(), sche.getLink(), sche.getCriteria());
        
    }

    @Override
    public void undoTransaction() {        
        data.addSchedule(sche.getType(), sche.getDate(), sche.getTime(), sche.getTitle(), sche.getTopic(), sche.getLink(), sche.getCriteria());
        
    }
}
