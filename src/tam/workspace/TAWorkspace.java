package tam.workspace;

import tam.CSGManagerProp;
import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;
import djf.settings.AppPropertyType;
import static djf.settings.AppStartupConstants.FILE_PROTOCOL;
import static djf.settings.AppStartupConstants.PATH_IMAGES;
import java.util.ArrayList;
import java.util.HashMap;
import tam.TAManagerApp;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SplitPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import jtps.jTPS_Transaction;
import jtps.test.AddToNum_Transaction;
import properties_manager.PropertiesManager;
import tam.style.TAStyle;
import tam.data.TAData;
import tam.data.TeachingAssistant;
import tam.workspace.courseDetailsTabContent;
import javafx.scene.control.TableCell;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Callback;
import tam.data.CallBackClassTA;
import tam.data.SitePageTemplate;
import tam.data.callBackClass;

/**
 * This class serves as the workspace component for the TA Manager application.
 * It provides all the user interface controls in the workspace area.
 *
 * @author Richard McKenna
 */
public class TAWorkspace extends AppWorkspaceComponent {

    // THIS PROVIDES US WITH ACCESS TO THE APP COMPONENTS
    TAManagerApp app;

    // THIS PROVIDES RESPONSES TO INTERACTIONS WITH THIS WORKSPACE
    TAController controller;

    // NOTE THAT EVERY CONTROL IS PUT IN A BOX TO HELP WITH ALIGNMENT
    // FOR THE HEADER ON THE LEFT
    HBox tasHeaderBox;
    Label tasHeaderLabel;
    

    // FOR THE TA TABLE
    Label teachingAssistantLabel;
    TableView<TeachingAssistant> taTable;
    TableColumn<TeachingAssistant, String> nameColumn;
    TableColumn<TeachingAssistant, String> emailColumn;

    // THE TA INPUT
    HBox addBox;
    TextField nameTextField;
    TextField emailTextField;
    Button addButton;
    Button updateTaButton;
    Button clearButton;
    Button clearButton1;

    // THE HEADER ON THE RIGHT
    HBox officeHoursHeaderBox;
    Label officeHoursHeaderLabel;
    //Start and End Time for Office Hours 
    boolean update = false;
    ChoiceBox newStartTime;
    ChoiceBox newEndTime;
    Button changeTimeButton;
    TabPane coursePagePane;
    Tab courseDetailsTab;
    Tab taTab;
    Tab recitationTab;
    Tab scheduleTab;
    Tab projectTab;
    // THE OFFICE HOURS GRID
    GridPane officeHoursGridPane;
    HashMap<String, Pane> officeHoursGridTimeHeaderPanes;
    HashMap<String, Label> officeHoursGridTimeHeaderLabels;
    HashMap<String, Pane> officeHoursGridDayHeaderPanes;
    HashMap<String, Label> officeHoursGridDayHeaderLabels;
    HashMap<String, Pane> officeHoursGridTimeCellPanes;
    HashMap<String, Label> officeHoursGridTimeCellLabels;
    HashMap<String, Pane> officeHoursGridTACellPanes;
    HashMap<String, Label> officeHoursGridTACellLabels;
    courseDetailsTabContent courseTabContent;
    recitationDataTabContent recitationTabContent;
    scheduleDataTabContent scheduleTabContent;
    projectDataTabContent projectTabContent;
    Button deleteButton;
    Label startTimeLabel;
    Label endTimeLabel ;
    String oldName;
    String oldEmail;
    
    /**
     * The contstructor initializes the user interface, except for the full
     * office hours grid, since it doesn't yet know what the hours will be until
     * a file is loaded or a new one is created.
     */
    public TAWorkspace(TAManagerApp initApp) {
        // KEEP THIS FOR LATER
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        app = initApp;
        courseDetailsTab = new Tab();
        taTab = new Tab();
        recitationTab = new Tab();
        scheduleTab = new Tab();
        projectTab = new Tab();
        coursePagePane = new TabPane();
        
        deleteButton = new Button();
        startTimeLabel = new Label();
        endTimeLabel = new Label();
        
        // WE'LL NEED THIS TO GET LANGUAGE PROPERTIES FOR OUR UI
        //Sets text for each tab and adds it to the pane
        courseDetailsTab.setText(props.getProperty(CSGManagerProp.COURSE_DETAILS_TAB.toString()));
        recitationTab.setText(props.getProperty(CSGManagerProp.RECITATION_TAB.toString()));
        scheduleTab.setText(props.getProperty(CSGManagerProp.SCHEDULE_TAB.toString()));
        taTab.setText(props.getProperty(CSGManagerProp.TA_TAB.toString()));
        projectTab.setText(props.getProperty(CSGManagerProp.PROJECT_TAB.toString()));
        recitationTab.setClosable(false);
        courseDetailsTab.setClosable(false);
        scheduleTab.setClosable(false);
        projectTab.setClosable(false);
        taTab.setClosable(false);        
        
        coursePagePane.getTabs().addAll(courseDetailsTab, taTab, recitationTab, scheduleTab, projectTab);
        courseTabContent = new courseDetailsTabContent(app);
        recitationTabContent = new recitationDataTabContent(app);
        recitationTab.setContent(recitationTabContent.getRecitationDataBox());
        courseDetailsTab.setContent(courseTabContent.getCourseDetailsSplitPane());
        scheduleTabContent = new scheduleDataTabContent(app);
        scheduleTab.setContent(scheduleTabContent.getScheduleTabContent());
        projectTabContent = new projectDataTabContent(app);
        projectTab.setContent(projectTabContent.getProjectTabContent());
        coursePagePane.setTabMinWidth(170);
        
        
        
        
        
        // INIT THE HEADER ON THE LEFT
        tasHeaderBox = new HBox();
        String tasHeaderText = props.getProperty(CSGManagerProp.TAS_HEADER_TEXT.toString());
        tasHeaderLabel = new Label(tasHeaderText);
        tasHeaderBox.getChildren().add(tasHeaderLabel);

        // MAKE THE TABLE AND SETUP THE DATA MODEL
        taTable = new TableView();
        taTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        TAData data = (TAData) app.getDataComponent();
        ObservableList<TeachingAssistant> tableData = data.getTeachingAssistants();
        taTable.setItems(tableData);
        String nameColumnText = props.getProperty(CSGManagerProp.NAME_COLUMN_TEXT.toString());
        String emailColumnText = props.getProperty(CSGManagerProp.EMAIL_COLUMN_TEXT.toString());
        String undergraduateColumnText = props.getProperty(CSGManagerProp.UNDERGRADUATE_COLUMN_TEXT.toString());
        nameColumn = new TableColumn(nameColumnText);
        emailColumn = new TableColumn(emailColumnText);
        nameColumn.setCellValueFactory(
                new PropertyValueFactory<TeachingAssistant, String>("name")
        );
        emailColumn.setCellValueFactory(
                new PropertyValueFactory<TeachingAssistant, String>("email")
        );
        TableColumn<TeachingAssistant, CheckBox> c2 = new TableColumn<TeachingAssistant, CheckBox>(undergraduateColumnText.substring(0, undergraduateColumnText.length()-1));
        c2.setCellValueFactory(new CallBackClassTA());
        
        
        c2.setMinWidth(200);
        
        nameColumn.setMinWidth(200);
        emailColumn.setMinWidth(300);
        taTable.getColumns().add(c2);
        taTable.getColumns().add(nameColumn);
        taTable.getColumns().add(emailColumn);
        

        // ADD BOX FOR ADDING A TA
        String namePromptText = props.getProperty(CSGManagerProp.NAME_PROMPT_TEXT.toString());
        String emailPromptText = props.getProperty(CSGManagerProp.EMAIL_PROMPT_TEXT.toString());
        
        String addButtonText = props.getProperty(CSGManagerProp.ADD_BUTTON_TEXT.toString());
        String updateTaButtonText = props.getProperty(CSGManagerProp.UPDATE_TA_BUTTON_TEXT.toString());
        String clearButtonText = props.getProperty(CSGManagerProp.CLEAR_BUTTON_TEXT.toString());
        String changeTimeButtonText = props.getProperty(CSGManagerProp.CHANGE_TIME_BUTTON_TEXT.toString());

        changeTimeButton = new Button(changeTimeButtonText);
        newStartTime = new ChoiceBox();
        newEndTime = new ChoiceBox();
        for (int i = 0; i < 24; i++) {
            newStartTime.getItems().addAll(buildCellText(i, "00"));
        }
        for (int i = 0; i < 24; i++) {
            newEndTime.getItems().addAll(buildCellText(i, "00"));
        }

        nameTextField = new TextField();
        emailTextField = new TextField();
        nameTextField.setPromptText(namePromptText);
        emailTextField.setPromptText(emailPromptText);
        addButton = new Button(addButtonText);
        updateTaButton = new Button(updateTaButtonText);
        clearButton = new Button(clearButtonText);
        clearButton1 = new Button(clearButtonText);
        addBox = new HBox();
//        newStartTime.setPromptText(startHourPromptText);
//        newEndTime.setPromptText(endHourPromptText);
        clearButton1.setDisable(!update);
        clearButton.setDisable(!update);
        nameTextField.prefWidthProperty().bind(addBox.widthProperty().multiply(.4));
        emailTextField.prefWidthProperty().bind(addBox.widthProperty().multiply(.4));
        addButton.prefWidthProperty().bind(addBox.widthProperty().multiply(.2));
        updateTaButton.prefWidthProperty().bind(addBox.widthProperty().multiply(.2));
        clearButton.prefWidthProperty().bind(addBox.widthProperty().multiply(.2));
        updateTaButton.prefHeightProperty().bind(addBox.heightProperty().multiply(1));
        clearButton.prefHeightProperty().bind(addBox.heightProperty().multiply(1));
        clearButton1.prefWidthProperty().bind(addBox.widthProperty().multiply(.2));
        clearButton1.prefHeightProperty().bind(addBox.heightProperty().multiply(1));

        addBox.getChildren().add(nameTextField);
        addBox.getChildren().add(emailTextField);
        addBox.getChildren().add(addButton);
        addBox.getChildren().add(clearButton1);
        
        
        // INIT THE HEADER ON THE RIGHT
        officeHoursHeaderBox = new HBox();
        String officeHoursGridText = props.getProperty(CSGManagerProp.OFFICE_HOURS_SUBHEADER.toString());
        officeHoursHeaderLabel = new Label(officeHoursGridText);
        startTimeLabel.setPadding(new Insets(4,0,0,0));
        endTimeLabel.setPadding(new Insets(4,0,0,0));
        changeTimeButton.setPadding(new Insets(4,0,0,0));
        startTimeLabel.setText(props.getProperty(CSGManagerProp.START_HOUR_PROMPT_TEXT));
        endTimeLabel.setText(props.getProperty(CSGManagerProp.END_HOUR_PROMPT_TEXT));
        HBox startTimeBox = new HBox(startTimeLabel, newStartTime);
        startTimeBox.setSpacing(3);
        HBox endTimeBox = new HBox(endTimeLabel, newEndTime);
        endTimeBox.setSpacing(3);
        HBox allTime = new HBox(startTimeBox, endTimeBox, changeTimeButton);
        changeTimeButton.setPrefWidth(100);
        allTime.setPadding(new Insets(5,0,5,0));
        allTime.setSpacing(15);
        
        officeHoursHeaderBox.getChildren().addAll(officeHoursHeaderLabel, allTime);
        officeHoursHeaderBox.setSpacing(55);
        
        

        
        // THESE WILL STORE PANES AND LABELS FOR OUR OFFICE HOURS GRID
        officeHoursGridPane = new GridPane();
        officeHoursGridTimeHeaderPanes = new HashMap();
        officeHoursGridTimeHeaderLabels = new HashMap();
        officeHoursGridDayHeaderPanes = new HashMap();
        officeHoursGridDayHeaderLabels = new HashMap();
        officeHoursGridTimeCellPanes = new HashMap();
        officeHoursGridTimeCellLabels = new HashMap();
        officeHoursGridTACellPanes = new HashMap();
        officeHoursGridTACellLabels = new HashMap();

        // ORGANIZE THE LEFT AND RIGHT PANES
        VBox leftPane = new VBox();
        
        
        String imagePath = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(AppPropertyType.DELETE_ICON);
        Image buttonImage = new Image(imagePath);
	
	// NOW MAKE THE BUTTON
        deleteButton.setDisable(false);
        deleteButton.setGraphic(new ImageView(buttonImage));
        
        
        teachingAssistantLabel = new Label(props.getProperty(CSGManagerProp.TAS_HEADER_TEXT));
        HBox headerBoxx = new HBox(teachingAssistantLabel, deleteButton);
        deleteButton.setPadding(new Insets(5,0,0,0));
        headerBoxx.setSpacing(5);
        headerBoxx.setPadding(new Insets(5,0,0,0));
        leftPane.getChildren().add(headerBoxx);
        leftPane.getChildren().add(taTable);
        leftPane.getChildren().add(addBox);
        VBox rightPane = new VBox();
        rightPane.getChildren().add(officeHoursHeaderBox);
        rightPane.getChildren().add(officeHoursGridPane);
        ScrollPane leftPane2 = new ScrollPane(leftPane);
        // BOTH PANES WILL NOW GO IN A SPLIT PANE
        SplitPane sPane = new SplitPane(leftPane, new ScrollPane(rightPane));
        workspace = new BorderPane();
        taTab.setContent(sPane);
        HBox newBox = new HBox(coursePagePane);
        coursePagePane.setPadding(new Insets(8,8,8,8));
        // AND PUT EVERYTHING IN THE WORKSPACE
        ((BorderPane) workspace).setCenter(newBox);
        
        recitationTabContent.getRecitationDataBox().setStyle("-fx-border-color : black;"
                + "-fx-background-color : #f4f4f4");
        courseTabContent.getCourseDetailsSplitPane().setStyle("-fx-border-color : black;"
                + "-fx-background-color : #f4f4f4");
        scheduleTabContent.getScheduleTabContent().setStyle("-fx-border-color : black;"
                + "-fx-background-color : bisque");
        projectTabContent.getProjectTabContent().setStyle("-fx-border-color : black;"
                + "-fx-background-color : bisque");
        sPane.setStyle("-fx-border-color : black;"
                + "-fx-background-color : #f4f4f4");
        
        
        newBox.setStyle("-fx-background-color : #ffdaad");

        // MAKE SURE THE TABLE EXTENDS DOWN FAR ENOUGH
        taTable.prefHeightProperty().bind(workspace.heightProperty().multiply(1));

        // NOW LET'S SETUP THE EVENT HANDLING
        controller = new TAController(app);

        // CONTROLS FOR ADDING TAs
        deleteButton.setOnAction(e -> {
            controller.handleKeyPress(KeyCode.DELETE);
        });
        nameTextField.setOnKeyPressed(e -> {
            controller.handleAddTA(e.getCode());            
        });
        emailTextField.setOnKeyPressed(e -> {
            controller.handleAddTA(e.getCode());
        });
        addButton.setOnMouseClicked(e -> {
            controller.handleAddTA(KeyCode.ENTER);
        });
        changeTimeButton.setOnMouseClicked(e -> {
            String startTime = (String) newStartTime.getValue();
            String endTime = (String) newEndTime.getValue();
 
            controller.handleChangeTime(startTime, endTime);

        });
        updateTaButton.setOnMouseClicked(e -> {
            Object selectedItem = taTable.getSelectionModel().getSelectedItem();
            TeachingAssistant ta = (TeachingAssistant) selectedItem;
            System.out.println(ta.getName());
            setOldName(ta.getName());
            setOldEmail(ta.getEmail());
            controller.handleUpdateTA();
            
        });
        clearButton.setOnMouseClicked(e -> {
            addBox.getChildren().add(addButton);
            addBox.getChildren().add(clearButton1);
            addBox.getChildren().remove(updateTaButton);
            addBox.getChildren().remove(clearButton);
            nameTextField.clear();
            emailTextField.clear();
            nameTextField.setPromptText(namePromptText);
            emailTextField.setPromptText(emailPromptText);
            update = false;
            clearButton1.setDisable(true);
            clearButton.setDisable(true);

        });
        clearButton1.setOnMouseClicked(e -> {
            nameTextField.clear();
            emailTextField.clear();
            nameTextField.setPromptText(namePromptText);
            emailTextField.setPromptText(emailPromptText);
            update = false;
            clearButton1.setDisable(true);
            clearButton.setDisable(true);
        });

        workspace.setOnKeyPressed(e -> {
            if (e.isControlDown() && e.getCode() == (KeyCode.Y)) {
                controller.handleReDoTransaction();
            } else if (e.isControlDown() && e.getCode() == (KeyCode.Z)) {
                controller.handleUndoTransaction();
            }

        });
        app.getGUI().getUndoButton().setOnMouseClicked(e -> {
            controller.handleUndoTransaction();
        });
        app.getGUI().getRedoButton().setOnMouseClicked(e -> {
            controller.handleReDoTransaction();
        });
        taTable.setFocusTraversable(true);
        taTable.setEditable(true);
        taTable.setOnKeyPressed(e -> {
            controller.handleKeyPress(e.getCode());
        });
        taTable.setOnMousePressed(e -> {
            //addBox.getChildren().clear();
            controller.handleTaClicked(workspace, addBox);
            System.out.println("Clicked TA");
            
        });

    }
    public courseDetailsTabContent getCourseDetailsTabContent()
    {
        return courseTabContent;
    }
    public scheduleDataTabContent getScheduleTabContent(){
        return scheduleTabContent;
    }
    public recitationDataTabContent getRecitationTabContent(){
        return recitationTabContent;
    }
    public projectDataTabContent getProjectTabContent(){
        return projectTabContent;
    }
    // WE'LL PROVIDE AN ACCESSOR METHOD FOR EACH VISIBLE COMPONENT
    // IN CASE A CONTROLLER OR STYLE CLASS NEEDS TO CHANGE IT
    public HBox getTAsHeaderBox() {
        return tasHeaderBox;
    }
    
    public Label getTAsHeaderLabel() {
        return tasHeaderLabel;
    }
    public Label getTALabel()
    {
        return teachingAssistantLabel;
    }

    public TableView getTATable() {
        return taTable;
    }
    public boolean getUpdate(){
        return update;
    }
    public void setUpdate(boolean update)
    {
        this.update = update;
    }
    public HBox getAddBox() {
        return addBox;
    }

    public TextField getNameTextField() {
        return nameTextField;
    }

    public TextField getEmailTextField() {
        return emailTextField;
    }

    public Button getAddButton() {
        return addButton;
    }

    public Button getUpdateTaButton() {
        return updateTaButton;
    }

    public Button getClearButton() {
        return clearButton;
    }

    public Button getClearButton1() {
        return clearButton1;
    }
    public Label getStartTimeLabel(){
        return startTimeLabel;
    }
    public Label getEndTimeLabel(){
        return endTimeLabel;
    }
    public Button getChangeTimeButton() {
        return changeTimeButton;
    }

    public ChoiceBox getNewStartBox() {
        return newStartTime;
    }

    public ChoiceBox getNewEndBox() {
        return newEndTime;

    }

    public HBox getOfficeHoursSubheaderBox() {
        return officeHoursHeaderBox;
    }

    public TAController getController() {
        return controller;
    }

    public Label getOfficeHoursSubheaderLabel() {
        return officeHoursHeaderLabel;
    }

    public GridPane getOfficeHoursGridPane() {
        return officeHoursGridPane;
    }

    public HashMap<String, Pane> getOfficeHoursGridTimeHeaderPanes() {
        return officeHoursGridTimeHeaderPanes;
    }

    public HashMap<String, Label> getOfficeHoursGridTimeHeaderLabels() {
        return officeHoursGridTimeHeaderLabels;
    }

    public HashMap<String, Pane> getOfficeHoursGridDayHeaderPanes() {
        return officeHoursGridDayHeaderPanes;
    }

    public HashMap<String, Label> getOfficeHoursGridDayHeaderLabels() {
        return officeHoursGridDayHeaderLabels;
    }

    public HashMap<String, Pane> getOfficeHoursGridTimeCellPanes() {
        return officeHoursGridTimeCellPanes;
    }

    public HashMap<String, Label> getOfficeHoursGridTimeCellLabels() {
        return officeHoursGridTimeCellLabels;
    }

    public HashMap<String, Pane> getOfficeHoursGridTACellPanes() {
        return officeHoursGridTACellPanes;
    }

    public HashMap<String, Label> getOfficeHoursGridTACellLabels() {
        return officeHoursGridTACellLabels;
    }
    public String getOldName(){
        return oldName;
    }
    public String getOldEmail()
    {
        return oldEmail;
    }
    public void setOldName(String name)
    {
        oldName = name;
    }
    public void setOldEmail(String email)
    {
        oldEmail = email;
    }

    public String getCellKey(Pane testPane) {
        for (String key : officeHoursGridTACellLabels.keySet()) {
            if (officeHoursGridTACellPanes.get(key) == testPane) {
                return key;
            }
        }
        return null;
    }

    public Label getTACellLabel(String cellKey) {
        return officeHoursGridTACellLabels.get(cellKey);
    }

    public Pane getTACellPane(String cellPane) {
        return officeHoursGridTACellPanes.get(cellPane);
    }

    public String buildCellKey(int col, int row) {
        return "" + col + "_" + row;
    }

    public String buildCellText(int militaryHour, String minutes) {
        // FIRST THE START AND END CELLS
        int hour = militaryHour;
        if (hour > 12) {
            hour -= 12;
        }
        if(militaryHour == 0 )
        {
            hour = 12;
        }
        String cellText = "" + hour + ":" + minutes;
        if (militaryHour < 12) {
            cellText += "am";
        } else {
            cellText += "pm";
        }
        return cellText;
    }

    @Override
    public void resetWorkspace() {
        // CLEAR OUT THE GRID PANE
        if(app.getGUI().getFileController().isNew() == true){
            scheduleTabContent.resetScheduleTab();
            courseTabContent.resetCourseDetails();
            recitationTabContent.resetRecitationTab();
            projectTabContent.resetProjectTab();
        }
        officeHoursGridPane.getChildren().clear();

        // AND THEN ALL THE GRID PANES AND LABELS
        officeHoursGridTimeHeaderPanes.clear();
        officeHoursGridTimeHeaderLabels.clear();
        officeHoursGridDayHeaderPanes.clear();
        officeHoursGridDayHeaderLabels.clear();
        officeHoursGridTimeCellPanes.clear();
        officeHoursGridTimeCellLabels.clear();
        officeHoursGridTACellPanes.clear();
        officeHoursGridTACellLabels.clear();
        nameTextField.clear();
        clearButton1.setDisable(true);
        clearButton.setDisable(true);
        emailTextField.clear();
        setUpdate(false);
//       Pane addBox = workspace.getAddBox();
        addBox.getChildren().clear();
        addBox.getChildren().add(nameTextField);
        addBox.getChildren().add(emailTextField);
        addBox.getChildren().add(getAddButton());
        addBox.getChildren().add(clearButton);    
    }

    @Override
    public void reloadWorkspace(AppDataComponent dataComponent) {
        TAData taData = (TAData) dataComponent;

        reloadOfficeHoursGrid(taData);
    }
    
    public void reloadOfficeHoursGrid(TAData dataComponent) {
        ArrayList<String> gridHeaders = dataComponent.getGridHeaders();

        // ADD THE TIME HEADERS
        for (int i = 0; i < 2; i++) {
            addCellToGrid(dataComponent, officeHoursGridTimeHeaderPanes, officeHoursGridTimeHeaderLabels, i, 0);
            dataComponent.getCellTextProperty(i, 0).set(gridHeaders.get(i));
        }

        // THEN THE DAY OF WEEK HEADERS
        for (int i = 2; i < 7; i++) {
            addCellToGrid(dataComponent, officeHoursGridDayHeaderPanes, officeHoursGridDayHeaderLabels, i, 0);
            dataComponent.getCellTextProperty(i, 0).set(gridHeaders.get(i));
        }

        // THEN THE TIME AND TA CELLS
        int row = 1;
        for (int i = dataComponent.getStartHour(); i < dataComponent.getEndHour(); i++) {
            // START TIME COLUMN
            int col = 0;
            addCellToGrid(dataComponent, officeHoursGridTimeCellPanes, officeHoursGridTimeCellLabels, col, row);
            dataComponent.getCellTextProperty(col, row).set(buildCellText(i, "00"));
            addCellToGrid(dataComponent, officeHoursGridTimeCellPanes, officeHoursGridTimeCellLabels, col, row + 1);
            dataComponent.getCellTextProperty(col, row + 1).set(buildCellText(i, "30"));

            // END TIME COLUMN
            col++;
            int endHour = i;
            addCellToGrid(dataComponent, officeHoursGridTimeCellPanes, officeHoursGridTimeCellLabels, col, row);
            dataComponent.getCellTextProperty(col, row).set(buildCellText(endHour, "30"));
            addCellToGrid(dataComponent, officeHoursGridTimeCellPanes, officeHoursGridTimeCellLabels, col, row + 1);
            dataComponent.getCellTextProperty(col, row + 1).set(buildCellText(endHour + 1, "00"));
            col++;

            // AND NOW ALL THE TA TOGGLE CELLS
            while (col < 7) {
                addCellToGrid(dataComponent, officeHoursGridTACellPanes, officeHoursGridTACellLabels, col, row);
                addCellToGrid(dataComponent, officeHoursGridTACellPanes, officeHoursGridTACellLabels, col, row + 1);
                col++;
            }
            row += 2;
        }

        // CONTROLS FOR TOGGLING TA OFFICE HOURS
        for (Pane p : officeHoursGridTACellPanes.values()) {
            p.setFocusTraversable(true);
            p.setOnKeyPressed(e -> {
                controller.handleKeyPress(e.getCode());
            });
            p.setOnMouseClicked(e -> {
                controller.handleCellToggle((Pane) e.getSource());
            });
            p.setOnMouseExited(e -> {
                controller.handleGridCellMouseExited((Pane) e.getSource());
            });
            p.setOnMouseEntered(e -> {
                controller.handleGridCellMouseEntered((Pane) e.getSource());
            });
        }

        // AND MAKE SURE ALL THE COMPONENTS HAVE THE PROPER STYLE
        TAStyle taStyle = (TAStyle) app.getStyleComponent();
        taStyle.initOfficeHoursGridStyle();
    }

    public void addCellToGrid(TAData dataComponent, HashMap<String, Pane> panes, HashMap<String, Label> labels, int col, int row) {
        // MAKE THE LABEL IN A PANE
        Label cellLabel = new Label("");
        HBox cellPane = new HBox();
        cellPane.setAlignment(Pos.CENTER);
        cellPane.getChildren().add(cellLabel);

        // BUILD A KEY TO EASILY UNIQUELY IDENTIFY THE CELL
        String cellKey = dataComponent.getCellKey(col, row);
        cellPane.setId(cellKey);
        cellLabel.setId(cellKey);

        // NOW PUT THE CELL IN THE WORKSPACE GRID
        officeHoursGridPane.add(cellPane, col, row);

        // AND ALSO KEEP IN IN CASE WE NEED TO STYLIZE IT
        panes.put(cellKey, cellPane);
        labels.put(cellKey, cellLabel);

        // AND FINALLY, GIVE THE TEXT PROPERTY TO THE DATA MANAGER
        // SO IT CAN MANAGE ALL CHANGES
        dataComponent.setCellProperty(col, row, cellLabel.textProperty());
    }
}
