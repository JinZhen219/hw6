/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.workspace;

import jtps.jTPS_Transaction;
import tam.data.Student;
import tam.data.TAData;
import tam.data.Team;

/**
 *
 * @author Jin
 */
public class EditStudentTransaction implements jTPS_Transaction{
    private Student student;
    private TAWorkspace workspace;
    private Student newStudent;
    private TAData data;
    private projectDataTabContent projectTab;
    public EditStudentTransaction(Student student, TAData data){
        this.student = student;
        this.data = data;
        workspace = data.getWorkspace();
        projectTab = workspace.getProjectTabContent();
        newStudent = new Student(projectTab.firstNameTextField.getText(), projectTab.lastNameTextField.getText(), projectTab.teamChoiceBox.getValue().toString(), projectTab.roleTextField.getText());
    }
    @Override 
    public void doTransaction(){
        data.removeStudent(student.getFirstName(), student.getLastName(), student.getTeam(), student.getRole());
        data.addStudent(newStudent.getFirstName(), newStudent.getLastName(), newStudent.getTeam(), newStudent.getRole());
        
    }
    @Override 
    public void undoTransaction(){
        data.removeStudent(newStudent.getFirstName(), newStudent.getLastName(), newStudent.getTeam(), newStudent.getRole());
        data.addStudent(student.getFirstName(), student.getLastName(), student.getTeam(), student.getRole());
       
    }
}
