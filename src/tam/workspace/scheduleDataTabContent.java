/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.workspace;

import djf.settings.AppPropertyType;
import static djf.settings.AppStartupConstants.FILE_PROTOCOL;
import static djf.settings.AppStartupConstants.PATH_IMAGES;
import djf.ui.AppMessageDialogSingleton;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import tam.CSGManagerProp;
import java.util.ArrayList;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import jtps.jTPS_Transaction;
import properties_manager.PropertiesManager;
import tam.TAManagerApp;
import tam.data.Schedule;
import tam.data.TAData;

/**
 *
 * @author Jin
 */
public class scheduleDataTabContent 
{
    Label schedule = new Label();
    Label calenderBoundaries = new Label();
    Label startingMonday = new Label();
    Label endingFriday = new Label();
    Label scheduleItems = new Label();
    ObservableList<String> scheduleGridHeaders;
    Label addEdit = new Label();
    Label type = new Label();
    Label date = new Label();
    Label time = new Label();
    Label title = new Label();
    Label topic = new Label();
    Label link = new Label();
    Label criteria = new Label();
    ChoiceBox typeChoiceBox = new ChoiceBox();
    DatePicker dateDatePicker = new DatePicker();
    TextField timeTextField = new TextField();
    TextField titleTextField = new TextField();
    TextField topicTextField = new TextField();
    TextField linkTextField = new TextField();
    TextField criteriaTextField = new TextField();
    
    DatePicker startDatePicker = new DatePicker();

    DatePicker endDatePicker = new DatePicker();
    Button addUpdateButton = new Button();
    Button clearButton = new Button();
    Button deleteButton = new Button();
    TableView scheduleTable = new TableView();
    TAManagerApp app;
    TableColumn typeColumn;
    TableColumn dateColumn;
    TableColumn titleColumn;
    TableColumn topicColumn;
    VBox scheduleTabContent;
    boolean update = false;
    public scheduleDataTabContent(TAManagerApp initApp)
    {
       
        app = initApp;
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        schedule.setText(props.getProperty(CSGManagerProp.SCHEDULE));
        calenderBoundaries.setText(props.getProperty(CSGManagerProp.CALENDER_BOUNDARIES));
        startingMonday.setText(props.getProperty(CSGManagerProp.STARTING_MONDAY));
        endingFriday.setText(props.getProperty(CSGManagerProp.ENDING_FRIDAY));
        scheduleItems.setText(props.getProperty(CSGManagerProp.SCHEDULE_ITEMS));
        type.setText(props.getProperty(CSGManagerProp.TYPE));
        date.setText(props.getProperty(CSGManagerProp.DATE));
        time.setText(props.getProperty(CSGManagerProp.TIME));
        title.setText(props.getProperty(CSGManagerProp.TITLE));
        criteria.setText(props.getProperty(CSGManagerProp.CRITERIA) + ":");
        topic.setText(props.getProperty(CSGManagerProp.TOPIC));
        link.setText(props.getProperty(CSGManagerProp.LINK));
        timeTextField.setPromptText(props.getProperty(CSGManagerProp.TIME).substring(0, props.getProperty(CSGManagerProp.TIME).length()-1));
        titleTextField.setPromptText(props.getProperty(CSGManagerProp.TITLE).substring(0, props.getProperty(CSGManagerProp.TITLE).length()-1));
        topicTextField.setPromptText(props.getProperty(CSGManagerProp.TOPIC).substring(0, props.getProperty(CSGManagerProp.TITLE).length()-1));
        linkTextField.setPromptText(props.getProperty(CSGManagerProp.LINK).substring(0, props.getProperty(CSGManagerProp.LINK).length()-1));
        criteriaTextField.setPromptText(props.getProperty(CSGManagerProp.CRITERIA));
        
        timeTextField.setMinWidth(200);
        titleTextField.setMinWidth(200);
        topicTextField.setMinWidth(200);
        linkTextField.setMinWidth(200);
        criteriaTextField.setMinWidth(200);
        typeChoiceBox.setMinWidth(100);
        
        typeChoiceBox.getItems().addAll("Holiday", "Lecture");
        
        addEdit.setText(props.getProperty(CSGManagerProp.ADD_EDIT));
        addUpdateButton.setText(props.getProperty(CSGManagerProp.ADD_UPDATE_BUTTON_TEXT));
        clearButton.setText(props.getProperty(CSGManagerProp.CLEAR_BUTTON_TEXT));
        
        String imagePath = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(AppPropertyType.DELETE_ICON);
        Image buttonImage = new Image(imagePath);
	
	// NOW MAKE THE BUTTON
        deleteButton.setDisable(false);
        deleteButton.setGraphic(new ImageView(buttonImage));
        
        
        TAData data = (TAData) app.getDataComponent();

        
        data.addSchedule("Holiday", "2/9/17", "All day", "SNOW DAY", "", "", "");
        data.addSchedule("Lecture", "2/14/17", "All day", "Lecture 3", "Event Programming", "", "");
        data.addSchedule("Holiday", "3/13/17", "All day", "Spring Break", "", "", "");
        data.addSchedule("HW", "3/27/17", "All day", "HW3", "UML", "", "");

        
        
        scheduleTable = new TableView();
        scheduleTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        ObservableList<Schedule> scheduleData = data.getSchedules();
        scheduleTable.setItems(scheduleData);
        ArrayList<String> scheduleGridHeaders = props.getPropertyOptionsList(CSGManagerProp.SCHEDULE_GRID_HEADERS);
        typeColumn = new TableColumn(scheduleGridHeaders.get(0));
        dateColumn = new TableColumn(scheduleGridHeaders.get(1));
        titleColumn = new TableColumn(scheduleGridHeaders.get(2));
        topicColumn = new TableColumn(scheduleGridHeaders.get(3));

        
        typeColumn.setCellValueFactory(new PropertyValueFactory<Schedule, String>("type"));
        dateColumn.setCellValueFactory(new PropertyValueFactory<Schedule, String>("date"));
        titleColumn.setCellValueFactory(new PropertyValueFactory<Schedule, String>("title"));
        topicColumn.setCellValueFactory(new PropertyValueFactory<Schedule, String>("topic"));
        
        typeColumn.setMinWidth(200);
        dateColumn.setMinWidth(200);
        titleColumn.setMinWidth(200);
        topicColumn.setMinWidth(200);
        scheduleTable.getColumns().addAll(typeColumn, dateColumn, titleColumn, topicColumn);
        scheduleTable.setFocusTraversable(true);
        scheduleTable.setMaxWidth(850);
        //ORGANIZING THE INFORMATION
        HBox typeBox = new HBox(type, typeChoiceBox);
        type.setPrefWidth(120);
        HBox dateBox = new HBox(date, dateDatePicker);
        date.setPrefWidth(120);
        HBox timeBox = new HBox(time, timeTextField);
        time.setPrefWidth(120);
        HBox titleBox = new HBox(title, titleTextField);
        title.setPrefWidth(120);
        HBox topicBox = new HBox(topic, topicTextField);
        topic.setPrefWidth(120);
        HBox linkBox = new HBox(link , linkTextField);
        link.setPrefWidth(120);
        HBox criteriaBox = new HBox(criteria, criteriaTextField);
        criteria.setPrefWidth(120);
        HBox buttonBox = new HBox(addUpdateButton, clearButton);
        buttonBox.setSpacing(40);
        HBox startBox = new HBox(startingMonday, startDatePicker);
        HBox endBox = new HBox(endingFriday, endDatePicker);
        HBox bigStartEndBox = new HBox(startBox, endBox);
        bigStartEndBox.setSpacing(140);
        VBox upperPortion = new VBox(calenderBoundaries, bigStartEndBox);
        upperPortion.setSpacing(25);
        VBox giantAddEditBox = new VBox(addEdit, typeBox, dateBox, timeBox, titleBox, topicBox, linkBox, criteriaBox, buttonBox);
        giantAddEditBox.setSpacing(15);
        HBox scheduleItemsBox = new HBox(scheduleItems, deleteButton);
        
        scheduleItemsBox.setSpacing(15);
        VBox tableBox = new VBox(scheduleItemsBox, scheduleTable);
        tableBox.setSpacing(15);
        VBox bottomPortion = new VBox(tableBox, giantAddEditBox);
        bottomPortion.setSpacing(30);
        VBox content = new VBox(upperPortion, bottomPortion);
        content.setSpacing(15);
        content.setPadding(new Insets(10, 10, 10 ,10));
        upperPortion.setStyle("-fx-background-color:#f4f4f4; -fx-border-color : black");
        bottomPortion.setStyle("-fx-background-color:#f4f4f4; -fx-border-color: black");
        upperPortion.setPadding(new Insets(5,5,5,5));
        bottomPortion.setPadding(new Insets(5,5,5,5));
        scheduleTabContent = new VBox(schedule, content);
        
        scheduleTabContent.setSpacing(5);
        scheduleTabContent.setPadding(new Insets(6, 6, 6, 6));
        content.setStyle("-fx-background-color: bisque");
        scheduleTabContent.setStyle("-fx-background-color : bisque");
        
        
        ScheduleController controller = new ScheduleController(app);
        scheduleTable.setOnMouseClicked(e->{
            try{
                Object selectedItem = scheduleTable.getSelectionModel().getSelectedItem();
                Schedule sche = (Schedule)selectedItem;
                timeTextField.setText(sche.getTime());
                titleTextField.setText(sche.getTitle());
                topicTextField.setText(sche.getTopic());
                linkTextField.setText(sche.getLink());
                criteriaTextField.setText(sche.getCriteria());
                setDateDatePicker(sche.getDate());
                typeChoiceBox.setValue(sche.getType());
                update = true;
            }
            catch(Exception ex){
//                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
//                dialog.show(props.getProperty(CSGManagerProp.MISSING_INFO_ERROR), props.getProperty(CSGManagerProp.MISSING_INFO_ERROR));
            }
        });
        addUpdateButton.setOnMouseClicked(e->{
            controller.handleAddUpdate();
        });
        clearButton.setOnMouseClicked(e->{
            controller.handleClear();
        });
        deleteButton.setOnMouseClicked(e->{
            controller.handleDelete(KeyCode.DELETE);
        });
        scheduleTable.setOnKeyPressed(e->{
            controller.handleDelete(e.getCode());
        });
        startDatePicker.setOnMouseClicked(e->{
            try{
                data.setOldStartDate(getStartDate());
                controller.handleStartDateChange();
                String y = data.getOldStartDate();
                System.out.println("old date" + y);
                
            }
            catch(NullPointerException xe)
            {
                System.out.println(data.getOldStartDate());
                data.setOldStartDate(null);
            }
        });
        endDatePicker.setOnMouseClicked(e->{          
            try{
                data.setOldEndDate(getEndDate());
                controller.handleEndDateChange();
            }
            catch(NullPointerException xe)
            {
                System.out.println(data.getOldStartDate());
                data.setOldEndDate(null);
            }
        });
        startDatePicker.setEditable(false);
        endDatePicker.setEditable(false);
        dateDatePicker.setEditable(false);
        
        
        
    }   
    public VBox getScheduleTabContent()
    {
        return scheduleTabContent;
    }
    public Label getSchedule(){
        return schedule;
    }
    public Label getCalenderBoundaries(){
        return calenderBoundaries;
    }
    public Label getStartingMonday(){
        return startingMonday;
    }
    
    public Label getEndingFriday(){
        return endingFriday;
    }
    public String getStartDate(){
        return startDatePicker.getValue().format(DateTimeFormatter.ofPattern("yyyy/MM/dd"));
    }
    public String getEndDate(){
        return endDatePicker.getValue().format(DateTimeFormatter.ofPattern("yyyy/MM/dd"));
    }
    public String getScheduleDate(){
        return dateDatePicker.getValue().format(DateTimeFormatter.ofPattern("yyyy/MM/dd"));
    }
    public void setStartDate(String startDate){
        String[] dateArray = (startDate.split("/"));
        int year = Integer.parseInt(dateArray[0]);
        int month = Integer.parseInt(dateArray[1]);
        int day = Integer.parseInt(dateArray[2]);
        if(month < 10 && dateArray[1].length() == 2)
            month = Integer.parseInt(dateArray[1].substring(1,2));
        if(day < 10 && dateArray[2].length() == 2)
            day = Integer.parseInt(dateArray[2].substring(1,2));
        
        startDatePicker.setValue(LocalDate.of(year, month, day));
    }
    public void setEndDate(String endDate){
        String[] dateArray = (endDate.split("/"));
        int year = Integer.parseInt(dateArray[0]);
        int month = Integer.parseInt(dateArray[1]);
        int day = Integer.parseInt(dateArray[2]);
        if(month < 10 && dateArray[1].length() == 2)
            month = Integer.parseInt(dateArray[1].substring(1,2));
        if(day < 10 && dateArray[2].length() == 2)
            day = Integer.parseInt(dateArray[2].substring(1,2));
        
        endDatePicker.setValue(LocalDate.of(year, month, day));
    }
    public void setDateDatePicker(String dateDate){
        String[] dateArray = (dateDate.split("/"));
        int year = Integer.parseInt(dateArray[0]);
        int month = Integer.parseInt(dateArray[1]);
        int day = Integer.parseInt(dateArray[2]);
        if(month < 10 && dateArray[1].length() == 2)
            month = Integer.parseInt(dateArray[1].substring(1,2));
        if(day < 10 && dateArray[2].length() == 2)
            day = Integer.parseInt(dateArray[2].substring(1,2));
        
        dateDatePicker.setValue(LocalDate.of(year, month, day));
    }
    public Label getScheduleItems(){
        return scheduleItems;
    }
    public Label getAddEdit(){
        return addEdit;
    }
    public Label getType(){
        return type;
    }
    public Label getDate(){
        return date;
    }
    public Label getTime(){
        return time;
    }
    public Label getTitle(){
        return title;
    }
    public Label getTopic(){
        return topic;
    }
    public Label getLink(){
        return link;
    }
    public Label getCriteria(){
        return criteria;
    }
    public ChoiceBox getTypeChoiceBox(){
        return typeChoiceBox;
    }
    public TextField getTimeTextField(){
        return timeTextField;
    }
    public TextField getTitleTextField(){
        return titleTextField;
    }
    public TextField getTopicTextField(){
        return topicTextField;
    }
    public TextField getLinkTextField(){
        return linkTextField;
    }
    public TextField getCriteriaTextField(){
        return criteriaTextField;
    }
    public void resetScheduleTab(){
        startDatePicker.getEditor().clear();
        startDatePicker.setValue(null);
        endDatePicker.getEditor().clear();
        endDatePicker.setValue(null);
        scheduleTable.getItems().clear();    
        typeChoiceBox.setValue(null);
        dateDatePicker.getEditor().clear();
        dateDatePicker.setValue(null);
        timeTextField.clear();
        titleTextField.clear();
        topicTextField.clear();
        linkTextField.clear();
        criteriaTextField.clear();
    }
}

