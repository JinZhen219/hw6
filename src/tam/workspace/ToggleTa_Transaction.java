/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.workspace;

import javafx.scene.layout.Pane;
import jtps.jTPS_Transaction;
import tam.TAManagerApp;
import tam.data.TAData;
import tam.data.TeachingAssistant;

/**
 *
 * @author khurr
 */
public class ToggleTa_Transaction implements jTPS_Transaction {

    String cellKey;
    String taName;
    TAData data;
    public ToggleTa_Transaction(String initCellKey, String initTAName, TAData initData)
    {
       cellKey = initCellKey;
       taName = initTAName;
       data = initData;
    }
    @Override 
    public void doTransaction()
    { 
       data.toggleTAOfficeHours(cellKey, taName);
    }
    @Override 
    public void undoTransaction()
    {
        data.toggleTAOfficeHours(cellKey, taName);
    
    }

}
