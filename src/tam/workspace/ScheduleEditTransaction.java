/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.workspace;

import jtps.jTPS_Transaction;
import tam.data.Schedule;
import tam.data.TAData;

/**
 *
 * @author Jin
 */
public class ScheduleEditTransaction implements jTPS_Transaction{
    private Schedule sche;
    private TAWorkspace workspace;
    private Schedule newSchedule;
    private TAData data;
    
    public ScheduleEditTransaction(Schedule sche, TAData data){
        this.sche = sche;
        this.data = data;
        workspace = data.getWorkspace();
        scheduleDataTabContent scheduleTab = workspace.getScheduleTabContent();
        newSchedule = new Schedule(scheduleTab.typeChoiceBox.getValue().toString(), scheduleTab.getScheduleDate(), scheduleTab.timeTextField.getText(), scheduleTab.titleTextField.getText(), 
                                            scheduleTab.topicTextField.getText(), scheduleTab.linkTextField.getText(), scheduleTab.criteriaTextField.getText());
    }
    @Override 
    public void doTransaction(){
        data.removeSchedule(sche.getType(), sche.getDate(), sche.getTime(), sche.getTitle(), sche.getTopic(), sche.getLink(), sche.getCriteria());
        data.addSchedule(newSchedule.getType(), newSchedule.getDate(), newSchedule.getTime(), newSchedule.getTitle(), newSchedule.getTopic(), newSchedule.getLink(), newSchedule.getCriteria());
    }
    @Override 
    public void undoTransaction(){
        data.removeSchedule(newSchedule.getType(), newSchedule.getDate(), newSchedule.getTime(), newSchedule.getTitle(), newSchedule.getTopic(), newSchedule.getLink(), newSchedule.getCriteria());
        data.addSchedule(sche.getType(), sche.getDate(), sche.getTime(), sche.getTitle(), sche.getTopic(), sche.getLink(), sche.getCriteria());
    }
}
