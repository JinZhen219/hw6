/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.workspace;

import jtps.jTPS_Transaction;
import tam.data.TAData;

/**
 *
 * @author Jin
 */
public class AddRecitationTransaction implements jTPS_Transaction{
    private recitationDataTabContent recitationTab;
    private TAData data;
    private String section;
    private String instructor;
    private String dayTime;
    private String location;
    private String ta1;
    private String ta2;
    public AddRecitationTransaction(recitationDataTabContent recitationTab, TAData data){
        this.recitationTab = recitationTab;
        this.data = data;
        section = recitationTab.sectionTextField.getText();
        instructor = recitationTab.instructorTextField.getText();
        dayTime = recitationTab.dayTimeTextField.getText();
        location = recitationTab.locationTextField.getText();
        ta1 = recitationTab.ta1ChoiceBox.getValue().toString();
        ta2 = recitationTab.ta2ChoiceBox.getValue().toString();
    }
    @Override
    public void doTransaction(){
        data.addRecitation(section, instructor, dayTime, location, ta1, ta2);
    }
    @Override
    public void undoTransaction(){
        data.removeRecitation(section);
    }
    public void clear(){
        TAWorkspace workspace = data.getWorkspace();
            recitationDataTabContent recitationTab = workspace.getRecitationTabContent();
            recitationTab.sectionTextField.clear();
            recitationTab.instructorTextField.clear();
            recitationTab.dayTimeTextField.clear();
            recitationTab.locationTextField.clear();
            recitationTab.ta1ChoiceBox.setValue(null);
            recitationTab.ta2ChoiceBox.setValue(null);
            recitationTab.sectionTextField.requestFocus();
            recitationTab.update = false;
    }
}
