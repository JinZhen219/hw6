/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.workspace;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.paint.Color;
import jtps.jTPS_Transaction;
import tam.data.Student;
import tam.data.TAData;
import tam.data.Team;

/**
 *
 * @author Jin
 */
public class EditTeamTransaction implements jTPS_Transaction{
    private Team team;
    private TAWorkspace workspace;
    private Team newTeam;
    private TAData data;
    private projectDataTabContent projectTab;
    ObservableList<Student> studentsList;
    ObservableList<Student> modifiedStudentList;
    ObservableList<Student> anotherOne;
    public EditTeamTransaction(Team team, TAData data){
        this.team = team;
        this.data = data;
        workspace = data.getWorkspace();
        projectTab = workspace.getProjectTabContent();
        newTeam = new Team(projectTab.nameTextField.getText(), projectTab.colorPicker.getValue().toString().substring(2,8), projectTab.textColorPicker.getValue().toString().substring(2,8), projectTab.linkTextField.getText());
        studentsList = data.getStudents();
        modifiedStudentList = data.getModifiedStudentList();
        anotherOne = FXCollections.observableArrayList(studentsList);
    }
    @Override 
    public void doTransaction(){
        data.removeTeam(team.getName());
        data.addTeam(newTeam.getName(), newTeam.getColor(), newTeam.getTextColor(), newTeam.getLink());
        projectTab.getTeamChoiceBox().getItems().remove(team.getName());
        projectTab.getTeamChoiceBox().getItems().add(newTeam.getName());
        for(Student stu: studentsList){
            try{
                if(stu.getTeam().equals(team.getName())){
                    Student stus = new Student(stu.getFirstName(), stu.getLastName(), stu.getTeam(), stu.getRole());
                    modifiedStudentList.add(stus);
                    stu.setTeam(newTeam.getName());
                    
                }
            }
            catch(NullPointerException e){
                
            }
                  
        }
        projectTab.studentTable.refresh();
        clear();
    }
    @Override 
    public void undoTransaction(){
        data.removeTeam(newTeam.getName());
        data.addTeam(team.getName(), team.getColor(), team.getTextColor(), team.getLink());
        projectTab.getTeamChoiceBox().getItems().remove(newTeam.getName());
        projectTab.getTeamChoiceBox().getItems().add(team.getName());
        
        for(Student stu : studentsList){
            for(Student student2 : modifiedStudentList){
                if(stu.getFirstName().equals(student2.getFirstName()) && stu.getLastName().equals(student2.getLastName()) && stu.getRole().equals(student2.getRole()))
                {
                    stu.setTeam(student2.getTeam());
                }
            }
        }
        modifiedStudentList.clear();
        projectTab.studentTable.refresh();
        clear();
    }
    public void clear(){
        projectDataTabContent projectTab = workspace.getProjectTabContent();
        projectTab.nameTextField.clear();
        projectTab.colorPicker.setValue(Color.WHITE);
        projectTab.textColorPicker.setValue(Color.WHITE);
        projectTab.linkTextField.clear();
        projectTab.teamUpdate = false;
    }
}
