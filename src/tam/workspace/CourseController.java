/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.workspace;

import static djf.controller.AppFileController.PATH_PUBLIC_HTML;
import static djf.settings.AppPropertyType.LOAD_ERROR_MESSAGE;
import static djf.settings.AppPropertyType.LOAD_ERROR_TITLE;
import static djf.settings.AppPropertyType.SAVE_WORK_TITLE;
import static djf.settings.AppPropertyType.WORK_FILE_EXT;
import static djf.settings.AppPropertyType.WORK_FILE_EXT_DESC;
import static djf.settings.AppStartupConstants.PATH_WORK;
import djf.ui.AppMessageDialogSingleton;
import java.io.File;
import java.io.IOException;
import javafx.scene.image.Image;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javax.imageio.ImageIO;
import jtps.jTPS;
import properties_manager.PropertiesManager;
import tam.CSGManagerProp;
import tam.TAManagerApp;
import static tam.workspace.TAController.jTPS;

/**
 *
 * @author Jin
 */
public class CourseController 
{
    TAManagerApp app;
    
    
     /*
     * 
     * Constructor, note that the app must already be constructed.
     */
    public CourseController(TAManagerApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;
    }
    public void handleChangeExportDirectory(){
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();

        courseDetailsTabContent coursePage = workspace.getCourseDetailsTabContent();
        try
        {
            
            DirectoryChooser dc = new DirectoryChooser();
            dc.setInitialDirectory(new File(PATH_WORK));
            dc.setTitle(props.getProperty(CSGManagerProp.PICK_EXPORT_DIRECTORY));
            // THIS IS DIRECTORY OF MY FOLDER
            // NEED TO PASS IN 
            File newPath = dc.showDialog(app.getGUI().getWindow());
            if(newPath != null)
            {
                coursePage.setExportDirectory(newPath.toString());
            }
        }
        catch(Exception e)
        {
            dialog.show(props.getProperty(CSGManagerProp.SELECT_PATH_ERROR), props.getProperty(CSGManagerProp.SELECT_PATH_ERROR));
        }
            
    }
    public void handleChangeTemplateDirectory(){
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();

        courseDetailsTabContent coursePage = workspace.getCourseDetailsTabContent();
        try
        {
            DirectoryChooser dc = new DirectoryChooser();
            dc.setInitialDirectory(new File(PATH_WORK));
            dc.setTitle(props.getProperty(CSGManagerProp.SELECT_PATH));
            // THIS IS DIRECTORY OF MY FOLDER
            // NEED TO PASS IN 
            File newPath = dc.showDialog(app.getGUI().getWindow());
            if(newPath != null)
            {
                coursePage.getTemplateDirectoryLabel().setText(newPath.toString());
            }
        }
        catch(Exception e)
        {
            dialog.show(props.getProperty(CSGManagerProp.SELECT_PATH_ERROR), props.getProperty(CSGManagerProp.SELECT_PATH_ERROR));
        }
    }
    public void handleChangeSchoolImage(){
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();

        courseDetailsTabContent coursePage = workspace.getCourseDetailsTabContent();
        try
        {
            FileChooser dc = new FileChooser();
            dc.setInitialDirectory(new File(PATH_WORK));

            File file = dc.showOpenDialog(null);
            if(file != null)
            {
                coursePage.setSchoolDirectory(file.toString());
                
                
            }
        }
        catch(Exception e)
        {
            dialog.show(props.getProperty(CSGManagerProp.SELECT_PATH_ERROR), props.getProperty(CSGManagerProp.SELECT_PATH_ERROR));
        }
    }
    public void handleChangeLeftImage(){
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();

        courseDetailsTabContent coursePage = workspace.getCourseDetailsTabContent();
        try
        {
            FileChooser dc = new FileChooser();
            dc.setInitialDirectory(new File(PATH_WORK));

            File file = dc.showOpenDialog(null);
            if(file != null)
            {
                coursePage.setLeftDirectory(file.toString());      
            }
        }
        catch(Exception e)
        {
            dialog.show(props.getProperty(CSGManagerProp.SELECT_PATH_ERROR), props.getProperty(CSGManagerProp.SELECT_PATH_ERROR));
        }
    }
    public void handleChangeRightImage(){
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();

        courseDetailsTabContent coursePage = workspace.getCourseDetailsTabContent();
        try
        {
            FileChooser dc = new FileChooser();
            dc.setInitialDirectory(new File(PATH_WORK));

            File file = dc.showOpenDialog(null);
            if(file != null)
            {
                coursePage.setRightDirectory(file.toString());
                
                
            }
        }
        catch(Exception e)
        {
            dialog.show(props.getProperty(CSGManagerProp.SELECT_PATH_ERROR), props.getProperty(CSGManagerProp.SELECT_PATH_ERROR));
        }
    }
}
