
package tam.workspace;

import djf.settings.AppPropertyType;
import static djf.settings.AppStartupConstants.FILE_PROTOCOL;
import static djf.settings.AppStartupConstants.PATH_IMAGES;
import djf.ui.AppMessageDialogSingleton;
import tam.CSGManagerProp;
import java.util.ArrayList;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;
import tam.TAManagerApp;
import tam.data.Recitation;
import tam.data.TAData;
import tam.data.TeachingAssistant;
import tam.data.Team;

/**
 *
 * @author Jin
 */
public class recitationDataTabContent
{
    VBox recitationDataBox = new VBox();
    
    Label recitationLabel = new Label();
    ObservableList<String> recitationGridHeaders;
    Label addEdit = new Label();
    Label section = new Label();
    Label instructor = new Label();
    Label dayTime = new Label();
    Label location = new Label();
    Label supervisingTA1 = new Label();
    Label supervisingTA2 = new Label();
    TextField sectionTextField = new TextField();
    TextField instructorTextField = new TextField();
    TextField dayTimeTextField = new TextField();
    TextField locationTextField = new TextField();
    ChoiceBox ta1ChoiceBox = new ChoiceBox();
    ChoiceBox ta2ChoiceBox = new ChoiceBox();
    Button addUpdateButton = new Button();
    Button clearButton = new Button();
    TableView recitationTable = new TableView();
    TAManagerApp app;
    TableColumn sectionColumn;
    TableColumn instructorColumn;
    TableColumn dayTimeColumn;
    TableColumn locationColumn;
    TableColumn firstTAColumn;
    TableColumn secondTAColumn;
    Button deleteButton;
    boolean update = false;
    public recitationDataTabContent(TAManagerApp initApp)
    {
        
        app = initApp;
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        recitationLabel.setText(props.getProperty(CSGManagerProp.RECITATION));
        addEdit.setText(props.getProperty(CSGManagerProp.ADD_EDIT));
        section.setText(props.getProperty(CSGManagerProp.SECTION));
        instructor.setText(props.getProperty(CSGManagerProp.INSTRUCTOR));
        dayTime.setText(props.getProperty(CSGManagerProp.DAY_TIME));
        location.setText(props.getProperty(CSGManagerProp.LOCATION));
        supervisingTA1.setText(props.getProperty(CSGManagerProp.TA_1));
        supervisingTA2.setText(props.getProperty(CSGManagerProp.TA_1));
        sectionTextField.setPromptText(props.getProperty(CSGManagerProp.SECTION_PROMPT_TEXT));
        instructorTextField.setPromptText(props.getProperty(CSGManagerProp.INSTRUCTOR_PROMPT_TEXT));
        dayTimeTextField.setPromptText(props.getProperty(CSGManagerProp.DAY_TIME_PROMPT_TEXT));
        locationTextField.setPromptText(props.getProperty(CSGManagerProp.LOCATION_PROMPT_TEXT));
        addUpdateButton.setText(props.getProperty(CSGManagerProp.ADD_UPDATE_BUTTON_TEXT));
        clearButton.setText(props.getProperty(CSGManagerProp.CLEAR_BUTTON_TEXT));
        TAData data = (TAData) app.getDataComponent();
        
        
        
//        data.addRecitation("R02", "McKenna", "Wed 3:30pm-4:23pm", "Old CS 2114", "Jane Doe", "Joe Schmo");
//        data.addRecitation("R05", "Banerjee", "Tues 5:30pm-6:23pm", "Old CS 2114", "xooo fdsa", "ybu fds");

        recitationTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        
        ObservableList<Recitation> recData = data.getRecitations();
        recitationTable.setItems(recData);
        ArrayList<String> recitationGridHeaders = props.getPropertyOptionsList(CSGManagerProp.RECITATION_GRID_HEADERS);
        sectionColumn = new TableColumn(recitationGridHeaders.get(0));
        instructorColumn = new TableColumn(recitationGridHeaders.get(1));
        dayTimeColumn = new TableColumn(recitationGridHeaders.get(2));
        locationColumn = new TableColumn(recitationGridHeaders.get(3));
        firstTAColumn = new TableColumn(recitationGridHeaders.get(4));
        secondTAColumn = new TableColumn(recitationGridHeaders.get(5));
        
        TableColumn newColumn = new TableColumn(recitationGridHeaders.get(4));
        
        sectionColumn.setCellValueFactory(new PropertyValueFactory<Recitation, String>("section"));
        instructorColumn.setCellValueFactory(new PropertyValueFactory<Recitation, String>("instructor"));
        dayTimeColumn.setCellValueFactory(new PropertyValueFactory<Recitation, String>("dayTime"));
        locationColumn.setCellValueFactory(new PropertyValueFactory<Recitation, String>("location"));
        firstTAColumn.setCellValueFactory(new PropertyValueFactory<Recitation, String>("ta1"));
        secondTAColumn.setCellValueFactory(new PropertyValueFactory<Recitation, String>("ta2"));
        newColumn.setCellValueFactory(new PropertyValueFactory<Recitation, String>("failure"));
        
        sectionColumn.setMinWidth(200);
        instructorColumn.setMinWidth(200);
        dayTimeColumn.setMinWidth(200);
        locationColumn.setMinWidth(200);
        firstTAColumn.setMinWidth(200);
        secondTAColumn.setMinWidth(200);
        recitationTable.getColumns().addAll(sectionColumn, instructorColumn, dayTimeColumn, locationColumn, firstTAColumn, secondTAColumn);
        recitationTable.setFocusTraversable(true);
        deleteButton = new Button();
        recitationTable.setMaxWidth(1300);

        
        String imagePath = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(AppPropertyType.DELETE_ICON);
        Image buttonImage = new Image(imagePath);
	
	// NOW MAKE THE BUTTON
        deleteButton.setDisable(false);
        deleteButton.setGraphic(new ImageView(buttonImage));


        
        
        
        
        HBox recitationLineBox = new HBox(recitationLabel, deleteButton);
        recitationLineBox.setSpacing(5);
        recitationLineBox.setPadding(new Insets(5,0,0,5));
        HBox sectionLine = new HBox(section, sectionTextField);
        section.setPrefWidth(160);
        HBox instructorLine = new HBox(instructor, instructorTextField);
        instructor.setPrefWidth(160);
        HBox dayTimeLine = new HBox(dayTime, dayTimeTextField);
        dayTime.setPrefWidth(160);
        HBox locationLine = new HBox(location, locationTextField);
        location.setPrefWidth(160);
        HBox ta1Line = new HBox(supervisingTA1, ta1ChoiceBox);
        supervisingTA1.setPrefWidth(160);
        ta1ChoiceBox.getItems().addAll("Joe Shmo", "Jane Doe");
        ta2ChoiceBox.getItems().addAll("Joe Shmo", "Jane Doe");
        HBox ta2Line = new HBox(supervisingTA2, ta2ChoiceBox);
        supervisingTA2.setPrefWidth(160);
        
        HBox buttonLine = new HBox(addUpdateButton, clearButton);
        buttonLine.setSpacing(30);
        VBox addEditBox2 = new VBox(sectionLine,instructorLine,dayTimeLine,locationLine,ta1Line,ta2Line,buttonLine);
        addEditBox2.setSpacing(5);
        addEditBox2.setPadding(new Insets(15,15,15,15));
        VBox addEditBox = new VBox(addEdit, addEditBox2);
        addEditBox.setSpacing(15);
        recitationDataBox.setPadding(new Insets(0,0,0,8));
        recitationDataBox.getChildren().addAll(recitationLineBox, recitationTable, addEditBox);
        recitationDataBox.setSpacing(20);
        
        
        RecitationController controller = new RecitationController(app);
        addUpdateButton.setOnMouseClicked(e->{
            controller.handleAddUpdate();
        });
        recitationTable.setOnMouseClicked(e->{
            try{
                Object selectedItem = recitationTable.getSelectionModel().getSelectedItem();
                Recitation rec = (Recitation)selectedItem;
                sectionTextField.setText(rec.getSection());
                instructorTextField.setText(rec.getInstructor());
                dayTimeTextField.setText(rec.getDayTime());
                locationTextField.setText(rec.getLocation());
                ta1ChoiceBox.setValue(rec.getTa1());
                ta2ChoiceBox.setValue(rec.getTa2());
                update = true;
            }
            catch(Exception ex){
//                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
//                dialog.show(props.getProperty(CSGManagerProp.MISSING_INFO_ERROR), props.getProperty(CSGManagerProp.MISSING_INFO_ERROR));
            }
        });
        clearButton.setOnMouseClicked(e->{
            controller.handleClear();
        });
        deleteButton.setOnMouseClicked(e->{
            controller.handleDelete(KeyCode.DELETE);
        });
        recitationTable.setOnKeyPressed(e->{
            controller.handleDelete(e.getCode());
        });
        
    }
    public VBox getRecitationDataBox()
    {
        return recitationDataBox;
    }
    public Label getRecitationLabel(){
        return recitationLabel;
    }
    public Label getaddEditLabel(){
        return addEdit;
    }
    public Label getSection(){
        return section;
    }
    public Label getInstructor(){
        return instructor;
    }
    public Label getDayTime(){
        return dayTime;
    }
    public Label getLocation(){
        return location;
    }
    public Label getSupervisingTA1(){
        return supervisingTA1;
    }
    public Label getSupervisingTA2(){
        return supervisingTA2;
    }
    public void resetRecitationTab(){
        recitationTable.getItems().clear();
        sectionTextField.clear();
        instructorTextField.clear();
        dayTimeTextField.clear();
        locationTextField.clear();
        ta1ChoiceBox.getItems().clear();
        ta2ChoiceBox.getItems().clear();
    }
    public TextField getSectionTextField(){
        return sectionTextField;
    }
    public TextField getInstructorTextField(){
        return instructorTextField;
    }
    public TextField getDayTimeTextField(){
        return dayTimeTextField;
    }
    public TextField getLocationTextField(){
        return locationTextField;
    }
    public ChoiceBox getTA1ChoiceBox(){
        return ta1ChoiceBox;
    }
    public ChoiceBox getTA2ChoiceBox(){
        return ta2ChoiceBox;
    }
    
}
    

