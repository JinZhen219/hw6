/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.workspace;

import javafx.scene.paint.Color;
import jtps.jTPS_Transaction;
import tam.data.Schedule;
import tam.data.TAData;

/**
 *
 * @author Jin
 */
public class AddStudentTransaction implements jTPS_Transaction{
    private TAData data;
    private projectDataTabContent projectTab;
    private String firstName;
    private String lastName;
    private String team;
    private String role;
    
    public AddStudentTransaction(TAData data, projectDataTabContent projectTab){
        this.data = data;
        this.projectTab = projectTab;
        firstName = projectTab.firstNameTextField.getText();
        lastName = projectTab.lastNameTextField.getText();
        team = projectTab.teamChoiceBox.getValue().toString();
        role = projectTab.roleTextField.getText();
    }
    @Override 
    public void doTransaction(){
        data.addStudent(firstName, lastName, team, role);
        handleClear();
    }
    @Override
    public void undoTransaction(){
        data.removeStudent(firstName, lastName, team, role);
        handleClear();
    }
    public void handleClear(){
        
        projectTab.firstNameTextField.clear();
        projectTab.lastNameTextField.clear();
        projectTab.teamChoiceBox.setValue(null);
        projectTab.roleTextField.clear();
        projectTab.studentUpdate = false;
    }
}
