/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.workspace;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.DatePicker;
import jtps.jTPS_Transaction;
import tam.data.Student;
import tam.data.TAData;

/**
 *
 * @author sincconsult
 */
public class DateChangeTransaction implements jTPS_Transaction{
    private DatePicker datePicker;
    private TAData data;
    private String previousDate;
    private String currentDate;
    public DateChangeTransaction(DatePicker datePicker, TAData data) {
        
        this.data = data;
        this.datePicker = datePicker;
        previousDate = data.getOldStartDate();
        
        currentDate = previousDate;
        if(currentDate.equals("") || !currentDate.equals(datePicker.getValue().format(DateTimeFormatter.ofPattern("yyyy/MM/dd")))){
            currentDate = datePicker.getValue().format(DateTimeFormatter.ofPattern("yyyy/MM/dd"));
            data.setNextStartDate(previousDate);
        }
    }

    @Override
    public void doTransaction() {  //control Y 
        
        
        String[] dateArray = (currentDate.split("/"));
        int year = Integer.parseInt(dateArray[0]);
        int month = Integer.parseInt(dateArray[1]);
        int day = Integer.parseInt(dateArray[2]);
        if(month < 10 && dateArray[1].length() == 2)
            month = Integer.parseInt(dateArray[1].substring(1,2));
        if(day < 10 && dateArray[2].length() == 2)
            day = Integer.parseInt(dateArray[2].substring(1,2));       
        datePicker.setValue(LocalDate.of(year, month, day));
        
        data.setOldStartDate(previousDate);
    }

    @Override
    public void undoTransaction() {               
        String oldDate = previousDate;
        String[] dateArray = (oldDate.split("/"));
        int year = Integer.parseInt(dateArray[0]);
        int month = Integer.parseInt(dateArray[1]);
        int day = Integer.parseInt(dateArray[2]);
        if(month < 10 && dateArray[1].length() == 2)
            month = Integer.parseInt(dateArray[1].substring(1,2));
        if(day < 10 && dateArray[2].length() == 2)
            day = Integer.parseInt(dateArray[2].substring(1,2));
        data.setNextStartDate(currentDate);
        datePicker.setValue(LocalDate.of(year, month, day));
    }
}
