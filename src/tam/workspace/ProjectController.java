/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.workspace;

import djf.ui.AppGUI;
import djf.ui.AppMessageDialogSingleton;
import java.util.HashMap;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableView;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;
import jtps.jTPS_Transaction;
import properties_manager.PropertiesManager;
import tam.CSGManagerProp;
import tam.TAManagerApp;
import tam.data.Schedule;
import tam.data.Student;
import tam.data.TAData;
import tam.data.TeachingAssistant;
import tam.data.Team;
import jtps.jTPS;
import static tam.workspace.TAController.jTPS;


/**
 *
 * @author Jin
 */
public class ProjectController {
    TAManagerApp app;
    TAData data;
    
     /*
     * 
     * Constructor, note that the app must already be constructed.
     */
    public ProjectController(TAManagerApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;
        data = (TAData)app.getDataComponent();
        
    }
    public void handleTeamClear(){
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        projectDataTabContent projectTab = workspace.getProjectTabContent();
        projectTab.nameTextField.clear();
        projectTab.colorPicker.setValue(Color.WHITE);
        projectTab.textColorPicker.setValue(Color.WHITE);
        projectTab.linkTextField.clear();
        projectTab.teamUpdate = false;
    }
    public void handleStudentClear(){
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        projectDataTabContent projectTab = workspace.getProjectTabContent();
        projectTab.firstNameTextField.clear();
        projectTab.lastNameTextField.clear();
        projectTab.roleTextField.clear();
        projectTab.teamChoiceBox.setValue(null);
        projectTab.studentUpdate = false;
    }
    public void handleTeamAddUpdate(){
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        projectDataTabContent projectTab = workspace.getProjectTabContent();
        try{
            if(projectTab.teamUpdate == true)
            {
                Object selectedItem = projectTab.teamTable.getSelectionModel().getSelectedItem();
                Team team = (Team) selectedItem;
                System.out.println("Updating team " + team.getName());

                // IF IT REACHES BELOW THEN IT IS UPDATING
//                data.editTeam(team, workspace);
                jTPS_Transaction trans = new EditTeamTransaction(team, data);
                jTPS.addTransaction(trans);
//                handleTeamClear();
            }
            else throw new Exception();
            
        }
        catch(Exception e)
        {
            System.out.println("adding team");
            if(data.containTeam(projectTab.nameTextField.getText()) == true)
            {
                PropertiesManager props = PropertiesManager.getPropertiesManager();
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(CSGManagerProp.TEAM_EXIST_ERROR), props.getProperty(CSGManagerProp.TEAM_EXIST_ERROR));
            
            }
            else{
                try{
//                    data.addTeam(projectTab.nameTextField.getText(), projectTab.colorPicker.getValue().toString().substring(2,8), projectTab.textColorPicker.getValue().toString().substring(2,8), projectTab.linkTextField.getText());
                    jTPS_Transaction trans = new AddTeamTransaction(data, projectTab);
                    jTPS.addTransaction(trans);
//                    projectTab.getTeamChoiceBox().getItems().add(projectTab.getTeamNameTextField().getText());
                    handleTeamClear();
                }
                catch(NullPointerException ex){
                    PropertiesManager props = PropertiesManager.getPropertiesManager();
                    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                    dialog.show(props.getProperty(CSGManagerProp.MISSING_INFO_ERROR), props.getProperty(CSGManagerProp.MISSING_INFO_ERROR));
                }
            }
        }
    }
    public void handleStudentAddUpdate(){
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        projectDataTabContent projectTab = workspace.getProjectTabContent();
        try{
            if(projectTab.studentUpdate == true)
            {
                Object selectedItem = projectTab.studentTable.getSelectionModel().getSelectedItem();
                Student student = (Student) selectedItem;
                System.out.println("Updating student " + student.getFirstName());

                // IF IT REACHES BELOW THEN IT IS UPDATING
//                data.editStudent(student, workspace);
                jTPS_Transaction trans = new EditStudentTransaction(student, data);
                jTPS.addTransaction(trans);
                handleStudentClear();
            }
            else throw new Exception();
            
        }
        catch(Exception e)
        {
            System.out.println("adding student");
            if(data.containStudent(projectTab.firstNameTextField.getText(), projectTab.lastNameTextField.getText(), projectTab.teamChoiceBox.getValue().toString(), projectTab.roleTextField.getText()) == true)
            {
                PropertiesManager props = PropertiesManager.getPropertiesManager();
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(CSGManagerProp.STUDENT_EXIST_ERROR), props.getProperty(CSGManagerProp.STUDENT_EXIST_ERROR));
            
            }
            else{
                try{
//                    data.addStudent(projectTab.firstNameTextField.getText(), projectTab.lastNameTextField.getText(), projectTab.teamChoiceBox.getValue().toString(), projectTab.roleTextField.getText());
                    jTPS_Transaction trans = new AddStudentTransaction(data, projectTab);
                    jTPS.addTransaction(trans);
                    handleStudentClear();
                }
                catch(NullPointerException ex){
                    PropertiesManager props = PropertiesManager.getPropertiesManager();
                    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                    dialog.show(props.getProperty(CSGManagerProp.MISSING_INFO_ERROR), props.getProperty(CSGManagerProp.MISSING_INFO_ERROR));
                }
            }
        }
    }
    
    public void handleTeamDelete(KeyCode code){
        if(code == KeyCode.DELETE){
            TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
            projectDataTabContent projectTab = workspace.getProjectTabContent();
            
            TableView teamTable = projectTab.teamTable;
            Object selectedItem = teamTable.getSelectionModel().getSelectedItem();
            if (selectedItem != null) {
                Team team = (Team) selectedItem;
                TAData data = (TAData) app.getDataComponent();
                
                jTPS_Transaction transaction1 = new DeleteTeamTransaction(team, data);
                jTPS.addTransaction(transaction1);
                markWorkAsEdited();
                handleTeamClear();
            }
        }
    }
    public void handleStudentDelete(KeyCode code){
        if(code == KeyCode.DELETE){
            TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
            projectDataTabContent projectTab = workspace.getProjectTabContent();
            
            TableView studentTable = projectTab.studentTable;
            Object selectedItem = studentTable.getSelectionModel().getSelectedItem();
            if (selectedItem != null) {
                Student student = (Student) selectedItem;
                TAData data = (TAData) app.getDataComponent();
                
                jTPS_Transaction transaction1 = new DeleteStudentTransaction(student, data);
                jTPS.addTransaction(transaction1);
                handleStudentClear();
                markWorkAsEdited();
            }
        }
    }
    public void markWorkAsEdited() {
        // MARK WORK AS EDITED
        AppGUI gui = app.getGUI();
        gui.getFileController().markAsEdited(gui);
    }    
}
