
package tam.workspace;
import java.util.HashMap;
import javafx.collections.ObservableList;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import jtps.jTPS_Transaction;
import tam.data.TAData;
import tam.TAManagerApp;
import tam.data.Recitation;
import tam.workspace.TAWorkspace;
/**
 *
 * @author Jin
 */
public class editTATransaction implements jTPS_Transaction
{
    String oldName;
    String newName;
    String oldEmail;
    String newEmail;
    TAData data;
    TAWorkspace workspace;
    boolean undergrad;
    recitationDataTabContent recitationTab;
    private ObservableList<Recitation> recitationList;
    private ObservableList<Recitation> modifiedRecitationList;
    public editTATransaction(String initName, String initEmail, TAData initData, TAWorkspace initWorkspace)
    {
       newName = initName;
       newEmail = initEmail;
       data = initData;
       workspace = initWorkspace;
       oldName = workspace.getOldName();
       oldEmail = workspace.getOldEmail();
       recitationTab = workspace.getRecitationTabContent();
       recitationList = data.getRecitations();
       modifiedRecitationList = workspace.getController().modifiedRecitationList;
    }
    @Override 
    public void doTransaction()
    { 

//        data.editTA(newName, newEmail);

        try{
            undergrad = data.getTA(workspace.getOldName()).getUndergraduate();
        }
        catch(NullPointerException e){
            undergrad = false;
        }
               //RETURNS WHETHER THE TA HAS BEEN CHANGED OR NOT
        
            data.removeTA(oldName);

            
            {
                data.addTA(newName,newEmail, undergrad);
                workspace.setOldEmail(oldEmail);
                workspace.setOldName(oldName);

            }
        HashMap<String, Label> labels = workspace.getOfficeHoursGridTACellLabels();
                for (Label label : labels.values()) {
                    if (label.getText().equals(workspace.getOldName())
                    || (label.getText().contains(workspace.getOldName() + "\n"))
                    || (label.getText().contains("\n" + workspace.getOldName()))) {
                        data.editTACell(label.textProperty(), newName);
                           }
                    }
//        workspace.setUpdate(false);
        workspace.emailTextField.setText("");
        workspace.nameTextField.setText("");
        
        workspace.taTable.refresh();
        Pane addBox = workspace.getAddBox();
        addBox.getChildren().clear();
                    
        addBox.getChildren().add(workspace.nameTextField);
        addBox.getChildren().add(workspace.emailTextField);
        addBox.getChildren().add(workspace.getAddButton());
        addBox.getChildren().add(workspace.clearButton); 
        recitationTab.ta1ChoiceBox.getItems().remove(oldName);
        recitationTab.ta2ChoiceBox.getItems().remove(oldName);
        recitationTab.ta1ChoiceBox.getItems().add(newName);
        recitationTab.ta2ChoiceBox.getItems().add(newName);
        
        for(Recitation rec : recitationList)
        {
            if(rec.getTa1().equals(oldName) && rec.getTa2().equals(oldName)){
                Recitation newRec = new Recitation(rec.getSection(), rec.getInstructor(), rec.getDayTime(), rec.getLocation(), rec.getTa1(), rec.getTa2());
                modifiedRecitationList.add(newRec);
                rec.setTA1(newName);
                rec.setTA2(newName);
                
            }
            else if(rec.getTa1().equals(oldName)){
                Recitation newRec = new Recitation(rec.getSection(), rec.getInstructor(), rec.getDayTime(), rec.getLocation(), rec.getTa1(), rec.getTa2());
                modifiedRecitationList.add(newRec);
                rec.setTA1(newName);
            }
            else if(rec.getTa2().equals(oldName)){
                Recitation newRec = new Recitation(rec.getSection(), rec.getInstructor(), rec.getDayTime(), rec.getLocation(), rec.getTa1(), rec.getTa2());
                modifiedRecitationList.add(newRec);
                rec.setTA2(newName);
            }            
        }
        workspace.getRecitationTabContent().recitationTable.refresh();
        
        
        
    }
    @Override 
    public void undoTransaction()
    {
//        workspace.setOldEmail(newEmail);
//        workspace.setOldName(newName);
//        data.editTA(oldName, oldEmail);
        try{
            undergrad = data.getTA(workspace.getOldName()).getUndergraduate();
        }
        catch(NullPointerException e){
            undergrad = false;
        }
               //RETURNS WHETHER THE TA HAS BEEN CHANGED OR NOT
        
            data.removeTA(newName);

                
                data.addTA(workspace.getOldName(),workspace.getOldEmail(),undergrad );
                
//                workspace.getTATable().refresh();    

//                data.addTA(oldName,oldEmail, undergrad);
                workspace.setOldEmail(newEmail);
                workspace.setOldName(newName);
//                workspace.getTATable().refresh();    
            
        HashMap<String, Label> labels = workspace.getOfficeHoursGridTACellLabels();
                for (Label label : labels.values()) {
                    if (label.getText().equals(newName)
                    || (label.getText().contains(newName + "\n"))
                    || (label.getText().contains("\n" + newName))) {
                        data.editTACell(label.textProperty(), oldName);
                           }
                    }
        workspace.taTable.refresh();
//        workspace.setUpdate(false);
        workspace.emailTextField.setText("");
        workspace.nameTextField.setText("");
        Pane addBox = workspace.getAddBox();
        addBox.getChildren().clear();
                    
        addBox.getChildren().add(workspace.nameTextField);
        addBox.getChildren().add(workspace.emailTextField);
        addBox.getChildren().add(workspace.getAddButton());
        addBox.getChildren().add(workspace.clearButton); 
        recitationTab.ta1ChoiceBox.getItems().add(oldName);
        recitationTab.ta2ChoiceBox.getItems().add(oldName);
        recitationTab.ta1ChoiceBox.getItems().remove(newName);
        recitationTab.ta2ChoiceBox.getItems().remove(newName);
        for(Recitation rec : recitationList){
            for(Recitation rec2 : modifiedRecitationList){
                String y = rec2.getTa1();
                String x = rec.getTa1();

                if(rec2.getSection().equals(rec.getSection())){
                    rec.setTA1(rec2.getTa1());
                    rec.setTA2(rec2.getTa2());
                }
            }
        }
        modifiedRecitationList.clear();
        workspace.getRecitationTabContent().recitationTable.refresh();
    }
}
