/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.workspace;

import java.util.ArrayList;
import java.util.HashMap;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import jtps.jTPS_Transaction;
import tam.data.Student;
import tam.data.TAData;
import tam.data.Team;

/**
 *
 * @author khurr
 */
public class DeleteStudentTransaction implements jTPS_Transaction {

    private Student student;
    private TAData data;
    private ObservableList<Student> removedStudentList = FXCollections.observableArrayList();
    public DeleteStudentTransaction(Student student, TAData data) {
        
        this.data = data;
        this.student = student;
    }

    @Override
    public void doTransaction() {  //control Y 
        
        String firstName = student.getFirstName();
        String lastName = student.getLastName();
        String team = student.getTeam();
        String role = student.getRole();
        data.removeStudent(firstName, lastName, team, role);
        
    }

    @Override
    public void undoTransaction() {        
        data.addStudent(student.getFirstName(), student.getLastName(), student.getTeam(), student.getRole());
        
    }
}
