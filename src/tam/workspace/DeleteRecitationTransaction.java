/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.workspace;

import jtps.jTPS_Transaction;
import tam.data.Recitation;
import tam.data.TAData;

/**
 *
 * @author Jin
 */
public class DeleteRecitationTransaction implements jTPS_Transaction{
    private TAData data;
    private Recitation rec;
    public DeleteRecitationTransaction(Recitation rec, TAData data) {       
        this.data = data;
        this.rec = rec;
    }

    @Override
    public void doTransaction() {  //control Y 
        data.removeRecitation(rec.getSection());       
    }

    @Override
    public void undoTransaction() {        
        data.addRecitation(rec.getSection(), rec.getInstructor(), rec.getDayTime(), rec.getLocation(), rec.getTa1(), rec.getTa2());        
    }
}
