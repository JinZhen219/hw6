
package tam.test_bed;

import djf.AppTemplate;
import djf.components.AppFileComponent;
import djf.controller.AppFileController;
import java.io.IOException;
import javafx.stage.Stage;
import tam.TAManagerApp;
import tam.data.TAData;
import tam.file.TAFiles;
import tam.workspace.TAController;
import tam.workspace.TAWorkspace;
import tam.workspace.courseDetailsTabContent;
import tam.workspace.projectDataTabContent;
import tam.workspace.recitationDataTabContent;
import tam.workspace.scheduleDataTabContent;

/**
 *
 * @author Jin
 */
public class TestSave {
    public static void main(String [] args){
        try
        {              
            TAManagerApp app = new TAManagerApp();
            app.loadProperties("app_properties.xml");
            

            TAData data = new TAData(app);
            data.addTA("Kenny Moo", "kennymoo@stonybrook.edu", true);
            data.addTA("Calvin Li", "calvinli@stonybrook.edu", false);
            data.addTA("Calvin Cheng", "calvincheng@stonybrook.edu", true);
            data.setSubject("CSE");
            data.setNumber("219");
            data.setSemester("Fall");
            data.setYear("2018");
            data.setTitle("Comp Sci III");
            data.setInstructorName("Richard McKenna");
            data.setInstructorHome("www.cse219.com");
            data.setHWBoolean(true);
            data.setHomeBoolean(false);
            data.setSyllabusBoolean(false);
            data.setScheduleBoolean(false);
            data.setProjectBoolean(false);
            data.setSchoolDirectory("");
            data.setLeftDirectory("");
            data.setRightDirectory("");
            data.setStartHour(8);
            data.setEndHour(20);
            data.initOfficeHours();
            data.setStyleshet("sea_wolf.css");
            
//            data.addOfficeHoursReservation("MONDAY", "8_00", "jose ramirzez");
            
            
            data.setStartDate("2017/04/08");
            data.setEndDate("2017/05/19");
            data.addSchedule("Holiday", "2/9/17", "All day", "SNOW DAY", "", "", "");
            data.addSchedule("Lecture", "2/14/17", "All day", "Lecture 3", "Event Programming", "", "");
            data.addSchedule("Holiday", "3/13/17", "All day", "Spring Break", "", "", "");
            data.addSchedule("HW", "3/27/17", "All day", "HW3", "UML", "", "");
            data.addRecitation("R02", "McKenna", "Wed 3:30pm-4:23pm", "Old CS 2114", "Jane Doe", "Joe Schmo");
            data.addRecitation("R05", "Banerjee", "Tues 5:30pm-6:23pm", "Old CS 2114", "", "");
            data.addTeam("Atomic Comics", "552211", "fff fff", "http://atomicomin.com");
            data.addTeam("C4 Comics", "235399", "fff fff", "https://c4-comics-appspot.com");
            data.addStudent("Beau", "Brummel", "Atomic Comics", "Lead Designer");
            data.addStudent("Jane", "Doe", "C4 Comics", "Lead Programmer");
            data.addStudent("Moonian", "Soong", "Atomic Comics", "Data Designer");
            
            TAFiles file = new TAFiles(app);            
            file.saveData(data, System.getProperty("user.home") + "\\Desktop\\SiteSaveTest.json");
            System.out.println("WORKED");
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
    
}
