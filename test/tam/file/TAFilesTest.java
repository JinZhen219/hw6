/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.file;

import djf.components.AppDataComponent;
import static djf.settings.AppStartupConstants.APP_PROPERTIES_FILE_NAME;
import java.util.HashMap;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import javax.json.JsonObject;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import tam.TAManagerApp;
import tam.data.Recitation;
import tam.data.Schedule;
import tam.data.Student;
import tam.data.TAData;
import tam.data.TeachingAssistant;
import tam.data.Team;

/**
 *
 * @author Jin
 */
public class TAFilesTest {
    
    public TAFilesTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }



    /**
     * Test of testLoadData method, of class TAFiles.
     */
    @Test
    public void testTestLoadData() throws Exception {
        TAManagerApp app = new TAManagerApp();
        app.loadProperties(APP_PROPERTIES_FILE_NAME);
        TAData data = new TAData(app);
        String filePath =  System.getProperty("user.home") + "\\Desktop\\SiteSaveTest.json";
        TAFiles instance = new TAFiles(app);
        data.initOfficeHours();
        instance.testLoadData(data, filePath);
        JsonObject json = instance.loadJSONFile(filePath);
        // CHECK COURSE INFO DETAILS
        assertEquals("CSE",data.getSubject());
        assertEquals( "219", data.getNumber());
        assertEquals( "Fall", data.getSemester());
        assertEquals("2018", data.getYear());
        assertEquals( "Comp Sci III", data.getTitle());
        assertEquals("www.cse219.com", data.getInstructorHome());
        assertEquals( "Richard McKenna", data.getInstructorName());
        
        //CHECK COURSE SITE CHECKBOX
        assertEquals( false, data.getHome());
        assertEquals( false, data.getSyllabus());
        assertEquals( false, data.getSchedule());
        assertEquals( true, data.getHW());
        assertEquals( false, data.getProject());
        
        //TEST START AND END HOURS
        assertEquals( 8, data.getStartHour());
        assertEquals( 20, data.getEndHour());
        
        //TEST EACH TA
        ObservableList tas = data.getTeachingAssistants();
        TeachingAssistant ta1 = (TeachingAssistant)tas.get(0);
        assertEquals( "Calvin Cheng", ta1.getName());
        assertEquals( "calvincheng@stonybrook.edu", ta1.getEmail());
        assertEquals( true, ta1.getUndergraduate());
        TeachingAssistant ta2 = (TeachingAssistant)tas.get(1);
        assertEquals("Calvin Li", ta2.getName());
        assertEquals( "calvinli@stonybrook.edu", ta2.getEmail());
        assertEquals( false, ta2.getUndergraduate());
        TeachingAssistant ta3 = (TeachingAssistant)tas.get(2);
        assertEquals( "Kenny Moo", ta3.getName());
        assertEquals( "kennymoo@stonybrook.edu", ta3.getEmail());
        assertEquals(true, ta3.getUndergraduate());
        
//        // TEST OFFICE HOURS
//        HashMap<String, StringProperty> officeHours = data.getOfficeHours();
//        StringProperty cellProp = officeHours.get("4_2");
//        assertEquals(cellProp.getValue(), ""); 
        
        // TEST DETAILS IN PAGE STYLE
        assertEquals( "", data.getSchoolDirectory());
        assertEquals( "", data.getLeftDirectory());
        assertEquals( "", data.getRightDirectory());
        assertEquals( "sea_wolf.css", data.getStylesheet());
        
        // TEST EACH RECITATION
        ObservableList recitations = data.getRecitations();
        Recitation rec1 = (Recitation)recitations.get(0);
        assertEquals( "R02", rec1.getSection());
        assertEquals("McKenna", rec1.getInstructor());
        assertEquals("Wed 3:30pm-4:23pm", rec1.getDayTime());
        assertEquals("Old CS 2114", rec1.getLocation());
        assertEquals("Jane Doe", rec1.getTa1());
        assertEquals( "Joe Schmo", rec1.getTa2());
        Recitation rec2 = (Recitation)recitations.get(1);
        assertEquals( "R05", rec2.getSection());
        assertEquals("Banerjee", rec2.getInstructor());
        assertEquals( "Tues 5:30pm-6:23pm", rec2.getDayTime());
        assertEquals("Old CS 2114", rec2.getLocation());
        assertEquals( "", rec2.getTa1());
        assertEquals( "", rec2.getTa2());
        
        // TEST START AND END DATE
        assertEquals( "2017/04/08", data.getStartDate());
        assertEquals( "2017/05/19", data.getEndDate());       


        // TESTING EACH SCHEDULE ITEM
        ObservableList schedules = data.getSchedules();
        Schedule sche1 = (Schedule) schedules.get(0);
        assertEquals( "Holiday", sche1.getType());
        assertEquals( "2/9/17", sche1.getDate());
        assertEquals( "All day", sche1.getTime());
        assertEquals( "SNOW DAY", sche1.getTitle());
        assertEquals( "", sche1.getTopic());
        assertEquals("", sche1.getLink());
        assertEquals( "", sche1.getCriteria());
        
        Schedule sche2 = (Schedule) schedules.get(1);
        assertEquals( "Lecture", sche2.getType());
        assertEquals( "2/14/17", sche2.getDate());
        assertEquals( "All day", sche2.getTime());
        assertEquals( "Lecture 3", sche2.getTitle());
        assertEquals("Event Programming", sche2.getTopic());
        assertEquals( "", sche2.getLink());
        assertEquals( "", sche2.getCriteria());
        
        Schedule sche3 = (Schedule) schedules.get(2);
        assertEquals( "Holiday", sche3.getType());
        assertEquals( "3/13/17", sche3.getDate());
        assertEquals( "All day", sche3.getTime());
        assertEquals("Spring Break", sche3.getTitle());
        assertEquals( "", sche3.getTopic());
        assertEquals( "", sche3.getLink());
        assertEquals("", sche3.getCriteria());
        
        Schedule sche4 = (Schedule) schedules.get(3);
        assertEquals( "HW", sche4.getType());
        assertEquals( "3/27/17", sche4.getDate());
        assertEquals( "All day", sche4.getTime());
        assertEquals( "HW3", sche4.getTitle());
        assertEquals( "UML", sche4.getTopic());
        assertEquals( "", sche4.getLink());
        assertEquals("", sche4.getCriteria());
        
        // TESTING TEAMS
        ObservableList teams = data.getTeams();
        Team team1 = (Team)teams.get(0);
        assertEquals( "Atomic Comics", team1.getName());
        assertEquals( "552211", team1.getColor());
        assertEquals( "fff fff", team1.getTextColor());
        assertEquals( "http://atomicomin.com", team1.getLink());
        Team team2 = (Team)teams.get(1);
        assertEquals( "C4 Comics", team2.getName());
        assertEquals( "235399", team2.getColor());
        assertEquals( "fff fff", team2.getTextColor());
        assertEquals( "https://c4-comics-appspot.com", team2.getLink());
        
        // TESTING STUDENTS
        ObservableList students = data.getStudents();
        Student student1 = (Student)students.get(0);
        assertEquals( "Beau", student1.getFirstName());
        assertEquals( "Brummel", student1.getLastName());
        assertEquals( "Atomic Comics", student1.getTeam());
        assertEquals( "Lead Designer", student1.getRole());
        Student student2 = (Student)students.get(1);
        assertEquals( "Jane", student2.getFirstName());
        assertEquals( "Doe", student2.getLastName());
        assertEquals("C4 Comics", student2.getTeam());
        assertEquals( "Lead Programmer", student2.getRole());
        Student student3 = (Student)students.get(2);
        assertEquals( "Moonian", student3.getFirstName());
        assertEquals( "Soong", student3.getLastName());
        assertEquals("Atomic Comics", student3.getTeam());
        assertEquals( "Data Designer", student3.getRole());
    }
    
}
